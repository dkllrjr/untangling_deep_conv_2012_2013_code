import pandas
import pickle
from datetime import datetime
import numpy as np

##############################################################################
# pandas table

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table

##############################################################################

with open('../data/delta_F.pickle', 'rb') as file:
    dF_datas = pickle.load(file)

with open('../data/mistral_attributes_new.pickle', 'rb') as file:
    precond_data = pickle.load(file)

begs = [datetime(1993, 8, 15), datetime(2004, 8, 31)]
ends = [datetime(1994, 6, 30), datetime(2005, 6, 30)]

inds = []
for i, beg in enumerate(begs):
    inds.append([])
    for j, event in enumerate(precond_data['events']):
        if beg.date() <= event[0] <= ends[i].date():
            inds[i].append(j)


##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table', 'begin{table}[t]', 'tophline', 'middlehline', 'bottomhline']

##############################################################################
# full year

for i, dF_data in enumerate(dF_datas):

    mid_i = int(len(dF_data['dF'])/2) - 1

    dates = []
    for event in np.array(precond_data['events'], dtype='object')[inds[i]]:
        dates.append(event[0])

    data_for_table = {}
    data_for_table['Start Date_0'] = dates[0:mid_i+1]
    data_for_table['val1_0'] = np.array(precond_data['duration'], dtype='object')[inds[i]][0:mid_i+1]
    data_for_table['val2_0'] = np.array(precond_data['period'], dtype='object')[inds[i]][0:mid_i+1]
    data_for_table['val_0'] = dF_data['dF'][0:mid_i+1]

    data_for_table['Start Date_1'] = dates[mid_i:-1]
    data_for_table['val1_1'] = np.array(precond_data['duration'], dtype='object')[inds[i]][mid_i:-1]
    data_for_table['val2_1'] = np.array(precond_data['period'], dtype='object')[inds[i]][mid_i:-1]
    data_for_table['val_1'] = dF_data['dF'][mid_i:-1]

    df = pandas.DataFrame(data_for_table)

    table_header = ['Date', 'replace0', 'replace1', 'replace2', 'Date', 'replace0', 'replace1', 'replace2']

    table = df.to_latex(index=False, float_format='%.2e', label='tab:mistral_strength', caption='', header = table_header)

    table = hlines(table, words_to_replace, replacing_words)
    table = table.replace('lllrlllr', 'lrrr|lrrr')
    table = table.replace('replace0', '$\\delta t_k$')
    table = table.replace('replace1', '$\\delta \\tau_k$')
    table = table.replace('replace2', '$\\delta F_k$')

    ##############################################################################

    print(table)
    with open('../tables/dF_' + str(i) + '.tex', 'w') as file:
        file.writelines(table)
