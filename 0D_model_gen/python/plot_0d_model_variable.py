import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle
import datetime
from glob import glob
import pandas
import delta_F

##############################################################################

def during(t, a_during, D, dFk, tk, dtk, dsitk):
    return (dsitk + D**2/2*dFk/a_during)*np.exp(-a_during*(t - tk)) - D**2/2*dFk/a_during

def after(t, a_during, a_after, D, dFk, tk, dtk, dsitk):
    return ((dsitk + D**2/2*dFk/a_during)*np.exp(-a_during * dtk) - D**2/2*dFk/a_during) * np.exp(-a_after * (t - (tk + dtk)))

def response(t, a_during, a_after, D, dFs, ts, tsi, dts, dtaus):

    dsi = np.zeros(len(t))
    dsio = dsi[0]
    j = 0
    
    for i, ti in enumerate(t):
        
        if ts[j] <= ti < ts[j] + dts[j]:
            dsi[i] = during(ti, a_during, D, dFs[j], ts[j], dts[j], dsio)

        elif ts[j] + dts[j] <= ti <= ts[j] + dtaus[j]:
            dsi[i] = after(ti, a_during, a_after, D, dFs[j], ts[j], dts[j], dsio)

            if ti == ts[j+1]: 
                dsio = dsi[i]
                j += 1
        
    return dsi

def npdate2date(t):
    pt = pandas.to_datetime(np.datetime_as_string(t))
    return pt


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

cont_paths = glob('../data/N-CONT*.pickle')
seas_paths = glob('../data/N-SEAS*.pickle')
cont_paths.sort()
seas_paths.sort()

cont = []
seas = []

for i, cpath in enumerate(cont_paths):
    with open(cpath, 'rb') as file:
        cont.append(pickle.load(file))
    with open(seas_paths[i], 'rb') as file:
        seas.append(pickle.load(file))

si_datas = []
for i, _ in enumerate(cont):
    si_datas.append({})
    si_datas[i]['dSI'] = cont[i]['si'] - seas[i]['si']
    si_datas[i]['control'] = cont[i]['si']
    si_datas[i]['seasonal'] = seas[i]['si']

    dates = [npdate2date(t) for t in cont[i]['t']]

    si_datas[i]['t'] = dates

with open('../data/delta_F.pickle', 'rb') as file:
    dF_datas = pickle.load(file)

mistral_data = delta_F.precond_data

with open('../data/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

D = depth_data['d']
a_during = alpha_data['alpha']
a_after = alpha_after_data['alpha']

##############################################################################


for l, dF_data in enumerate(dF_datas):

    si_data = si_datas[l]

    precond_data = {}
    precond_data['events'] = np.array(mistral_data['events'])[dF_data['mistral_ind']]
    precond_data['duration'] = np.array(mistral_data['duration'])[dF_data['mistral_ind']]
    precond_data['period'] = np.array(mistral_data['period'])[dF_data['mistral_ind']]

    # for j, event in enumerate(precond_data['events'][:-1:]):
        # print(event, precond_data['events'][j+1][0], (precond_data['events'][j+1][0] - event[-1]).days)

    k = len(precond_data['events'])

    dFs = dF_data['dF']
    dts = precond_data['duration']
    dtaus = precond_data['period']

    dsi_start = precond_data['events'][0][0]
    dsi_ref_date = datetime.datetime(dsi_start.year, dsi_start.month, dsi_start.day, 12)
    t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
    for i, ti in enumerate(t):
        if ti < 0:
            t[i] = 0

    dates = [dtime.date() for dtime in si_data['t']]
    tsi = []
    print(l)
    for i, event in enumerate(precond_data['events']):
        tsi.append(dates.index(event[0]))

    tsi.append(-1)
    ts = t[tsi]

    dtaus[-1] = ts[-1] - ts[-2]

    dsi = response(t, a_during, a_after, D, dFs, ts, tsi, dts, dtaus)

    # restrat_ind = []
    # for i, event in enumerate(mistral_data['events']):
        # if event[0] > precond_data['events'][-1][-1]:
            # restrat_ind.append(i)

    # rdFs = np.append(np.array(dFs), np.array(dF_data_all['dF'])[restrat_ind])
    # rdts = np.append(np.array(dts), np.array(mistral_data['duration'])[restrat_ind])
    # rdtaus = np.append(np.array(dtaus), np.array(mistral_data['period'])[restrat_ind])

    # combined = precond_data['events'] + list(np.array(mistral_data['events'])[restrat_ind])
    # rtsi = []
    # for i, event in enumerate(combined):
        # rtsi.append(dates.index(event[0]))

    # rtsi.append(-1)
    # rts = t[rtsi]

    # rdtaus[-1] = rts[-1] - rts[-2]

    # rdsi = response(t, a_during, a_after, D, rdFs, rts, rtsi, rdts, rdtaus)

    ##############################################################################

    print(np.min(si_data['control']))
    print(np.max(si_data['control']))
    print(si_data['dSI'])

    dates = si_data['t']

    fig_height = 16 #actually width
    fig_width = 8 #actually height

    fig, ax = plt.subplots(2, 1, dpi=300, figsize=(fig_height, fig_width))

    fsize = 18
    tsize = 22

    ax[0].plot(dates, dsi + si_data['seasonal'], color='tab:blue', linestyle='--', label='Simplified model + Seasonal')
    ax[0].plot(dates, si_data['seasonal'], color='tab:blue', label='Seasonal')
    ax[0].plot(dates, si_data['control'], color='black', label='Control')

    plot_0_ymax = np.max(si_data['control'])*1.1

    ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
    ax[0].set_ylim([0, plot_0_ymax])

    ax[1].plot(dates, dsi, color='tab:blue', linestyle='--', label='Simplified model')
    ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')

    y_max = np.max(si_data['dSI'])*2
    y_min = np.min(si_data['dSI'])*1.1

    ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
    ax[1].set_xlabel('Time $days$', fontsize=fsize)
    ax[1].set_ylim([y_min, y_max])

    for i, axis in enumerate(ax):
        axis.tick_params(axis='x', labelsize=fsize)
        axis.tick_params(axis='y', labelsize=fsize)
        axis.set_xlim([dates[0], dates[-1]])

        if i == 0:
            h0 = 0
            h1 = plot_0_ymax - h0
        else:
            h0 = y_min
            h1 = y_max - h0

        for event in precond_data['events']:
            if event == precond_data['events'][0]:
                rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
            else:
                rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
            axis.add_patch(rect)

        # for event in np.array(mistral_data['events'])[restrat_ind]:
            # if event == np.array(mistral_data['events'])[restrat_ind][0]:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:red', alpha=.25, label='Post deep conv. Mistrals')
            # else:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:red', alpha=.25)
            # axis.add_patch(rect)
            
        # axis.legend(fontsize=fsize-4)

    ax[0].legend(ncol=2, fontsize=fsize-2)
    ax[1].legend(ncol=3, fontsize=fsize-2, loc='lower left')

    fig.suptitle('Simple model with varying Mistrals', fontsize=tsize)

    fig.text(0.015, 0.95, '(a)', fontweight='bold', fontsize=fsize)
    fig.text(0.015, 0.49, '(b)', fontweight='bold', fontsize=fsize)

    fig.tight_layout()
    fig.savefig('../plots/dsi_variable_mistrals_' + str(l) + '.png')
