import pickle
import numpy as np
from datetime import datetime
import pandas
from glob import glob

##############################################################################

def delta_F(dSI_end, dSI_beg, alpha, dt, D):
    return (dSI_end - dSI_beg*np.exp(-alpha * dt)) * 2 * alpha / (D**2 * (np.exp(-alpha * dt) - 1))

def npdate2date(t):
    pt = pandas.to_datetime(np.datetime_as_string(t))
    return pt.date()
    
##############################################################################
# load data

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

alpha = alpha_data['alpha']

with open('../data/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

D = depth_data['d']

cont_paths = glob('../data/N-CONT*.pickle')
seas_paths = glob('../data/N-SEAS*.pickle')
cont_paths.sort()
seas_paths.sort()

cont = []
seas = []

for i, cpath in enumerate(cont_paths):
    with open(cpath, 'rb') as file:
        cont.append(pickle.load(file))
    with open(seas_paths[i], 'rb') as file:
        seas.append(pickle.load(file))

with open('../data/mistral_attributes_new.pickle', 'rb') as file:
    precond_data = pickle.load(file)


##############################################################################
# calculations

si_datas = []
for i, _ in enumerate(cont):
    si_datas.append({})
    si_datas[i]['dSI'] = cont[i]['si'] - seas[i]['si']
    si_datas[i]['t'] = cont[i]['t']

# begs = [datetime(1994, 8, 15), datetime(2005, 8, 31)]
# ends = [datetime(1995, 6, 30), datetime(2006, 6, 30)]

begs = [datetime(1993, 8, 15), datetime(2004, 8, 31)]
ends = [datetime(1994, 6, 30), datetime(2005, 6, 30)]

inds = []
for i, beg in enumerate(begs):
    inds.append([])
    for j, event in enumerate(precond_data['events']):
        if beg.date() <= event[0] <= ends[i].date():
            inds[i].append(j)

datas = []
for j, si_data in enumerate(si_datas):

    dts = np.array(precond_data['duration'])[inds[j]]

    dates = [npdate2date(t) for t in si_data['t']]
    t_beg_i = []
    t_end_i = []
    for i, event in enumerate(np.array(precond_data['events'])[inds[j]]):
        t_beg_i.append(dates.index(event[0]))
        t_end_i.append(dates.index(event[-1]))

    dFs = []
    for i, _ in enumerate(np.array(precond_data['events'])[inds[j]]):
        t_beg_i.append(dates.index(event[0]))
        dFs.append(delta_F(si_data['dSI'][t_end_i[i]], si_data['dSI'][t_beg_i[i]], alpha, dts[i], D))

    dSI_beg = []
    for i, _ in enumerate(np.array(precond_data['events'])[inds[j]]):
        dSI_beg.append(si_data['dSI'][t_beg_i[i]])

    data = {'dF': dFs, 'dF_ave': np.mean(dFs), 'dF_std': np.std(dFs), 'dSI_k-1': dSI_beg, 'mistral_ind': inds[j]}

    datas.append(data)

##############################################################################
# saving

with open('../data/delta_F.pickle', 'wb') as file:
    pickle.dump(datas, file)
