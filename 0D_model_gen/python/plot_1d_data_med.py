##############################################################################
# Python3
# laplace
# Tue Mar 10 15:33:22 2020
##############################################################################

import xarray as xr
from glob import glob
import numpy as np
from datetime import datetime
import os

##############################################################################

def reduce_density(arr,i):
    return arr[::i,::i]

def mediterranean_isobar(uas,vas,Psl,time,fname_path=None,wind_min=None,wind_max=None,save=False):
    ##########################################################################
    # Setting up the data
    uas = uas.loc[time]
    vas = vas.loc[time]
    Psl = Psl.loc[time]/100
    
    i = np.arange(0,192)
    j = np.arange(0,300)
    uas_np = np.array(uas[i,j])
    vas_np = np.array(vas[i,j])
    X = np.array(uas.nav_lon_grid_M[i,j])
    Y = np.array(uas.nav_lat_grid_M[i,j])
    Psl = Psl[i,j]
    
    density = 1
    uas_np = reduce_density(uas_np,density)
    vas_np = reduce_density(vas_np,density)
    X = reduce_density(X,density)
    Y = reduce_density(Y,density)
    
    wind_mag = (uas_np**2 +  vas_np**2)**.5
    
    ##########################################################################
    # Setting up the plot

    europe_land_10m = cfeature.NaturalEarthFeature('physical','land','10m',edgecolor='black',facecolor='none',linewidth=2) 
    
    plt.figure(figsize=(12,8),dpi=200)
    ax = plt.axes(projection=ccrs.Orthographic(8.9558,43.555));
    ax = plt.axes(projection=ccrs.Mercator());
    ax.add_feature(europe_land_10m)
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,-2,2,6,10,14,18,22,90])
    gl.ylocator = mticker.FixedLocator([0,37,40,43,46,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
   
    cs = Psl.plot.contour('nav_lon_grid_M','nav_lat_grid_M',ax=ax,transform=ccrs.PlateCarree(),cmap='black',levels=50,linewidths=1); 
    fig = plt.contourf(X,Y,wind_mag,transform=ccrs.PlateCarree(),cmap='rainbow',levels=30);
    plt.quiver(X,Y,uas_np,vas_np,transform=ccrs.PlateCarree(),width=.001,scale=750);
    ax.set_extent([-4,24,35,48],crs=ccrs.PlateCarree());
    
    if wind_min != None:
        cbar = plt.cm.ScalarMappable(cmap='rainbow')
        cbar.set_array(wind_mag)
        cbar.set_clim(wind_min,wind_max)
        plt.colorbar(cbar,fraction=0.030,pad=0.04,label='Wind Magnitude [m/s]',boundaries=np.linspace(wind_min,wind_max,30))
    else:
        plt.colorbar(fig,fraction=0.030,pad=0.04,label='Wind Magnitude [m/s]')
        
    plt.clabel(cs,fontsize=6,fmt='%1.1f')
    
    plt.title(time[0:10]+' Wind/Pressure Plot')
    
    if save:
        plt.savefig(fname_path)
    
    plt.show()
    plt.close()


##############################################################################
# Loading data

data_path = '/home/'+os.environ['USER']+'/Data/PhD/MEDCORDEX-B/'
    
u_path = glob(data_path + '*1D_uas.nc')
v_path = glob(data_path + '*1D_vas.nc')
p_path = glob(data_path + '*1D_psl.nc')

u = xr.open_dataset(u_path[0])
v = xr.open_dataset(v_path[0])
p = xr.open_dataset(p_path[0])

u = u.uas
v = v.vas
p = p.psl
to = list(p.time_counter.values.astype(np.str))

print('Data loaded')

##############################################################################
# Sorting time for 2012 to 2013

begs = [datetime(1993, 8, 15).isoformat(), datetime(2004, 8, 31).isoformat()]
ends = [datetime(1994, 6, 30).isoformat(), datetime(2005, 6, 30).isoformat()]

t = []
for j, beg in enumerate(begs):
    end = ends[j]
    for i in to:
        if i >= beg and i <= end:
            t.append(i)

print('Time of interest found')

##############################################################################
# Getting wind max and min

i = np.arange(57,132)
j = np.arange(91,172)
    
u_np = np.array(u.loc[t[0]:t[-1]][i,j])
v_np = np.array(v.loc[t[0]:t[-1]][i,j])
wind_mag = (u_np**2 + v_np**2)**.5

wmax = np.max(wind_mag)
wmin = np.min(wind_mag)

print('Found wind min and maxes')

##############################################################################
# Plotting

plot_path = '../plots/wind_gol/'

for i in t:
    file_path = plot_path + i[0:10] + '.png'
    mediterranean_isobar(u,v,p,i,file_path,wind_max=wmax,wind_min=wmin,save=True)

