import pickle
import datetime
import numpy as np

##############################################################################

m1994 = [[(28, 8, 1993), (29, 8, 1993), (30, 8, 1993), (1, 9, 1993)],
        [(4, 9, 1993), (5, 9, 1993)],
        [(25, 9, 1993), (26, 9, 1993), (27, 9, 1993), (28, 9, 1993)],
        [(20, 10, 1993), (21, 10, 1993), (22, 10, 1993), (23, 10, 1993), (24, 10, 1993), (25, 10, 1993)],
        [(5, 11, 1993)],
        [(8, 11, 1993), (9, 11, 1993)],
        [(12, 11, 1993), (13, 11, 1993), (14, 11, 1993), (15, 11, 1993), (16, 11, 1993)],
        [(21, 11, 1993), (22, 11, 1993)],
        [(26, 11, 1993), (27, 11, 1993), (28, 11, 1993), (29, 11, 1993), (30, 11, 1993), (1, 12, 1993), (2, 12, 1993)],
        [(5, 12, 1993)],
        [(26, 12, 1993), (27, 12, 1993), (28, 12, 1993)],
        [(8, 1, 1994)],
        [(11, 1, 1994), (12, 1, 1994)],
        [(17, 1, 1994), (18, 1, 1994), (19, 1, 1994), (20, 1, 1994), (21, 1, 1994), (22, 1, 1994), (23, 1, 1994), (24, 1, 1994), (25, 1, 1994), (26, 1, 1994), (27, 1, 1994), (28, 1, 1994), (29, 1, 1994), (30, 1, 1994), (31, 1, 1994)],
        [(5, 2, 1994), (6, 2, 1994), (7, 2, 1994), (8, 2, 1994), (9, 2, 1994), (10, 2, 1994), (11, 2, 1994)],
        [(19, 2, 1994)],
        [(6, 3, 1994), (7, 3, 1994), (8, 3, 1994)],
        [(13, 3, 1994), (14, 3, 1994), (15, 3, 1994), (16, 3, 1994), (17, 3, 1994), (18, 3, 1994)],
        [(20, 3, 1994), (21, 3, 1994), (22, 3, 1994)],
        [(26, 3, 1994), (27, 3, 1994)],
        [(3, 4, 1994)]]

m2005 = [[(15, 9, 2004), (16, 9, 2004)],
        [(i, 9, 2004) for i in range(19, 29)],
        [(26, 10, 2004)],
        [(1, 11, 2004), (2, 11, 2004)],
        [(i, 11, 2004) for i in range(5, 21)],
        [(23, 11, 2004), (24, 11, 2004)],
        [(27, 11, 2004)],
        [(i, 12, 2004) for i in range(4, 11)],
        [(i, 12, 2004) for i in range(18, 32)] + [(i, 1, 2005) for i in range(1, 4)],
        [(i, 1, 2005) for i in range(18, 32)] + [(i, 2, 2005) for i in range(1, 4)],
        [(i, 2, 2005) for i in range(11, 29)],
        [(i, 3, 2005) for i in range(4, 14)],
        [(8, 4, 2005)]]

mistrals_tup = m1994

mistrals = {'events': [], 'period': [], 'duration': []}
for i, event in enumerate(mistrals_tup[:-1:]):
    temp = []
    for date in event:
        temp.append(datetime.date(date[2], date[1], date[0]))

    mistrals['events'].append(temp)
    mistrals['duration'].append(len(temp))

    temp_tup = mistrals_tup[i+1][0]
    temp_per = datetime.date(temp_tup[2], temp_tup[1], temp_tup[0])
    mistrals['period'].append((temp_per - temp[0]).days)
    # print(temp[0], temp_per, mistrals['period'][-1])

print(np.mean(mistrals['period']), np.mean(mistrals['duration']))
print(np.sum(mistrals['duration']))

print((mistrals['events'][-1][0] - mistrals['events'][0][0]).days)
print()

mistrals_tup = m2005

mistrals2 = {'events': [], 'period': [], 'duration': []}

for i, event in enumerate(mistrals_tup[:-1:]):
    temp = []
    for date in event:
        temp.append(datetime.date(date[2], date[1], date[0]))

    mistrals['events'].append(temp)
    mistrals['duration'].append(len(temp))

    temp_tup = mistrals_tup[i+1][0]
    temp_per = datetime.date(temp_tup[2], temp_tup[1], temp_tup[0])
    mistrals['period'].append((temp_per - temp[0]).days)
    # print(temp[0], temp_per, mistrals['period'][-1])

    mistrals2['events'].append(temp)
    mistrals2['duration'].append(len(temp))

    temp_tup = mistrals_tup[i+1][0]
    temp_per = datetime.date(temp_tup[2], temp_tup[1], temp_tup[0])
    mistrals2['period'].append((temp_per - temp[0]).days)

print(np.mean(mistrals2['period']), np.mean(mistrals2['duration']))
print(np.sum(mistrals2['duration']))
print((mistrals2['events'][-1][0] - mistrals2['events'][0][0]).days)

##############################################################################

with open('../data/mistral_attributes_new.pickle', 'wb') as file:
    pickle.dump(mistrals, file)
