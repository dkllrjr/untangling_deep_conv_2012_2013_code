import pandas
import pickle

##############################################################################
# pandas table

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table

##############################################################################

with open('../data/sensitivity.pickle', 'rb') as file:
    sens_data = pickle.load(file)

with open('../data/sensitivity_var.pickle', 'rb') as file:
    sens_var_data = pickle.load(file)

##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table*', 'begin{table*}[t]', 'tophline', 'middlehline', 'bottomhline']

##############################################################################
# full year

temp = {}
for key in sens_data.keys():
    temp[key] = [sens_data[key]]

sens = pandas.DataFrame.from_dict(temp)

# table_header = ['r0', 'r1', 'r2', 'r3', 'r4', 'r5']
table_header = ['r0', 'r1', 'r2', 'r3']

table = sens.to_latex(index=False, float_format='%.2e', label='tab:model_sens', caption='', header=table_header)

# table_header_rep = ['$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\delta F}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\alpha_d}$',
        # '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\alpha_a}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta t}$',
        # '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta \\tau}$', '$\\frac{\\partial \\delta SI_k}{\\partial k}|_{k=16}$']

table_header_rep = ['$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\delta F}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta t}$',
        '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta \\tau}$', '$\\frac{\\partial \\delta SI_k}{\\partial k}|_{k=16}$']

table = hlines(table, words_to_replace, replacing_words)

for i, item in enumerate(table_header):
    table = table.replace(item, table_header_rep[i])

print(table)

##############################################################################

with open('../tables/sensitivity.tex', 'w') as file:
    file.writelines(table)

##############################################################################
# full year

temp = {}
for key in sens_var_data.keys():
    temp[key] = [sens_var_data[key]]

sens = pandas.DataFrame.from_dict(temp)

# table_header = ['r0', 'r1', 'r2', 'r3', 'r4', 'r5']
table_header = ['r0', 'r1', 'r2', 'r3', 'r4', 'r5']

table = sens.to_latex(index=False, float_format='%.2e', label='tab:model_sens_var', caption='', header=table_header)

# table_header_rep = ['$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\delta F}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\alpha_d}$',
        # '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\alpha_a}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta t}$',
        # '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta \\tau}$', '$\\frac{\\partial \\delta SI_k}{\\partial k}|_{k=16}$']

table_header_rep = ['$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\delta F} \sigma_{\delta F}$', '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta t} \sigma_{\Delta t}$',
        '$\\frac{\\partial \\delta SI_{\\infty}}{\\partial \\Delta \\tau} \sigma_{\Delta \\tau}$',
        '$\sigma_{\delta F}$', '$\sigma_{\Delta t}$', '$\sigma_{\Delta \\tau}$']

table = hlines(table, words_to_replace, replacing_words)

for i, item in enumerate(table_header):
    table = table.replace(item, table_header_rep[i])

print(table)

##############################################################################

with open('../tables/sensitivity_var.tex', 'w') as file:
    file.writelines(table)
