##############################################################################

##############################################################################

import pickle
import numpy as np
import datetime
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import csv
from matplotlib.patches import Rectangle
from glob import glob
import pandas

##############################################################################
# functions

def npdate2date(t):
    pt = pandas.to_datetime(np.datetime_as_string(t))
    return pt


def normalized_SI(tpppp, alpha):
    
    dt = 5.6875
    dtau = 10.875
    # nSI = -(np.exp(-alpha * dt * tpp) - 1) / (np.exp(-alpha * dt) - 1)
    
    nSI = (np.exp(-alpha * tpppp *(dtau - dt)) - 1) / (np.exp(-alpha * (dtau - dt)) - 1)
    
    return nSI

##############################################################################
# load data

with open('../data/delta_F.pickle', 'rb') as file:
    dF_datas = pickle.load(file)

cont_paths = glob('../data/N-CONT*.pickle')
seas_paths = glob('../data/N-SEAS*.pickle')
cont_paths.sort()
seas_paths.sort()

cont = []
seas = []

for i, cpath in enumerate(cont_paths):
    with open(cpath, 'rb') as file:
        cont.append(pickle.load(file))
    with open(seas_paths[i], 'rb') as file:
        seas.append(pickle.load(file))

si_datas = []
for i, _ in enumerate(cont):
    si_datas.append({})
    si_datas[i]['dSI'] = cont[i]['si'] - seas[i]['si']
    si_datas[i]['control'] = cont[i]['si']
    si_datas[i]['seasonal'] = seas[i]['si']

    dates = [npdate2date(t) for t in cont[i]['t']]

    si_datas[i]['t'] = dates

with open('../data/delta_F.pickle', 'rb') as file:
    dF_datas = pickle.load(file)

with open('../data/mistral_attributes_new.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

D = depth_data['d']

for l, dF_data in enumerate(dF_datas):

    precond_data = {}
    precond_data['events'] = np.array(mistral_data['events'])[dF_data['mistral_ind']]
    precond_data['duration'] = np.array(mistral_data['duration'])[dF_data['mistral_ind']]
    precond_data['period'] = np.array(mistral_data['period'])[dF_data['mistral_ind']]

##############################################################################
# calculations

    si_data = si_datas[l]
    si_dates = []
    for date in si_data['t']:
        si_dates.append(date.date())

    si_dates = np.array(si_dates)

    # focusing on preconditioning
    good_dates = []
    precond_part = []

    for j, event in enumerate(precond_data['events']):
        good_dates.append(event[0])
        precond_part.append([])
        for i, date in enumerate(si_dates):
            
            if event[-1] <= date <= event[-1] + datetime.timedelta(days=(int(precond_data['period'][j] - precond_data['duration'][j]))):
                precond_part[-1].append(i)

    # just pulling the part during the mistral event
    after_SI = []
    after_SI_t = []

    # for moving average
    fix_alt = [0]
    fix_end = [4]

    # for butter
    # fix_alt = [4]
    # fix_alt2 = [0]
    # fix_beg = [15]

    for i, part in enumerate(precond_part):
        after_SI.append(si_data['dSI'][part])
        after_SI_t.append(np.arange(0, len(part), 1))

        # if i in fix_alt:
            # # for moving average
            # part = [part[0] - 1] + part

            # # for butter
            # # part = part[5::]

            # after_SI.append(si_data['dSI'][part])
            # # after_SI_t.append(np.arange(0, precond_data['period'][i] + 2 - precond_data['duration'][i], 1))
            # after_SI_t.append(np.arange(0, len(part), 1))

        # # for moving average
        # elif i in fix_end:
            # part = part + [part[-1] + 1]
            # after_SI.append(si_data['dSI'][part])
            # # after_SI_t.append(np.arange(0, precond_data['period'][i] + 2 - precond_data['duration'][i], 1))
            # after_SI_t.append(np.arange(0, len(part), 1))


        # # elif i in fix_beg:
            # # part = [part[0] - 1] + part + [part[-1] + 1] + [part[-1] + 2] + [part[-1] + 3] + [part[-1] + 4]
            # # after_SI.append(si_data['dSI'][part])
            # # after_SI_t.append(np.arange(0, precond_data['period'][i] + 2 - precond_data['duration'][i], 1))
            # # after_SI_t.append(np.arange(0, len(part), 1))

        # # elif i in fix_alt2:
            # # # part = part[4::]
            # # print(part)
            # # # print(i)
            # # after_SI.append(si_data['dSI'][part])
            # # # after_SI_t.append(np.arange(0, precond_data['period'][i] + 2 - precond_data['duration'][i], 1))
            # # after_SI_t.append(np.arange(0, len(part), 1))

        # else: 
            # after_SI.append(si_data['dSI'][part])
            # # after_SI_t.append(np.arange(0, precond_data['period'][i] + 1 - precond_data['duration'][i], 1))
            # after_SI_t.append(np.arange(0, len(part), 1))


    # fixing bad starts for moving average
    # fix = [1, 2, 4, 6, 11]

    for i, event in enumerate(after_SI):
        # if i in fix:
            # after_SI[i] = event[1::]
            # after_SI_t[i] = after_SI_t[i][1::] - 1
        after_SI[i] = event[1::]
        after_SI_t[i] = after_SI_t[i][1::] - 1

    # normalizing time and intercept
    nt_SI = []
    nt_SI_t = []

    for i, event in enumerate(after_SI):
        nt_SI.append(event - event[0])  # normalize intercept
        nt_SI_t.append(after_SI_t[i]/after_SI_t[i][-1])  # normalize tim 

    # normalizing height
    nh_SI = []
    nh_SI_t = []

    for i, event in enumerate(nt_SI):
        nh_SI.append(event/(after_SI[i][-1]-after_SI[i][0]))

    nh_SI_t = nt_SI_t

    # removing negative gradient
    pos_SI = []
    pos_SI_t = []
    pos_good_dates = []

    for i, event in enumerate(nh_SI):
        # if np.mean(np.diff(event)) > 0:
        # if not (np.diff(event)<0).any():
        # if not (np.abs(np.gradient(event, nh_SI_t[i])) > 15).any() and precond_data['destrat_flag'][i]:
        # if True:
        # if np.mean(np.gradient(np.gradient(event, nh_SI_t[i]), nh_SI_t[i])) < 0:
        # if precond_data['restrat_flag'][i] and len(event) > 1:
        if len(event) > 1:
            pos_SI.append(event)
            pos_SI_t.append(nh_SI_t[i])
            pos_good_dates.append(good_dates[i])

    ##############################################################################
    # analytical normalized SI

    comp_SI = []
    comp_SI_t = []

    for i, event in enumerate(pos_SI):

        comp_SI += list(event)
        comp_SI_t += list(pos_SI_t[i])

    tpppp = np.arange(0, 1, .01)

    param, pcov = curve_fit(normalized_SI, comp_SI_t, comp_SI, bounds=(0, np.inf))
    print(param)
    perr = np.sqrt(np.diag(pcov))

    nSI = normalized_SI(tpppp, param[0])
    nSIp = normalized_SI(tpppp, param[0]+perr)
    nSIm = normalized_SI(tpppp, param[0]-perr)

    ##############################################################################
    # plotting

    fig, ax = plt.subplots(1, 1, figsize=(8, 6), dpi=200)

    fsize=18
    tsize=22

    for i, event in enumerate(pos_SI):
        # ax.plot(pos_SI_t[i], event, alpha=.5, label=str(i))
        # ax.plot(pos_SI_t[i], event, alpha=.5, label=str(i))
        ax.plot(pos_SI_t[i], event, alpha=.5)

    ax.plot(tpppp, nSI, linestyle='--', linewidth=3, color='black', label='Normalized $\delta$SI$_{fit}$')
    ax.fill_between(tpppp, nSIp, nSIm, color='tab:red', alpha=.5, label='$\\pm \\sigma_{\\delta SI_{fit}}$')

    # ax.set_ylim(-1.1, .1)
    ax.set_xlabel('$t\'\'\'/ (\\Delta \\tau - \\Delta t)$', fontsize=fsize)
    ax.set_ylabel('Normalized $\delta$SI', fontsize=fsize)
    ax.legend(fontsize=fsize-4)
    ax.tick_params(axis="x", labelsize=fsize)
    ax.tick_params(axis="y", labelsize=fsize)

    ax.set_title('Post Mistral Restratification', fontsize=tsize)

    fig.tight_layout()
    fig.savefig('../plots/dsi_mistral_restrat' + str(l) + '.png')

    ##############################################################################
    # save to csv

    with open('../data/dsi_after_curve_fit' + str(l) + '.csv', 'w') as file:
        csvwriter = csv.writer(file, delimiter=',')
        
        csvwriter.writerow(['alpha (days^-1)'])
        csvwriter.writerow(param)

    with open('../data/dsi_after_curve_fit' + str(l) + '.pickle', 'wb') as file:
        csv_data = {'alpha': param[0], 'units': 'days^-1', 'events': pos_good_dates}
        pickle.dump(csv_data, file)
