import numpy as np
import pickle
from glob import glob
import pandas

##############################################################################

def npdate2date(t):
    pt = pandas.to_datetime(np.datetime_as_string(t))
    return pt


def ddSI_dad(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * ( -1/a_d**2 * (np.exp(-a_d*dt) - 1) + 1/a_d * (-dt*np.exp(-a_d*dt)) + 1/a_d * (np.exp(-a_d*dt) - 1) * (-dt * np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) )

def ddSI_ddF(D, a_d, a_a, dt, dtau):
    return D**2/2 * 1/a_d * (np.exp(-a_d*dt) - 1) / (1 - np.exp((a_a - a_d)*dt - a_a*dtau))

def ddSI_daa(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1) * ( (dt - dtau)*np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau) )**2

def ddSI_ddtau(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1) * (-a_a * np.exp((a_a - a_d)*dt - a_a*dtau))/(1 - np.exp((a_a - a_d)*dt - a_a*dtau))**2

def ddSI_ddt(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * (-a_d*np.exp(-a_d*dt) + (np.exp(-a_d*dt) - 1) * ((a_a - a_d)*np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)))

def dSI(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1)/(1 - np.exp((a_a - a_d)*dt - a_a*dtau))

def diff_dSI(D, dF, DF, a_d, Da_d, a_a, Da_a, dt, Dt, dtau, Dtau):
    dF2 = dF + DF
    a_d2 = a_d + Da_d
    a_a2 = a_a + Da_a
    dt2 = dt + Dt
    dtau2 = dtau + Dtau

    diff = {}
    diff['dF'] = (dSI(D, dF2, a_d, a_a, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(DF)
    diff['a_d'] = (dSI(D, dF, a_d2, a_a, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Da_d)
    diff['a_a'] = (dSI(D, dF, a_d, a_a2, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Da_a)
    diff['dt'] = (dSI(D, dF, a_d, a_a, dt2, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Dt)
    diff['dtau'] = (dSI(D, dF, a_d, a_a, dt, dtau2) - dSI(D, dF, a_d, a_a, dt, dtau))/(Dtau)

    return diff

def ddSI_dk(D, dF, a_d, a_a, dt, dtau, k):
    return D**2/2 * dF/a_d * (np.exp(-a_d * dt) - 1) / (1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * (- np.exp(((a_a - a_d)*dt - a_a*dtau)*k)) * ((a_a - a_d)*dt - a_a*dtau)


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

cont_paths = glob('../data/N-CONT*.pickle')
seas_paths = glob('../data/N-SEAS*.pickle')
cont_paths.sort()
seas_paths.sort()

cont = []
seas = []

for i, cpath in enumerate(cont_paths):
    with open(cpath, 'rb') as file:
        cont.append(pickle.load(file))
    with open(seas_paths[i], 'rb') as file:
        seas.append(pickle.load(file))

si_datas = []
for i, _ in enumerate(cont):
    si_datas.append({})
    si_datas[i]['dSI'] = cont[i]['si'] - seas[i]['si']
    si_datas[i]['control'] = cont[i]['si']
    si_datas[i]['seasonal'] = seas[i]['si']

    dates = [npdate2date(t) for t in cont[i]['t']]

    si_datas[i]['t'] = dates

with open('../data/delta_F.pickle', 'rb') as file:
    dF_datas = pickle.load(file)

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

D = depth_data['d']
a_during = alpha_during_data['alpha']
a_after = alpha_after_data['alpha']

##############################################################################

for l, dF_data in enumerate(dF_datas):

    dF = dF_data['dF_ave']

    precond_data = {}
    precond_data['events'] = np.array(mistral_data['events'])[dF_data['mistral_ind']]
    precond_data['duration'] = np.array(mistral_data['duration'])[dF_data['mistral_ind']]
    precond_data['period'] = np.array(mistral_data['period'])[dF_data['mistral_ind']]

    dt = np.mean(precond_data['duration'])
    dtau = np.mean(precond_data['period'])
    k = len(dF_data['dF'])

    sens = {}
    sens['ddSI_ddF'] = ddSI_ddF(D, a_during, a_after, dt, dtau)
    # sens['ddSI_dad'] = ddSI_dad(D, dF, a_during, a_after, dt, dtau)
    # sens['ddSI_daa'] = ddSI_daa(D, dF, a_during, a_after, dt, dtau)
    sens['ddSI_ddt'] = ddSI_ddt(D, dF, a_during, a_after, dt, dtau)
    sens['ddSI_ddtau'] = ddSI_ddtau(D, dF, a_during, a_after, dt, dtau)

    d = .001

    dff = diff_dSI(D, dF, d, a_during, d, a_after, d, dt, d, dtau, d)

    sens['ddSI_dk'] = ddSI_dk(D, dF, a_during, a_after, dt, dtau, k)


    with open('../data/sensitivity' + str(l) + '.pickle', 'wb') as file:
        pickle.dump(sens, file)

    sens_var = {}
    sens_var['ddSI_ddF'] = sens['ddSI_ddF'] * dF_data['dF_std']
    sens_var['ddSI_ddt'] = sens['ddSI_ddt'] * np.std(precond_data['duration'])
    sens_var['ddSI_ddtau'] = sens['ddSI_ddtau'] * np.std(precond_data['period'])

    sens_var['dF_std'] = dF_data['dF_std']
    sens_var['duration_std'] = np.std(precond_data['duration'])
    sens_var['period_std'] = np.std(precond_data['period'])


    with open('../data/sensitivity_var' + str(l) + '.pickle', 'wb') as file:
        pickle.dump(sens_var, file)


    print(sens_var)
