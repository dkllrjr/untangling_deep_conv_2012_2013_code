##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from dask.diagnostics import ProgressBar
import numpy as np

##############################################################################

def find_lat_lon(loc,lat,lon):

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        #    (r0**2 + r1**2 - 2*r0*r1*(np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)))**.5
        a = r0**2 + r1**2
        b = 2*r0*r1
        c = np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)
        d = (a - b*c)
        return d**.5
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            # print(i,len(lat_lon))
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind

##############################################################################
# CONTROL data

## Setting the data paths

#data_path = ['../../NEMO/NEMO-BULK-CONT/OCE/Output/DA/','../../NEMO/NEMO-SEAS2/OCE/Output/DA/']
#save_path = ['../../GOL/cont_gol_conv_','../../GOL/seas_gol_conv_']

#data_path = ['../../NEMO/NEMO-BUTT/']
#save_path = ['../../GOL/seas_gol_conv_']

data_path = ['../../NEMO/NEMO-BUTV/OCE/Output/']
save_path = ['../../GOL/seas_gol_conv_']

for i in range(len(data_path)):

    print(i)
    rho_path = glob(data_path[i] + 'DA/*rho.nc')
    grid_path = glob(data_path[i] + 'HF/*2D.nc')

    #rho_path = glob(data_path[i] + '*rho.nc')
    #grid_path = glob(data_path[i] + '../HF/*2D.nc')
    
    #rho_path = glob(data_path[i] + 'RHO/*rho.nc')
    #grid_path = glob(data_path[i] + '2D/*2D.nc')
    
    rho_path.sort()
    grid_path.sort()
    
    print(rho_path)
    rho = xr.open_mfdataset(rho_path) # vodensity, vovaisala
    grid = xr.open_mfdataset(grid_path) # somixhgt, somxl010
    
    print('Loaded data')
    
    loc = [[42,5]]
    lat = grid.nav_lat.values
    lon = grid.nav_lon.values

    ind = find_lat_lon(loc,lat,lon)
    ind = ind[0]
    
    spacing = 1
    ind_y = [ind[0]-spacing,ind[0]+spacing+1]
    ind_x = [ind[1]-spacing,ind[1]+spacing+1]

    print('Found indices')

    new_rho = rho.isel(y_grid_T=slice(ind_y[0],ind_y[1],1),y_grid_W=slice(ind_y[0],ind_y[1],1),x_grid_T=slice(ind_x[0],ind_x[1],1),x_grid_W=slice(ind_x[0],ind_x[1],1))
    new_grid = grid.isel(x=slice(ind_x[0],ind_x[1],1),y=slice(ind_y[0],ind_y[1],1))
   
    N = new_rho.vovaisala
    Nave = N.mean(dim=['y_grid_W','x_grid_W'])
    delayed = Nave.to_netcdf(save_path[i]+'N.nc',compute=False)
    
    print('N')
    with ProgressBar():
        results = delayed.compute()
    
    ngrid = new_grid.mean(dim=['x','y'])
    delayed = ngrid.to_netcdf(save_path[i]+'grid.nc',compute=False)

    print('grid')
    with ProgressBar():
        results = delayed.compute()

    print('Saved data')

