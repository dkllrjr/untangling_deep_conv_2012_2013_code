##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle

##############################################################################
# Plotting

def gol_si_plot(cont_si, seas_si, snapshots, gmin, gmax, plot_path):

    def gol_si_subplot(ax, grid, gmin, gmax, cbaxes, counter):

        europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
        fsize = 18 
        # ax.add_feature(europe_land_10m)
        
        gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
        gl.xlabels_top = False
        gl.ylabels_right = False
        gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
        gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'size': fsize}
        gl.ylabel_style = {'size': fsize}
        
    #    ax.plot(5,42,transform=ccrs.PlateCarree(),marker='x',color='black',markersize=10,linestyle='',label='42,5')

        levels = np.linspace(gmin,gmax,20)
        m = ax.contourf(grid.nav_lon_grid_W, grid.nav_lat_grid_W, grid, transform=ccrs.PlateCarree(), vmin=gmin, vmax=gmax, levels=levels, extend='both', cmap='gist_rainbow_r')
        
        if counter < 1:
            cbar = plt.colorbar(m, orientation='vertical', cax=cbaxes)
            cbar.ax.tick_params(labelsize=fsize-4)

        if counter%2 == 0:
            tname = str(grid.time_counter.values)[0:10]
            ax.set_title(tname, fontsize=fsize)
        
        ax.set_extent([0,10,38,45], crs=ccrs.PlateCarree());

        ax.add_feature(europe_land_10m)

    fig, ax = plt.subplots(2, len(snapshots), figsize=(14, 5.5), dpi=300, subplot_kw={'projection': ccrs.PlateCarree(5)})
    fsize = 18
    tsize = 22
    
    cbaxes = fig.add_axes([.91, 0.2, 0.01, 0.6]) 
    
    counter = 0
    for i, date in enumerate(snapshots):
        
        gol_si_subplot(ax[0, i], cont_si.loc[date][0], gmin, gmax, cbaxes, counter)
        counter += 1
        gol_si_subplot(ax[1, i], seas_si.loc[date][0], gmin, gmax, cbaxes, counter)
        counter += 1
    
    left = .07
    fig.text(left-.025, .895, '(a)', fontweight='bold', fontsize=fsize)
    fig.text(left-.025, .445, '(b)', fontweight='bold', fontsize=fsize)
    
    x, y = 3.9, 41.05
    rect = Rectangle((x, y), 5.9-x, 42.4-y, fill=False, edgecolor='white', linestyle='--', linewidth=2.25, transform=ccrs.PlateCarree())
    ax[0, 2].add_patch(rect)
    ax[0, 2].text(6, 42, 'DC', color='white', fontweight='extra bold', transform=ccrs.PlateCarree())
    
    fig.suptitle('Stratification Index $m^2/s^2$', y=.98, fontsize=tsize)
    
    fig.subplots_adjust(top = .85, wspace = .33, hspace = .3, left = left)
    # fig.tight_layout(w_pad=.1, h_pad=0)
    fig.savefig(plot_path)
    # plt.show()
    plt.close()

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/'
data_path = [root_path + 'MED/NEMO-BULK-CONT', root_path + 'MED/NEMO-SEAS2']

cont_si_path = glob(data_path[0] + '*')
cont_si_path.sort()
cont_si = xr.open_mfdataset(cont_si_path, combine='by_coords')
cont_si = cont_si.__xarray_dataarray_variable__

seas_si_path = glob(data_path[1] + '*')
seas_si_path.sort()
seas_si = xr.open_mfdataset(seas_si_path, combine='by_coords')
seas_si = seas_si.__xarray_dataarray_variable__

plot_path = root_path + 'PLOTS/gol_si_area.png'

snapshots = ['2012-08-30', '2012-12-11', '2013-02-13', '2013-06-01']

gol_si_plot(cont_si, seas_si, snapshots, 0, 3, plot_path)
