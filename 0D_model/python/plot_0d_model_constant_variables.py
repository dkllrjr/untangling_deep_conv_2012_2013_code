import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle
import datetime

##############################################################################

def during(t, tk, k, a_during, a_after, D, dF, dt, dtau):
    return D**2/2*dF/a_during * ((1 - np.exp(a_during*dt)) * ((1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k))/(1 - np.exp(((a_after - a_during)*dt - a_after*dtau))) - 1) + (1 - np.exp(a_during*(t - tk)))) * np.exp(-a_during*(t - tk))

def after(t, tk, k, a_during, a_after, D, dF, dt, dtau):
    return D**2/2*dF/a_during * (1 - np.exp(a_during*dt)) * (1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k))/(1 - np.exp(((a_after - a_during)*dt - a_after*dtau))) * np.exp((a_after - a_during)*dt - a_after*(t - tk))

def response(t, ts, a_during, a_after, D, dF, dt, dtau):

    dsi = np.zeros(len(t))
    
    for j, _ in enumerate(ts[0:-1]):
        for i, ti in enumerate(t):
            
            if ts[j] <= ti < ts[j] + dt:
                dsi[i] = during(ti, ts[j], j+1, a_during, a_after, D, dF, dt, dtau)
            elif ts[j] + dt <= ti < ts[j] + dtau:
                dsi[i] = after(ti, ts[j], j+1, a_during, a_after, D, dF, dt, dtau)
            elif ti >= ts[j] + dtau:
                dsi[i] = after(ti, ts[j], j+1, a_during, a_after, D, dF, dt, dtau)
        
    return dsi

def event_end(k, a_during, a_after, D, dF, dt, dtau):

    return D**2/2 * dF/a_during * (1 - np.exp(a_during * dt)) * (1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k)) / (1 - np.exp((a_after - a_during)*dt - a_after*dtau)) * np.exp(-a_during*dt)

def end_response(t, ts, dates, a_during, a_after, D, dF, dt, dtau):
    
    dsi = np.zeros(len(t))

    for j, _ in enumerate(ts[0:-1]):
        for i, ti in enumerate(t):
            
            if ts[j] + dt - .5 <= ti <= ts[j] + dt + .5:
                dsi[i] = event_end(j+1, a_during, a_after, D, dF, dt, dtau)
            elif ti > ts[-2] + dt:
                dsi[i] = event_end(j+1, a_during, a_after, D, dF, dt, dtau)
    
    dsi = np.where(dsi == 0, np.nan, dsi)

    dsi_dates = []
    dsi_ends = []

    for i, dsii in enumerate(dsi):
        if not np.isnan(dsii):
            dsi_ends.append(dsii)
            dsi_dates.append(dates[i])

    return dsi_dates, dsi_ends

##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

a_during = alpha_during_data['alpha']
a_after = alpha_after_data['alpha']
dF = dF_data['dF_ave']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
D = si_data['D']

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime.datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

dates = [dtime.date() for dtime in si_data['t']]

ts = []
for i, _ in enumerate(precond_data['events']):
    ts.append(i*dtau)
ts.append(t[-1])

fig_height = 16
fig_width = 8

##############################################################################
# Average values

dsi_dates, dsi_end = end_response(t, ts, dates, a_during, a_after, D, dF, dt, dtau)
dsi = response(t, ts, a_during, a_after, D, dF, dt, dtau)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

ax[0].plot(dates, dsi + si_data['seasonal']['SI'], color='tab:blue', label='Simplified Model + Seasonal')
ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

ax[1].plot(dates, dsi, color='tab:blue', label='Simplified Model')
ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')
ax[1].plot(dsi_dates, dsi_end, linestyle='--', color='tab:blue', label='Max Potential Destrat.', alpha=.5)

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for i, event in enumerate(precond_data['events']):
        if event == precond_data['events'][0]:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25)
        axis.add_patch(rect)

    axis.legend(ncol=2, fontsize=fsize-4)

fig.suptitle('Simplified Model with Constant Attribute Mistrals', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals.png')

##############################################################################
# Changing dF

dFs = [.5*dF, dF, 1.5*dF]
dsis = []
dsie = []
dsid = []
for dFi in dFs:
    dsis.append(response(t, ts, a_during, a_after, D, dFi, dt, dtau))
    temp_d, temp_e = end_response(t, ts, dates, a_during, a_after, D, dFi, dt, dtau)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    str_dF = '{:.2e}'.format(dFs[i])
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], color=mistral_colors[i], label='$\\delta$F = ' + str_dF)

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    str_dF = '{:.2e}'.format(dFs[i])
    ax[1].plot(dates, dsii, label='$\\delta$F = ' + str_dF, color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for i, event in enumerate(precond_data['events']):
        if event == precond_data['events'][0]:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25)
        axis.add_patch(rect)

    # axis.legend(fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[1].legend(ncol=3, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simplified Model with Varying $\\delta$F', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_dF.png')

##############################################################################
# Changing dt

dts = [.25*dt, .5*dt, dt, 1.5*dt]
dsis = []
dsie = []
dsid = []
for dti in dts:
    dsis.append(response(t, ts, a_during, a_after, D, dF, dti, dtau))
    temp_d, temp_e = end_response(t, ts, dates, a_during, a_after, D, dF, dti, dtau)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], color=mistral_colors[i], label='$\\Delta t$ = ' + str(round(dts[i], 2)))

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\Delta t$ = ' + str(round(dts[i], 2)), color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 1
        h1 = 0.125
        space = h1 * 1.25
    else:
        h0 = -.45
        h1 = 0.05
        space = h1 * 1.25
    
    for i, event in enumerate(precond_data['events']):
        for j, dti in enumerate(dts):
            if i == j:
                rect = Rectangle((dsi_ref_date, h0-i*space), datetime.timedelta(days=dti), h1, color=mistral_colors[j], alpha=.25, label='Mistrals $\\Delta t$ = ' + str(round(dts[j], 2)))
                axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=4, fontsize=fsize-4, loc='upper right')
ax[1].legend(ncol=5, fontsize=fsize-4, loc='lower right')

fig.suptitle('Simplified Model with Varying $\\Delta t$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_dt.png')

##############################################################################
# Changing dtau

dtaus = [.75*dtau, dtau, 1.5*dtau]
dsis = []
dsie = []
dsid = []
for dtaui in dtaus:
    tis = []
    for i, _ in enumerate(precond_data['events']):
        tis.append(i*dtaui)
    tis.append(t[-1])
    dsis.append(response(t, tis, a_during, a_after, D, dF, dt, dtaui))
    temp_d, temp_e = end_response(t, tis, dates, a_during, a_after, D, dF, dt, dtaui)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], color=mistral_colors[i], label='$\\Delta \\tau$ = ' + str(round(dtaus[i], 2)))

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\Delta \\tau$ = ' + str(round(dtaus[i], 2)), color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])
    
    if i == 0:
        h0 = 1
        h1 = 0.125
        space = h1 * 1.25
    else:
        h0 = -.5
        h1 = 0.05
        space = h1 * 1.25

    for i, event in enumerate(precond_data['events']):
        for j, dtaui in enumerate(dtaus):
            if i == j:
                rect = Rectangle((dsi_ref_date, h0-space*i), datetime.timedelta(days=dtaui), h1, color=mistral_colors[j], alpha=.25, label='Mistral Period $\\Delta \\tau$ = ' + str(round(dtaui, 2)))
                axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4, loc='upper right')
ax[1].legend(ncol=4, fontsize=fsize-4, loc='lower right')

fig.suptitle('Simplified Model with Varying $\\Delta \\tau$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_dtau.png')

##############################################################################
# Changing a_during

a_ds = [a_during*.5, a_during*.75, a_during, a_during*2]
dsis = []
dsie = []
dsid = []
for a_d in a_ds:
    dsis.append(response(t, ts, a_d, a_after, D, dF, dt, dtau))
    temp_d, temp_e = end_response(t, ts, dates, a_d, a_after, D, dF, dt, dtau)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], label='$\\alpha_{d}$ = ' + str(round(a_ds[i], 4)), color=mistral_colors[i])

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\alpha_d$ = ' + str(round(a_ds[i], 4)), color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for i, event in enumerate(precond_data['events']):
        if event == precond_data['events'][0]:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25)
        axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[1].legend(ncol=4, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simplified Model with Varying $\\alpha_d$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_a_d.png')

##############################################################################
# Changing a_after

a_as = [a_after*.1, a_after*1/3, a_after, a_after*2]
dsis = []
dsie = []
dsid = []
for a_a in a_as:
    dsis.append(response(t, ts, a_during, a_a, D, dF, dt, dtau))
    temp_d, temp_e = end_response(t, ts, dates, a_during, a_a, D, dF, dt, dtau)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], label='$\\alpha_a$ = ' + str(round(a_as[i], 4)), color=mistral_colors[i])

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\alpha_a$ = ' + str(round(a_as[i], 4)), color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for i, event in enumerate(precond_data['events']):
        if event == precond_data['events'][0]:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25)
        axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[1].legend(ncol=4, fontsize=fsize-4, loc='lower right')

fig.suptitle('Simplified Model with Varying $\\alpha_a$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_a_a.png')

##############################################################################
# Changing k

ks = [6, 10, len(precond_data['events']), 24]

dsis = []
dsie = []
dsid = []
for ki in ks:
    tsi = []
    for i in range(ki):
        tsi.append(i*dtau)
    tsi.append(t[-1])
    dsis.append(response(t, tsi, a_during, a_after, D, dF, dt, dtau))
    temp_d, temp_e = end_response(t, tsi, dates, a_during, a_after, D, dF, dt, dtau)
    dsie.append(temp_e)
    dsid.append(temp_d)

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], label='$k$ = ' + str(ks[i]), color=mistral_colors[i])

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$k$ = ' + str(ks[i]), color=mistral_colors[i])
    ax[1].plot(dsid[i], dsie[i], color=mistral_colors[i], alpha=.5, linestyle='--')

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for i, event in enumerate(precond_data['events']):
        if event == precond_data['events'][0]:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((dsi_ref_date + datetime.timedelta(days=dtau*i), h0), datetime.timedelta(days=dt), h1, color='tab:green', hatch='///', alpha=.25)
        axis.add_patch(rect)

    axis.legend(ncol=4, fontsize=fsize-4)

fig.suptitle('Simple Model with Varying $k$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_constant_mistrals_varying_k.png')
