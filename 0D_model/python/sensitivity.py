import numpy as np
import pickle

##############################################################################

def ddSI_dad(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * ( -1/a_d**2 * (np.exp(-a_d*dt) - 1) + 1/a_d * (-dt*np.exp(-a_d*dt)) + 1/a_d * (np.exp(-a_d*dt) - 1) * (-dt * np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) )

def ddSI_ddF(D, a_d, a_a, dt, dtau):
    return D**2/2 * 1/a_d * (np.exp(-a_d*dt) - 1) / (1 - np.exp((a_a - a_d)*dt - a_a*dtau))

def ddSI_daa(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1) * ( (dt - dtau)*np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau) )**2

def ddSI_ddtau(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1) * (-a_a * np.exp((a_a - a_d)*dt - a_a*dtau))/(1 - np.exp((a_a - a_d)*dt - a_a*dtau))**2

def ddSI_ddt(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * (-a_d*np.exp(-a_d*dt) + (np.exp(-a_d*dt) - 1) * ((a_a - a_d)*np.exp((a_a - a_d)*dt - a_a*dtau) )/(1 - np.exp((a_a - a_d)*dt - a_a*dtau)))

def dSI(D, dF, a_d, a_a, dt, dtau):
    return D**2/2 * dF/a_d * (np.exp(-a_d*dt) - 1)/(1 - np.exp((a_a - a_d)*dt - a_a*dtau))

def diff_dSI(D, dF, DF, a_d, Da_d, a_a, Da_a, dt, Dt, dtau, Dtau):
    dF2 = dF + DF
    a_d2 = a_d + Da_d
    a_a2 = a_a + Da_a
    dt2 = dt + Dt
    dtau2 = dtau + Dtau

    diff = {}
    diff['dF'] = (dSI(D, dF2, a_d, a_a, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(DF)
    diff['a_d'] = (dSI(D, dF, a_d2, a_a, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Da_d)
    diff['a_a'] = (dSI(D, dF, a_d, a_a2, dt, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Da_a)
    diff['dt'] = (dSI(D, dF, a_d, a_a, dt2, dtau) - dSI(D, dF, a_d, a_a, dt, dtau))/(Dt)
    diff['dtau'] = (dSI(D, dF, a_d, a_a, dt, dtau2) - dSI(D, dF, a_d, a_a, dt, dtau))/(Dtau)

    return diff

def ddSI_dk(D, dF, a_d, a_a, dt, dtau, k):
    return D**2/2 * dF/a_d * (np.exp(-a_d * dt) - 1) / (1 - np.exp((a_a - a_d)*dt - a_a*dtau)) * (- np.exp(((a_a - a_d)*dt - a_a*dtau)*k)) * ((a_a - a_d)*dt - a_a*dtau)


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

a_during = alpha_during_data['alpha']
a_after = alpha_after_data['alpha']
dF = dF_data['dF_ave']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
D = si_data['D']
k = len(dF_data['dF'])

sens = {}
sens['ddSI_ddF'] = ddSI_ddF(D, a_during, a_after, dt, dtau)
# sens['ddSI_dad'] = ddSI_dad(D, dF, a_during, a_after, dt, dtau)
# sens['ddSI_daa'] = ddSI_daa(D, dF, a_during, a_after, dt, dtau)
sens['ddSI_ddt'] = ddSI_ddt(D, dF, a_during, a_after, dt, dtau)
sens['ddSI_ddtau'] = ddSI_ddtau(D, dF, a_during, a_after, dt, dtau)

d = .001

dff = diff_dSI(D, dF, d, a_during, d, a_after, d, dt, d, dtau, d)

sens['ddSI_dk'] = ddSI_dk(D, dF, a_during, a_after, dt, dtau, k)

##############################################################################

with open('../data/sensitivity.pickle', 'wb') as file:
    pickle.dump(sens, file)

##############################################################################
# with variability

sens_var = {}
sens_var['ddSI_ddF'] = sens['ddSI_ddF'] * dF_data['dF_std']
sens_var['ddSI_ddt'] = sens['ddSI_ddt'] * precond_data['duration_std']
sens_var['ddSI_ddtau'] = sens['ddSI_ddtau'] * precond_data['period_std']

sens_var['dF_std'] = dF_data['dF_std']
sens_var['duration_std'] = precond_data['duration_std']
sens_var['period_std'] = precond_data['period_std']

##############################################################################

with open('../data/sensitivity_var.pickle', 'wb') as file:
    pickle.dump(sens_var, file)
