import pickle
import numpy as np
from datetime import datetime
import pandas

##############################################################################

def delta_F(dSI_end, dSI_beg, alpha, dt, D):
    return (dSI_end - dSI_beg*np.exp(-alpha * dt)) * 2 * alpha / (D**2 * (np.exp(-alpha * dt) - 1))

##############################################################################
# load data

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

##############################################################################
# calculations

D = si_data['D']
alpha = alpha_data['alpha']
dts = mistral_data['duration']

dates = [t.date() for t in si_data['t']]
t_beg_i = []
t_end_i = []
for i, event in enumerate(mistral_data['events']):
    t_beg_i.append(dates.index(event[0]))
    t_end_i.append(dates.index(event[-1]))

dFs = []
for i, _ in enumerate(mistral_data['events']):
    dFs.append(delta_F(si_data['dSI'][t_end_i[i]], si_data['dSI'][t_beg_i[i]], alpha, dts[i], D))

dSI_beg = []
for i, _ in enumerate(mistral_data['events']):
    dSI_beg.append(si_data['dSI'][t_beg_i[i]])

##############################################################################
# saving

data = {'dF': dFs, 'dF_ave': np.mean(dFs), 'dF_std': np.std(dFs), 'dSI_k-1': dSI_beg}

with open('../data/delta_F_all.pickle', 'wb') as file:
    pickle.dump(data, file)
