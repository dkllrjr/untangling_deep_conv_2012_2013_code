import pandas
import pickle

##############################################################################
# pandas table

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table

##############################################################################

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table', 'begin{table}[t]', 'tophline', 'middlehline', 'bottomhline']

##############################################################################
# full year

mid_i = int(len(dF_data['dF'])/2) - 1

dates = []
for event in precond_data['events']:
    dates.append(event[0])

data_for_table = {}
data_for_table['Start Date_0'] = dates[0:mid_i+1]
data_for_table['val_0'] = dF_data['dF'][0:mid_i+1]
data_for_table['Start Date_1'] = dates[mid_i:-1]
data_for_table['val_1'] = dF_data['dF'][mid_i:-1]

df = pandas.DataFrame(data_for_table)

table_header = ['Date', 'replace', 'Date', 'replace']

table = df.to_latex(index=False, float_format='%.2e', label='tab:mistral_strength', caption='', header = table_header)

table = hlines(table, words_to_replace, replacing_words)
table = table.replace('lrlr', 'lr|lr')
table = table.replace('replace', '$\\delta F_k$')

##############################################################################

print(table)
with open('../tables/dF.tex', 'w') as file:
    file.writelines(table)
