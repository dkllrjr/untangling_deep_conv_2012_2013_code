import plot_spaghetti_after as psa
import plot_spaghetti_during as psd
import plot_0d_model_example as pex
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

##############################################################################

fig, ax = plt.subplots(1, 3, figsize=(18, 4), dpi=300)

fsize = 20
tsize = 24

# spaghetti

for i, event in enumerate(psd.neg_SI):
    ax[0].plot(psd.neg_SI_t[i], event, alpha=.5)

for i, event in enumerate(psa.pos_SI):
    ax[1].plot(psa.pos_SI_t[i], event, alpha=.5)

# during simple model

ax[0].plot(psd.tpp, psd.nSI, linestyle='--', linewidth=3, color='black', label='$\delta$SI$_{fit}$')

ax[0].fill_between(psd.tpp, psd.nSIp, psd.nSIm, color='tab:red', alpha=.5, label='$\\pm \\sigma_{\\delta SI_{fit}}$')

ax[0].set_ylim(-1.1, .1)
ax[0].legend(fontsize=fsize-2)
ax[0].tick_params(axis="x", labelsize=fsize)
ax[0].tick_params(axis="y", labelsize=fsize)
ax[0].set_xlabel('$t\' / \Delta t$', fontsize=fsize)
ax[0].set_ylabel('Normalized $\delta$SI$_\\ast$', fontsize=fsize)

ax[0].set_title('Mistral induced destratification', fontsize=tsize)

# after simple model

ax[1].plot(psa.tpppp, psa.nSI, linestyle='--', linewidth=3, color='black', label='$\delta$SI$_{fit}$')
ax[1].fill_between(psa.tpppp, psa.nSIp, psa.nSIm, color='tab:red', alpha=.5, label='$\\pm \\sigma_{\\delta SI_{fit}}$')

# [1]ax.set_ylim(-1.1, .1)
ax[1].legend(fontsize=fsize-2)
ax[1].tick_params(axis="x", labelsize=fsize)
ax[1].tick_params(axis="y", labelsize=fsize)
ax[1].set_xlabel('$t\'\'\'/ (\\Delta \\tau - \\Delta t)$', fontsize=fsize)
ax[1].set_ylabel('Normalized $\delta$SI$_\dagger$', fontsize=fsize)

ax[1].set_title('Post Mistral restratification', fontsize=tsize)

# example plot

ax[2].plot(pex.t, pex.dsi, color='black', linestyle=':', label='Affected by next Mistral')
ax[2].plot(pex.t, np.where(pex.t > pex.dtau, np.nan, pex.dsi), color = 'black')
ax[2].set_ylim([pex.y_min, pex.y_max])
ax[2].set_xlim([pex.t[0], 20])
ax[2].tick_params(axis='x', labelsize=fsize)
ax[2].tick_params(axis='y', labelsize=fsize)
ax[2].legend(fontsize=fsize-2)
ax[2].set_xlabel('Time $days$', fontsize=fsize)
ax[2].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)

rect = Rectangle((0, pex.y_min), pex.dt, pex.y_max-pex.y_min, color='tab:green', alpha=.25, label='Mistral')
ax[2].add_patch(rect)

rect = Rectangle((pex.dt, pex.y_min), pex.dtau-pex.dt, pex.y_max-pex.y_min, color='tab:blue', alpha=.25, label='After Mistral')
ax[2].add_patch(rect)

ax[2].set_title('Example Mistral response', fontsize=tsize)

fig.text(0.02, 0.95, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(0.36, 0.95, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(0.67, 0.95, '(c)', fontweight='bold', fontsize=fsize+2)

# fig.subplots_adjust(left=0.5)
# fig.tight_layout()
fig.savefig('../plots/dsi_mistral_alphas.png', bbox_inches='tight')
