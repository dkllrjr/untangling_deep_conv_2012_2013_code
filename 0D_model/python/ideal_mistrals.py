import pickle

##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    after_data = pickle.load(file)

##############################################################################

print('Ideal events for during Mistral')
print(during_data['events'],'\n\n')

print('Ideal events for after Mistral')
print(after_data['events'],'\n\n')

