import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle
import datetime

##############################################################################

def during(t, a_during, D, dFk, tk, dtk, dsitk):
    return (dsitk + D**2/2*dFk/a_during)*np.exp(-a_during*(t - tk)) - D**2/2*dFk/a_during

def after(t, a_during, a_after, D, dFk, tk, dtk, dsitk):
    return ((dsitk + D**2/2*dFk/a_during)*np.exp(-a_during * dtk) - D**2/2*dFk/a_during) * np.exp(-a_after * (t - (tk + dtk)))

def response(t, a_during, a_after, D, dFs, ts, tsi, dts, dtaus):

    dsi = np.zeros(len(t))
    dsio = dsi[0]
    j = 0
    
    for i, ti in enumerate(t):
        
        if ts[j] <= ti < ts[j] + dts[j]:
            dsi[i] = during(ti, a_during, D, dFs[j], ts[j], dts[j], dsio)
        elif ts[j] + dts[j] <= ti <= ts[j] + dtaus[j]:
            dsi[i] = after(ti, a_during, a_after, D, dFs[j], ts[j], dts[j], dsio)
            if ti == ts[j+1]: 
                dsio = dsi[i]
                j += 1
        
    return dsi


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/delta_F_all.pickle', 'rb') as file:
    dF_data_all = pickle.load(file)

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

D = si_data['D']
a_during = alpha_data['alpha']
a_after = alpha_after_data['alpha']
k = len(precond_data['events'])

dFs = dF_data['dF']
dts = precond_data['duration']
dtaus = precond_data['period']

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime.datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

dates = [dtime.date() for dtime in si_data['t']]
tsi = []
for i, event in enumerate(precond_data['events']):
    tsi.append(dates.index(event[0]))

tsi.append(-1)
ts = t[tsi]

dtaus[-1] = ts[-1] - ts[-2]

dsi = response(t, a_during, a_after, D, dFs, ts, tsi, dts, dtaus)

restrat_ind = []
for i, event in enumerate(mistral_data['events']):
    if event[0] > precond_data['events'][-1][-1]:
        restrat_ind.append(i)

rdFs = np.append(np.array(dFs), np.array(dF_data_all['dF'])[restrat_ind])
rdts = np.append(np.array(dts), np.array(mistral_data['duration'])[restrat_ind])
rdtaus = np.append(np.array(dtaus), np.array(mistral_data['period'])[restrat_ind])

combined = precond_data['events'] + list(np.array(mistral_data['events'])[restrat_ind])
rtsi = []
for i, event in enumerate(combined):
    rtsi.append(dates.index(event[0]))

rtsi.append(-1)
rts = t[rtsi]

rdtaus[-1] = rts[-1] - rts[-2]

rdsi = response(t, a_during, a_after, D, rdFs, rts, rtsi, rdts, rdtaus)

fig_height = 16 #actually width
fig_width = 8 #actually height

##############################################################################

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=300, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

ax[0].plot(dates, dsi + si_data['seasonal']['SI'], color='tab:blue', label='Simplified model + Seasonal')
ax[0].plot(dates, rdsi + si_data['seasonal']['SI'], color='tab:blue', linestyle='--', label='Post deep conv.')
ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

ax[1].plot(dates, dsi, color='tab:blue', label='Simplified model')
ax[1].plot(dates, rdsi, color='tab:blue', linestyle='--', label='Post deep conv.')
ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for event in precond_data['events']:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
        axis.add_patch(rect)

    for event in np.array(mistral_data['events'])[restrat_ind]:
        if event == np.array(mistral_data['events'])[restrat_ind][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:red', alpha=.25, label='Post deep conv. Mistrals')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:red', alpha=.25)
        axis.add_patch(rect)
        
    # axis.legend(fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-2)
ax[1].legend(ncol=3, fontsize=fsize-2, loc='upper center')

fig.suptitle('Simple model with varying Mistrals', fontsize=tsize)

fig.text(0.015, 0.95, '(a)', fontweight='bold', fontsize=fsize)
fig.text(0.015, 0.49, '(b)', fontweight='bold', fontsize=fsize)

fig.tight_layout()
fig.savefig('../plots/dsi_variable_mistrals.png')

##############################################################################
# Changing a_after

a_as = [a_after*.1, a_after*1/3, a_after, a_after*2]
dsis = []
for a_a in a_as:
    dsis.append(response(t, a_during, a_a, D, dFs, ts, tsi, dts, dtaus))

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], label='$\\alpha_a$ = ' + str(round(a_as[i], 4)), color=mistral_colors[i])

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\alpha_a$ = ' + str(round(a_as[i], 4)), color=mistral_colors[i])

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for event in precond_data['events']:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
        axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[1].legend(ncol=4, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simplified Model with Variable Mistrals and Varying $\\alpha_a$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_varying_mistrals_varying_a_a.png')

##############################################################################
# Changing a_after

a_as = [a_after*.1, a_after*1/3, a_after, a_after*2]
dsis = []
for a_a in a_as:
    dsis.append(response(t, a_during, a_a, D, dFs, ts, tsi, dts, dtaus))

dates = si_data['t']

fig, ax = plt.subplots(1, 1, dpi=200, figsize=(fig_height, fig_width/1.75))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

for i, dsii in enumerate(dsis):
    ax.plot(dates, dsii, label='$\\alpha_a$ = ' + str(round(a_as[i], 4)), color=mistral_colors[i])

ax.plot(dates, si_data['dSI'], color='black', label='NEMO')
ax.plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax.set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax.set_xlabel('Time $days$', fontsize=fsize)
ax.set_ylim([y_min, y_max])

ax.tick_params(axis='x', labelsize=fsize)
ax.tick_params(axis='y', labelsize=fsize)
ax.set_xlim([dates[0], dates[-1]])

if i == 0:
    h0 = 0
    h1 = plot_0_ymax - h0
else:
    h0 = y_min
    h1 = y_max - h0

for event in precond_data['events']:
    if event == precond_data['events'][0]:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
    else:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
    ax.add_patch(rect)

ax.legend(ncol=4, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simplified Model with Variable Mistrals and Varying $\\alpha_a$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_varying_mistrals_varying_a_a_poster.png')

##############################################################################
# Changing a_during

a_ds = [a_during*.5, a_during*.75, a_during, a_during*2]
dsis = []
for a_d in a_ds:
    dsis.append(response(t, a_d, a_after, D, dFs, ts, tsi, dts, dtaus))

dates = si_data['t']

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(fig_height, fig_width))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

for i, dsii in enumerate(dsis):
    ax[0].plot(dates, dsii + si_data['seasonal']['SI'], label='$\\alpha_d$ = ' + str(round(a_ds[i], 4)), color=mistral_colors[i])

ax[0].plot(dates, si_data['control']['SI'], color='black', label='Control')

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim([0, plot_0_ymax])

for i, dsii in enumerate(dsis):
    ax[1].plot(dates, dsii, label='$\\alpha_d$ = ' + str(round(a_ds[i], 4)), color=mistral_colors[i])

ax[1].plot(dates, si_data['dSI'], color='black', label='NEMO')
ax[1].plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to Overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_xlabel('Time $days$', fontsize=fsize)
ax[1].set_ylim([y_min, y_max])

for i, axis in enumerate(ax):
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)
    axis.set_xlim([dates[0], dates[-1]])

    if i == 0:
        h0 = 0
        h1 = plot_0_ymax - h0
    else:
        h0 = y_min
        h1 = y_max - h0

    for event in precond_data['events']:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
        axis.add_patch(rect)

    # axis.legend(ncol=2, fontsize=fsize-4)

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[1].legend(ncol=4, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simplified Model with Variable Mistrals and Varying $\\alpha_d$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_varying_mistrals_varying_a_d.png')

##############################################################################
# Changing a_during

a_ds = [a_during*.5, a_during*.75, a_during, a_during*2]
dsis = []
for a_d in a_ds:
    dsis.append(response(t, a_d, a_after, D, dFs, ts, tsi, dts, dtaus))

dates = si_data['t']

fig, ax = plt.subplots(1, 1, dpi=200, figsize=(fig_height, fig_width/1.75))

fsize = 18
tsize = 22

mistral_colors = ['tab:red', 'tab:orange', 'tab:green', 'tab:blue']

plot_0_ymax = np.max(si_data['control']['SI'])*1.1

for i, dsii in enumerate(dsis):
    ax.plot(dates, dsii, label='$\\alpha_d$ = ' + str(round(a_ds[i], 4)), color=mistral_colors[i])

ax.plot(dates, si_data['dSI'], color='black', label='NEMO')
ax.plot(dates, -si_data['seasonal']['SI'], linestyle=':', color='black', label='Stratification to overcome')

y_max = np.max(si_data['dSI'])*2
y_min = np.min(si_data['dSI'])*1.1

ax.set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax.set_xlabel('Time $days$', fontsize=fsize)
ax.set_ylim([y_min, y_max])

ax.tick_params(axis='x', labelsize=fsize)
ax.tick_params(axis='y', labelsize=fsize)
ax.set_xlim([dates[0], dates[-1]])

if i == 0:
    h0 = 0
    h1 = plot_0_ymax - h0
else:
    h0 = y_min
    h1 = y_max - h0

for event in precond_data['events']:
    if event == precond_data['events'][0]:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistrals')
    else:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + datetime.timedelta(days=1), h1, color='tab:green', alpha=.25)
    ax.add_patch(rect)

ax.legend(ncol=4, fontsize=fsize-4, loc='upper center')

fig.suptitle('Simple model with variable Mistrals and varying $\\alpha_d$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dsi_varying_mistrals_varying_a_d_poster.png')
