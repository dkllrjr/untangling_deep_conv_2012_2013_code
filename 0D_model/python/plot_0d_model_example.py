import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle

##############################################################################

def during(t, a_during, D, dFk, tk, dtk, dsitk):
    return (np.heaviside(t - tk, 1) - np.heaviside(t - (tk + dtk), 0)) * ((dsitk + D**2/2*dFk/a_during)*np.exp(-a_during*(t - tk)) - D**2/2*dFk/a_during)

def after(t, a_during, a_after, D, dFk, tk, dtk, dsitk, dtauk):
    return (np.heaviside(t - (tk + dtk), 1) - np.heaviside(t - (tk + dtauk), 0)) * (((dsitk + D**2/2*dFk/a_during)*np.exp(-a_during * dtk) - D**2/2*dFk/a_during) * np.exp(-a_after * (t - (tk + dtk))))


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

a_during = alpha_data['alpha']
a_after = alpha_after_data['alpha']
dF = dF_data['dF_ave']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
D = si_data['D']
dsio = 0

##############################################################################
# 1 event look

res = 1000
t = np.linspace(0, 30, res)

dsi = during(t, a_during, D, dF, 0, dt, dsio) + after(t, a_during, a_after, D, dF, 0, dt, dsio, t[-1])

fig, ax = plt.subplots(1, 1, dpi=200, figsize=(10, 6))

fsize = 18
tsize = 22

ax.set_xlabel('Time $days$', fontsize=fsize)
ax.set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)

y_max = 0
y_min = np.min(dsi)*1.05

ax.set_ylim([y_min, y_max])
ax.set_xlim([t[0], t[-1]])

ax.tick_params(axis='x', labelsize=fsize)
ax.tick_params(axis='y', labelsize=fsize)

ax.plot(t, dsi, color='black', linestyle=':', label='Affected by Next Mistral')
ax.plot(t, np.where(t > dtau, np.nan, dsi), color = 'black')

rect = Rectangle((0, y_min), dt, y_max-y_min, color='tab:green', alpha=.25, label='Mistral')
ax.add_patch(rect)

rect = Rectangle((dt, y_min), dtau-dt, y_max-y_min, color='tab:blue', alpha=.25, label='After Mistral')
ax.add_patch(rect)

str_dF = '{:.2e}'.format(dF)

ax.set_title('$\\alpha_d$ = ' + str(round(a_during, 3)) + ', $\\alpha_a$ = ' + str(round(a_after, 3)) + ', $\\delta F$ = ' + str_dF + ', $\\Delta t$ = ' + str(round(dt, 2)) + ',\n $\\Delta \\tau$ = ' + str(round(dtau, 2)) + ', $\\delta SI_o$ = 0', fontsize=fsize)

fig.suptitle('Average Mistral Simple Model Response', fontsize=tsize)

ax.legend(fontsize=fsize-4)

fig.tight_layout()
fig.savefig('../plots/average_destratification_mistral.png')
