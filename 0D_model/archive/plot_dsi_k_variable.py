import pickle
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

##############################################################################

def dsi_response(t, k, tks, dts, dtaus, dFs, alpha, D):
    forcing = 0
    for j in range(0, k):

        print(k,' ',j,'\t',k-j-1)
        period_exp = 0
        for i in range(1, j+1):
            period_exp += dtaus[k-i-1]
            print(k,' ',j,'\t',k-j-1,'\t',k-i-1)

        forcing += dFs[k-j-1] * (1 - np.exp(alpha * dts[k-j-1])) * np.exp(-alpha * period_exp)

    return D**2/2 * forcing/alpha * np.exp(-alpha * (t - tks[-1]))

##############################################################################
# load data

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

##############################################################################
# calculations

D = si_data['D']
alpha = alpha_data['alpha']
dts = precond_data['duration']
dtaus = precond_data['period']
dFs = dF_data['dF']
k = len(precond_data['events'])

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

dates = [dtime.date() for dtime in si_data['t']]
tks_i = []
for i, event in enumerate(precond_data['events']):
    tks_i.append(dates.index(event[0]))

tks = t[tks_i]

dsi = dsi_response(t, k, tks, dts, dtaus, dFs, alpha, D)

##############################################################################
# plotting

plt.plot(dsi)
plt.show()
