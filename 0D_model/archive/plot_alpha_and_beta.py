##############################################################################

##############################################################################

import pickle
import numpy as np
import matplotlib.pyplot as plt
import csv
from datetime import datetime

##############################################################################

def simple_dSI(beta, dt, dtau, k, alpha):
    dSI = beta * (1 - np.exp(-alpha * dtau * k)) / (1 - np.exp(-alpha * dtau)) * np.exp( -alpha * dtau ) 
    return dSI

# def simple_SI(SI_max, SI_min, omega, t, phi):
    # SI = (SI_max - SI_min)/2 * np.cos(omega * (t + phi)) + (SI_max + SI_min)/2
    # return SI

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

with open('../../mistrals/data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_seasonal_sine_curve_fit.csv') as file:
    si_s_cf_reader = csv.reader(file)
    si_s_cf = []
    for row in si_s_cf_reader:
        si_s_cf.append(row)

with open('../data/dsi_beta.pickle', 'rb') as file:
    beta_data = pickle.load(file)

##############################################################################
# calculations

ref_date = datetime(2013, 1, 1, 12)
t = np.array([(date - ref_date).days for date in si_data['t']])

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
dsit = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(dsit):
    if ti < 0:
        dsit[i] = 0

# phi = float(si_s_cf[1][2])
# SI_max = float(si_s_cf[1][0])
# SI_min = float(si_s_cf[1][1])

# omega = 2*np.pi/365.25

dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
k = dsit/dtau
k = np.where(k > 24, 24, k)

alphas = np.array([10**-1.5, 10**-1.25, 10**-1, 10**-.5, 10**0])
alphas_str = ['$10^{-1.5}$','$10^{-1.25}$','$10^{-1}$','$10^{-0.5}$','$10^{0}$']
betas = np.array([-0.10, -0.25, beta_data['average'], -.75])

##############################################################################
# plotting

fig, ax = plt.subplots(2, 1, dpi=200, figsize=(15, 8))

fsize = 18
tsize = 22

for i, alpha in enumerate(alphas):

    beta = betas[2]

    f = simple_dSI(beta, dt, dtau, k, alpha) + si_data['seasonal']['SI']
    ax[0].plot(si_data['t'], f, label='$\\alpha$ = ' + alphas_str[i])

    if i == 0:
        for beta in betas:
            f = simple_dSI(beta, dt, dtau, k, 0.101) + si_data['seasonal']['SI']
            ax[1].plot(si_data['t'], f, label='$\\beta$ = '+str(beta)[0:6])

ax[1].set_xlabel('$\Delta \\tau$ k [days]', fontsize=fsize)

for axis in ax:
    axis.legend(fontsize=fsize-4)
    axis.set_ylim(0, 2)
    axis.set_xlim(si_data['t'][0], si_data['t'][-1])
    axis.set_ylabel('$m^2/s^2$', fontsize=fsize)
    axis.tick_params(axis='x', labelsize=fsize)
    axis.tick_params(axis='y', labelsize=fsize)

ax[1].plot(si_data['t'], si_data['control']['SI'])

ax[0].set_title('$\\beta$ = '+str(betas[2])[0:6], fontsize=fsize)
ax[1].set_title('$\\alpha$ = 0.101', fontsize=fsize)

fig.suptitle('Theoretical Stratification Index', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/alpha_w_seasonal_gol.png')
