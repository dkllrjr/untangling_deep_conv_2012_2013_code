##############################################################################

##############################################################################

import pickle
import numpy as np
import csv
import pandas

##############################################################################

# def beta_k(dSIk, alpha, dtau, k):
    # beta = dSIk * (1 - np.exp(-alpha * dtau)) / (1 - np.exp(-alpha * dtau * k))
    
    # return beta

def beta_k(dSIk, alpha, dtau, k):
    beta = dSIk * (1 - np.exp(-alpha * dtau)) / (1 - np.exp(-alpha * dtau * k)) / np.exp( -alpha * dtau )
    
    return beta

##############################################################################
# loading data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/dsi_curve_fit.csv', 'r') as file:
    dsi_cf_reader = csv.reader(file)
    dsi_cf = []
    for row in dsi_cf_reader:
        dsi_cf.append(row)

##############################################################################

alpha = float(dsi_cf[1][0])
dtau = precond_data['period_ave']

dates = [t.date() for t in si_data['t']]
k_i = []

for i, event in enumerate(precond_data['events']):
    k_i.append(dates.index(event[-1]))

beta = []
beta_dates = []
for i, t in enumerate(k_i):
    beta.append(beta_k(si_data['dSI'][t], alpha, dtau, i+1))
    beta_dates.append(dates[t].strftime('%Y-%m-%d'))

##############################################################################
# saving

data = {'average': np.mean(beta), 'beta': beta}

with open('../data/dsi_beta.pickle', 'wb') as file:
    pickle.dump(data, file)

with open('../data/dsi_beta.csv', 'w') as file:
    csvwriter = csv.writer(file, delimiter=',')

    csvwriter.writerow(['beta (m^2/s^2)'])
    csvwriter.writerow([data['average']])

##############################################################################
# pandas table

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table

##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table*', 'begin{table*}[t]', 'tophline', 'middlehline', 'bottomhline']

##############################################################################
# full year

data_for_table = {}
data_for_table['Start Date_0'] = beta_dates[0:int(len(beta_dates)/2)]
data_for_table['Period_0'] = beta[0:int(len(beta_dates)/2)]
data_for_table['Start Date_1'] = beta_dates[int(len(beta_dates)/2):len(beta_dates)]
data_for_table['Period_1'] = beta[int(len(beta_dates)/2):len(beta_dates)]

df = pandas.DataFrame(data_for_table)

table_header = ['Date', 'replace', 'Date', 'replace']

table = df.to_latex(index=False, float_format='%.3f', label='tab:mistral_year', caption='', header = table_header)

table = hlines(table, words_to_replace, replacing_words)
table = table.replace('lrlr', 'lr|lr')
table = table.replace('replace', '$\\beta_k (m^2/s^2)$')

with open('../tables/betas_precond.tex', 'w') as file:
    file.writelines(table)
