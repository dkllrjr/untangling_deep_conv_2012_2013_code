import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle
from datetime import datetime

##############################################################################

def start(k, a_during, a_after, D, dF, dt, dtau):
    return D**2/2*dF/a_during * ((1 - np.exp(a_during*dt)) * ((1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k))/(1 - np.exp(((a_after - a_during)*dt - a_after*dtau))) - 1))

def middle(k, a_during, a_after, D, dF, dt, dtau):
    return D**2/2*dF/a_during * (1 - np.exp(a_during*dt)) * (1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k))/(1 - np.exp(((a_after - a_during)*dt - a_after*dtau))) * np.exp(-a_during*dt)

def end(k, a_during, a_after, D, dF, dt, dtau):
    return D**2/2*dF/a_during * (1 - np.exp(a_during*dt)) * (1 - np.exp(((a_after - a_during)*dt - a_after*dtau)*k))/(1 - np.exp(((a_after - a_during)*dt - a_after*dtau))) * np.exp((a_after - a_during)*dt - a_after*(dtau))

def looking(k, a_during, a_after, D, dF, dt, dtau):
    
    timing = ['beg', 'mid', 'end']
    dsi = {}
    
    for var in timing:
        dsi[var] = []

    for ki in range(k):  
        ki += 1
        print(ki)
        dsi['beg'].append(start(ki, a_during, a_after, D, dF, dt, dtau))
        dsi['mid'].append(middle(ki, a_during, a_after, D, dF, dt, dtau))
        dsi['end'].append(end(ki, a_during, a_after, D, dF, dt, dtau))

    return dsi
       

##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

a_during = alpha_during_data['alpha']
a_after = alpha_after_data['alpha']
dF = dF_data['dF_ave']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
D = si_data['D']
k = len(dF_data['dF'])

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

dates = [dtime.date() for dtime in si_data['t']]

ts = []
for i, _ in enumerate(precond_data['events']):
    ts.append(i*dtau)

ts.append(t[-1])

dsi_looking = looking(k, a_during, a_after, D, dF, dt, dtau)

