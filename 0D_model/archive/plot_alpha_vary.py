##############################################################################

##############################################################################

import pickle
import numpy as np
import matplotlib.pyplot as plt

##############################################################################

def simple_dSI(beta, dt, dtau, k, alpha):
    dSI = -beta * (1 - np.exp(alpha * dt)) * (1 - np.exp(-alpha * dtau * k)) / (1 - np.exp(-alpha * dtau)) * np.exp(-alpha * dtau * k)

    return dSI

def simple_dSI(beta, dt, dtau, k, alpha):
    dSI = beta * (1 - np.exp(-alpha * dtau * k)) / (1 - np.exp(-alpha * dtau)) 
    return dSI

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

with open('../../mistrals/data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################
# calculations

dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
k = np.arange(0, len(precond_data['events']) + 1, .1)

beta = -.156

alpha = np.arange(.1, .5, .1)

##############################################################################
# plotting

fig,ax = plt.subplots(2, 1, dpi=150, figsize=(10,6))

for a in alpha:
    f = simple_dSI(beta, dt, dtau, k, a)
    ax[0].plot(k, f, label='$\\alpha$ = '+str(a)[0:3])
    ax[1].plot(k*dtau, f, label='$\\alpha$ = '+str(a)[0:3])

ax[0].set_xlabel('k')
ax[1].set_xlabel('t or ($\Delta \\tau$ k) [days]')

for axis in ax:
    axis.legend()
    axis.set_ylabel('$\delta SI$')

ax[0].set_xlim(0, 5)
ax[1].set_xlim(0, 25)

fig.tight_layout()
# fig.savefig('../plots/alpha_gol.png') # no longer used for this script

