import matplotlib.pyplot as plt
import numpy as np
import pickle
from matplotlib.patches import Rectangle
from datetime import datetime

##############################################################################

def during(t, a_during, D, dFk, tk, dtk, dsitk):
    return (dsitk + D**2/2*dFk/a_during)*np.exp(-a_during*(t - tk)) - D**2/2*dFk/a_during

def after(t, a_during, a_after, D, dFk, tk, dtk, dsitk):
    return ((dsitk + D**2/2*dFk/a_during)*np.exp(-a_during * dtk) - D**2/2*dFk/a_during) * np.exp(-a_after * (t - (tk + dtk)))

# def response(t, a_during, a_after, D, dFs, ts, tsi, dts, dtaus):

    # dsi = np.zeros(len(t))
    # dsio = dsi[0]
    # j = 0
    
    # print('i', '\t', 'ti', '\t', 'ts[j+1]', '\t', 'j', '\t', 'dsio', '\t', 'dsi[i]')
    # print('----------------------------------')
    # for i, ti in enumerate(t):
        
        # if ts[j] <= ti < ts[j] + dts[j]:
            # dsi[i] = during(ti, a_during, D, dFs[j], ts[j], dts[j], dsio)
            # print(i, '\t', ti, '\t', ts[j+1], '\t\t', j, '\t', dsio, '\t', dsi[i], '\t during')
        # elif ts[j] + dts[j] <= ti <= ts[j] + dtaus[j]:
            # dsi[i] = after(ti, a_during, a_after, D, dFs[j], ts[j], dts[j], dsio)
            # print(i, '\t', ti, '\t', ts[j+1], '\t\t', j, '\t', dsio, '\t', dsi[i], '\t after')
            # if ti == ts[j+1]: 
                # dsio = dsi[i]
                # print('----------------------')
                # print('i', '\t', 'ti', '\t', 'ts[j+1]', '\t', 'j', '\t', 'dsio', '\t', 'dsi[i]')
                # j += 1
        
    # return dsi

def response(t, ts, a_during, a_after, D, dF, dt, dtau):

    dsi = np.zeros(len(t))
    j = 0
    dsio = dsi[0]
    
    print('i', '\t', 'ti', '\t', 'ts[j+1]', '\t\t', 'j', '\t', 'dsi[i]')
    print('----------------------------------')
    for i, ti in enumerate(t):
        
        if ts[j] < ti < ts[j] + dt:
            dsi[i] = during(ti, a_during, D, dF, ts[j], dt, dsio)
            print(i, '\t', ti, '\t', ts[j+1], '\t\t', j+1, '\t', dsi[i], '\t during')

        elif ts[j] + dt <= ti <= ts[j] + dtau:
            dsi[i] = after(ti, a_during, a_after, D, dF, ts[j], dt, dsio)
            print(i, '\t', ti, '\t', ts[j+1], '\t\t', j+1, '\t', dsi[i], '\t after')
            
        elif ti > ts[j] + dtau and j < 15:
            dsi[i] = after(ti, a_during, a_after, D, dF, ts[j], dt, dsio)
            dsio = dsi[i]
            print('----------------------')
            print('i', '\t', 'ti', '\t', 'ts[j+1]', '\t\t', 'j', '\t', 'dsi[i]')
            print(i, '\t', ti, '\t', ts[j+1], '\t\t', j+1, '\t', dsi[i], '\t after')
            j += 1
    
    return dsi


##############################################################################

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_during_data = pickle.load(file)

with open('../data/dsi_after_curve_fit.pickle', 'rb') as file:
    alpha_after_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################

a_during = alpha_during_data['alpha']
a_after = alpha_after_data['alpha']
dF = dF_data['dF_ave']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
D = si_data['D']

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

dates = [dtime.date() for dtime in si_data['t']]

ts = []
for i, _ in enumerate(precond_data['events']):
    ts.append(i*dtau)

ts.append(t[-1])

dsi = response(t, ts, a_during, a_after, D, dF, dt, dtau)

print('------')
print(np.min(dsi+si_data['seasonal']['SI']))

plt.plot(dsi+si_data['seasonal']['SI'])
plt.plot(si_data['control']['SI'])
plt.show()

plt.plot(dsi)
plt.plot(si_data['dSI'])
plt.ylim([-1, .1])
plt.show()

##############################################################################
# 1 event look

# res = 1000
# t = np.linspace(0, 25, res)

# dsi = destrat(t, a, D, dF, 0, dt, dsio) + restrat(t, a, D, dF, 0, dt, dsio, t[-1])

# fig, ax = plt.subplots(1, 1, dpi=200, figsize=(8, 5))

# fsize = 18
# tsize = 22

# ax.set_xlabel('Time $days$', fontsize=fsize)
# ax.set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)

# y_max = 0
# y_min = np.min(dsi)*1.1

# ax.set_ylim([y_min, y_max])

# ax.tick_params(axis='x', labelsize=fsize)
# ax.tick_params(axis='y', labelsize=fsize)

# ax.plot(t, dsi, color='black', linestyle=':', label='Affected by Next Event')
# ax.plot(t, np.where(t > dtau, np.nan, dsi), color = 'black')

# rect = Rectangle((0, y_min), dt, y_max-y_min, color='tab:green', alpha=.25, label='Mistral')
# ax.add_patch(rect)

# rect = Rectangle((dt, y_min), dtau-dt, y_max-y_min, color='tab:blue', alpha=.25, label='No Mistral')
# ax.add_patch(rect)

# str_dF = '{:.2e}'.format(dF)

# ax.set_title('$\\alpha$ = 0.101, $\\delta F$ = ' + str_dF + ', $\\Delta t$ = ' + str(dt)[0:4] + ',\n $\\Delta \\tau$ = ' + str(dtau)[0:4] + ', $\\delta SI_o$ = 0', fontsize=fsize)

# fig.suptitle('Average Single Event Model Response', fontsize=tsize)

# ax.legend(fontsize=fsize-4)

# fig.tight_layout()
# fig.savefig('../plots/average_destratification_event.png')
