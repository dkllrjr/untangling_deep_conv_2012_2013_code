import pickle
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

##############################################################################

def dsi_k(ti, k, tks, dt, dtau, dF, alpha, D):
    func = 0

    for i in range(k):
        func += (np.heaviside(ti - tks[i], 1) - np.heaviside(ti - (tks[i] + dt), 0)) * dF * (np.exp(alpha*dt) * np.exp(-alpha*(ti - tks[i])) - 1) + (np.heaviside(ti - tks[i], 1) - np.heaviside(ti - tks[i + 1], 0)) * dF * (1 - np.exp(alpha * dt)) * (1 - np.exp(-alpha * dtau * i))/(1 - np.exp(-alpha * dtau)) * np.exp(-alpha * (ti - tks[i]))
    
    print(func)

    return func

def dsi_response(t, k, tks, dt, dtau, dF, alpha, D):
    
    dsi = np.zeros(len(t))
    print(dsi)
    for i, ti in enumerate(t):
        dsi[i] = dsi_k(ti, k, tks, dt, dtau, dF, alpha, D)
        print(dsi[i])

    # dsi = dsi

    return dsi

##############################################################################
# load data

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/delta_F.pickle', 'rb') as file:
    dF_data = pickle.load(file)

##############################################################################
# calculations

D = si_data['D']
alpha = alpha_data['alpha']
dt = precond_data['duration_ave']
dtau = precond_data['period_ave']
dF = dF_data['dF_ave']*D**2/2/alpha
k = len(precond_data['events'])

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
t = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(t):
    if ti < 0:
        t[i] = 0

tks = []
for i in range(k):
    tks.append(i*dtau)

tks.append(t[-1])

dates = [dtime.date() for dtime in si_data['t']]

# tks_i = []
# for i, event in enumerate(precond_data['events']):
    # tks_i.append(dates.index(event[0]))

# tks_i.append(-1)
# tks = t[tks_i]

dsi = dsi_response(t, k, tks, dt, dtau, dF, alpha, D)

##############################################################################
# plotting

plt.plot(dsi + si_data['seasonal']['SI'])
plt.plot(si_data['control']['SI'])
plt.show()

plt.plot(dsi)
plt.plot(si_data['dSI'])
plt.show()
