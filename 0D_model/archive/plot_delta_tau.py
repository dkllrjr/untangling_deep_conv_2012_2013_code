import pickle
import numpy as np
import matplotlib.pyplot as plt
import csv
from datetime import datetime

##############################################################################

def simple_dSI(beta, dt, dtau, k, alpha):
    dSI = beta * (1 - np.exp(-alpha * dtau * k)) / (1 - np.exp(-alpha * dtau)) * np.exp( -alpha * dtau ) 
    return dSI

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

with open('../../mistrals/data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

with open('../data/si_seasonal_sine_curve_fit.csv') as file:
    si_s_cf_reader = csv.reader(file)
    si_s_cf = []
    for row in si_s_cf_reader:
        si_s_cf.append(row)

with open('../data/dsi_beta.pickle', 'rb') as file:
    beta_data = pickle.load(file)

##############################################################################
# calculations

ref_date = datetime(2013, 1, 1, 12)
t = np.array([(date - ref_date).days for date in si_data['t']])

dsi_start = precond_data['events'][0][0]
dsi_ref_date = datetime(dsi_start.year, dsi_start. month, dsi_start.day, 12)
dsit = np.array([(date - dsi_ref_date).days for date in si_data['t']])
for i, ti in enumerate(dsit):
    if ti < 0:
        dsit[i] = 0

dt = precond_data['duration_ave']
dtaus = [2, 3, 5, precond_data['period_ave'], 10, 20, 50]

betas = np.array([-0.10, -0.25, beta_data['average'], -.75])

##############################################################################
# plotting

fig, ax = plt.subplots(1, 1, dpi=200, figsize=(15, 8))

fsize = 18
tsize = 22

for i, dtau in enumerate(dtaus):
    
    k = dsit/dtau
    k = np.where(k > 24, 24, k)
    alpha = 0.101
    beta = betas[2]

    f = simple_dSI(beta, dt, dtau, k, alpha) + si_data['seasonal']['SI']
    ax.plot(si_data['t'], f, label='$\\Delta \\tau$ = ' + str(dtau))

ax.set_xlabel('Time [days]', fontsize=fsize)

# ax.set_ylim(0, 2)
ax.set_xlim(si_data['t'][0], si_data['t'][-1])
ax.set_ylabel('$m^2/s^2$', fontsize=fsize)
ax.tick_params(axis='x', labelsize=fsize)
ax.tick_params(axis='y', labelsize=fsize)

ax.plot(si_data['t'], si_data['control']['SI'], color='black', linestyle='--', label='Control')

ax.legend(ncol=2, fontsize=fsize-4)

ax.plot([si_data['t'][0], si_data['t'][-1]], [0, 0], color = "black", linestyle=':')

ax.set_title('$\\alpha$ = 0.101, $\\beta$ = '+str(betas[2])[0:6] +' $k$ = 24, $\\Delta t$ = ' + str(dt)[0:4], fontsize=fsize)

fig.suptitle('Varying the Number of Events $k$', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/dtau_gol.png')
