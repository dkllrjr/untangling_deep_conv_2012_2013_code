# Mistral Events

This document outlines the method used to determine Mistral events in the MEDCORDEX WRF ORCH coupled simulation dataset `WRFORCH_19790101_20161231_1D_uas.nc`.

## Mistral Constraints

Two things primarily describe a Mistral event:

- strong, northerly flow in the Gulf of Lion from the Rhône Valley
- presence of a Genoa cyclone

### Surface Wind Locations

The following locations were selected for surface wind data:

Montélimar, France or the Rhône Valley: 44.5569$^\circ$ N, 4.7495$^\circ$ E

Gulf of Lion: 42.666161$^\circ$ N, 4.437177$^\circ$ E

### Wind Constraints

The wind at 10m above the surface is constrained by a range of direction and to be above a certain threshold. At both locations, the wind must be:

- northerly flow between -135$^\circ$ and -45$^\circ$
- above 2 $m/s$

### Genoa Cyclone Constraints

The presence of a Genoa cyclone or Genoa low was determined retrospectively through the ERA Interim Reanalysis. A low with a complete 0.5 hPa contour within or touching the following boxes denoted the presence of a Genoa low.

- 38-44$^\circ$ N, 4-14$^\circ$ E `Genoa_Cyc_table.mat`

- 40-44$^\circ$ N, 4-12$^\circ$ E `Genoa_Cyc_dates.mat`

## Python Scripts

Here is a quick synopsis of the python scripts used to look at the MEDCORDEX dataset. They are run in the following order.

1. `wrforch_nc2pickle.py` takes the WRF ORCH data (in netcdf files on ClimServ) and selects the surface wind data at Montélimar, FR (in the Rhône Valley) and at 42.6 N, 4.4 E in the Gulf of Lion. It saves this to `mistral_wrforch_rhone_gol.pickle`.
1. `genoa_cyclones_dates_mat2pickle.py` takes the Genoa cyclone presence data (boolean, determined from ERA Interim Reanalysis, `Genoa_Cyc_dates.mat` or `Genoa_Cyc_table.mat`) and saves the dates in `genoa_cyclones.pickle`.
1. `genoa_cyclones_and_wind.py` takes the surface wind data produced by `mistral_wrforch_rhone_gol.pickle` and the Genoa cyclone dates from `genoa_cyclones.pickle` and determines if a given day in from the MEDCORDEX has a Mistral, given the constraints outlined above.
1. `mistral_attributes.py` takes the Mistral dates from `mistral_dates_yyyy-mm-dd.pickle` and clumps them into events. Adjacent dates are clumped together. The length of these clumps/events and the time between clumps/events is recorded in `mistral_attributes.pickle`.
1. `mistral_attributes_2012-2013.py` takes the event data from `mistral_attributes.pickle` and determines the average duration of the Mistral events and the period between them from 21-09-2012 to 21-03-2013 and saves it to `mistral_attributes_2012-2013.pickle`. 
