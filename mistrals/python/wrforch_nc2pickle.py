##############################################################################
# By Doug Keller - Laboratoire de Météorologie Dynamique
# 
# Script to separate out a Rhône Valley and Gulf of Lion data points
# for surface level winds from the MEDCORDEX-B WRF ORCH coupled run
# dataset.

import os
import pickle
import numpy as np
import xarray as xr

def dist_sphere(r0, the0, phi0, r1, the1, phi1):
#    (r0**2 + r1**2 - 2*r0*r1*(np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)))**.5
    a = r0**2 + r1**2
    b = 2*r0*r1
    c = np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)
    d = (a - b*c)
    return d**.5

def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
    the0 = np.deg2rad(the0)
    phi0 = np.deg2rad(phi0)
    the1 = np.deg2rad(the1)
    phi1 = np.deg2rad(phi1)
    return dist_sphere(r0, the0, phi0, r1, the1, phi1)

def lat_lon_near(lat_lon, loc):
    E_r = 6371000  # Earth's radius
    dist = []
    for i in range(len(lat_lon)):
        # print(i,len(lat_lon))
        dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
    ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
    return np.array(lat_lon)[ind]


##############################################################################
# Extracting netcdf data from MEDCORDEX-B dataset

MB_path = '/bdd/MEDI/workspaces/dkeller/MEDCORDEX-B'  # my personal path to the data directory on my computer

vas_path = os.path.join(MB_path, 'WRFORCH_19790101_20161231_1D_vas.nc')
uas_path = os.path.join(MB_path, 'WRFORCH_19790101_20161231_1D_uas.nc')

vas = xr.open_dataset(vas_path)
uas = xr.open_dataset(uas_path)

##############################################################################
# Finding closest grid point to desired spots

print('Selecting location indices')

M_loc = [44.5569, 4.7495]  # Montélimar, France
Lion_loc = [42.666161, 4.437177]  # Gulf of Lion

lat = np.array(vas.nav_lat_grid_M)
lon = np.array(vas.nav_lon_grid_M)
lat_lon = []

for i in range(lat.shape[0]):
    for j in range(lat.shape[1]):
        lat_lon.append([lat[i, j], lon[i, j]])

loc = [M_loc, Lion_loc]

ind_loc = []

for i in range(len(loc)):
    ind_loc.append(lat_lon_near(lat_lon, loc[i]))

lat_lon_np = np.array(lat_lon)
lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
ind_loc_np = np.array(ind_loc)
ind_loc_np = ind_loc_np.reshape(len(loc), 2)

ind = []

for k in range(ind_loc_np.shape[0]):
    for i in range(lat_lon_np.shape[0]):
        for j in range(lat_lon_np.shape[1]):
            if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                ind.append([i, j])

ind = np.array(ind)

print('Indices selected')

##############################################################################
# Separating components at the specified grid points

print('Separating out data')

Rv10 = vas.vas[:, ind[0, 0], ind[0, 1]]
Ru10 = uas.uas[:, ind[0, 0], ind[0, 1]]
GLv10 = vas.vas[:, ind[1, 0], ind[1, 1]]
GLu10 = uas.uas[:, ind[1, 0], ind[1, 1]]

print('Data separated')

##############################################################################
# Saving the data

print('Saving data')

data = {'Rv10': Rv10, 'Ru10': Ru10, 'GLu10': GLu10, 'GLv10': GLv10, 'ind_loc': ind_loc_np}
with open('../data/mistral_wrforch_rhone_gol.pickle', 'wb') as file:
    pickle.dump(data, file)
