##############################################################################
# By Doug Keller - Laboratoire de Météorologie Dynamique
#
# Script to determine the average duration and time between Mistral events.

import pickle
import datetime
import numpy as np

##############################################################################
# Opening mistral dates data

with open('data/mistral_dates_2020-01-13.pickle', 'rb') as file:
    mistral_raw = pickle.load(file)

dates = []
for i, date in enumerate(mistral_raw['mistral_dates']):
    dates.append(datetime.date.fromisoformat(date[0:10]))

##############################################################################
# Grouping dates into events

dates_diff = np.diff(dates)

events = []
i = 0
while i < len(dates_diff):
    events.append([])
    events[-1].append(dates[i])

    if dates_diff[i].days < 2:

        while i < len(dates_diff) and dates_diff[i].days < 2:
            i += 1
            events[-1].append(dates[i])

    i += 1

# looking at the duration of the events
duration = []
for i, event in enumerate(events):
    duration.append(len(event))

# looking at the period between the beginning dates of each event
period = []
for i, event in enumerate(events[0:-1]):
    period.append((events[i+1][0] - events[i][0]).days)

##############################################################################
# Saving data

data = {'events': events, 'duration': duration, 'period': period}

with open('data/mistral_attributes.pickle','wb') as file:
    pickle.dump(data,file)
