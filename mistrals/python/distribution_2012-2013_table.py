import pickle
import pandas

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table

##############################################################################

with open('../data/combined_mistrals.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    pc_data = pickle.load(file)

##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table*', 'begin{table*}[t]', 'tophline', 'middlehline', 'bottomhline']
dt = '$\\Delta t_k$'
dtau = '$\\Delta \\tau_k$'

##############################################################################
# full year

dates = data['events']
start_dates = []
end_dates = []

for date in dates:
    start_dates.append(date[0].strftime('%Y-%m-%d'))
    end_dates.append(date[-1].strftime('%Y-%m-%d'))

data_for_table = {}
data_for_table['Start Date_0'] = start_dates[0:int(len(start_dates)/2)]
data_for_table['Duration_0'] = data['duration'][0:int(len(start_dates)/2)]
data_for_table['Period_0'] = data['period'][0:int(len(start_dates)/2)]
data_for_table['Start Date_1'] = start_dates[int(len(start_dates)/2):len(start_dates)]
data_for_table['Duration_1'] = data['duration'][int(len(start_dates)/2):len(start_dates)]
data_for_table['Period_1'] = data['period'][int(len(start_dates)/2):len(start_dates)]

df = pandas.DataFrame(data_for_table)

table_header = ['Start Date', 'Duration', 'Period', 'Start Date', 'Duration', 'Period']

table = df.to_latex(index=False, label='tab:mistral_year', caption='', header = table_header)

table = hlines(table, words_to_replace, replacing_words)
table = table.replace('lrrlrr', 'lrr|lrr')
table = table.replace('Duration', dt)
table = table.replace('Period', dtau)

with open('../tables/mistrals_2012-2013.tex', 'w') as file:
    file.writelines(table)

##############################################################################
# preconditioning

pc_dates = pc_data['events']
pc_start_dates = []
pc_end_dates = []

for date in pc_dates:
    pc_start_dates.append(date[0].strftime('%Y-%m-%d'))
    pc_end_dates.append(date[-1].strftime('%Y-%m-%d'))

data_for_table = {}
data_for_table['Start Date_0'] = pc_start_dates[0:int(len(pc_start_dates)/2)]
data_for_table['Duration_0'] = pc_data['duration'][0:int(len(pc_start_dates)/2)]
data_for_table['Period_0'] = pc_data['period'][0:int(len(pc_start_dates)/2)]
data_for_table['Start Date_1'] = pc_start_dates[int(len(pc_start_dates)/2):len(pc_start_dates)]
data_for_table['Duration_1'] = pc_data['duration'][int(len(pc_start_dates)/2):len(pc_start_dates)]
data_for_table['Period_1'] = pc_data['period'][int(len(pc_start_dates)/2):len(pc_start_dates)]

pc_df = pandas.DataFrame(data_for_table)

pc_table_header = ['Start Date', 'Duration', 'Period', 'Start Date', 'Duration', 'Period']

pc_table = pc_df.to_latex(index=False, label='tab:mistral_precond', caption='', header = pc_table_header)

pc_table = hlines(pc_table, words_to_replace, replacing_words)
pc_table = pc_table.replace('lrrlrr', 'lrr|lrr')
pc_table = pc_table.replace('Duration', dt)
pc_table = pc_table.replace('Period', dtau)


with open('../tables/mistrals_2012-2013_precond.tex', 'w') as file:
    file.writelines(pc_table)

##############################################################################
# averages

dtab = {}
dtab['Start Dates'] = [start_dates[0]+' to '+end_dates[-1], pc_start_dates[0]+' to '+pc_end_dates[-1]]
dtab['DurationAve'] = [data['duration_ave'], pc_data['duration_ave']]
dtab['PeriodAve'] = [data['period_ave'], pc_data['period_ave']]
dtab['DurationStd'] = [data['duration_std'], pc_data['duration_std']]
dtab['PeriodStd'] = [data['period_std'], pc_data['period_std']]

df = pandas.DataFrame(dtab)

table = df.to_latex(index=False, float_format= '%.2f', label = 'tab:mistral_ave', caption = '')

table = hlines(table, words_to_replace, replacing_words)
table = table.replace('DurationAve', '$\\overline{\\Delta t}$')
table = table.replace('PeriodAve', '$\\overline{\\Delta \\tau}$')
table = table.replace('DurationStd', '$\\sigma_{\\Delta t}$')
table = table.replace('PeriodStd', '$\\sigma_{\\Delta \\tau}$')

with open('../tables/mistrals_2012-2013_ave.tex', 'w') as file:
    file.writelines(table)
