##############################################################################
# By Doug Keller - Laboratoire de Météorologie Dynamique
# 
# Script to look at just the mistral attributes of the 21-09-2012 to 
# 21-03-2013.

import pickle
import datetime
import numpy as np

##############################################################################
# Opening data

with open('../data/mistral_attributes.pickle', 'rb') as file:
    data = pickle.load(file)

##############################################################################
# Separating out fall equinox 2012 to spring equinox 2013

beg = datetime.date(2012, 8, 1)
end = datetime.date(2013, 8, 1)

sep_data = {'events': [], 'duration': [], 'period': []}
for i, event in enumerate(data['events']):
    if beg <= event[0] < end:
        for key in sep_data.keys():
            sep_data[key].append(data[key][i])

sep_data['avg_duration'] = np.mean(sep_data['duration'])
sep_data['avg_period'] = np.mean(sep_data['period'])

##############################################################################
# Saving data

with open('../data/mistral_attributes_2012-2013.pickle', 'wb') as file:
    pickle.dump(sep_data, file)
