module nc_mod

using Statistics

export multi_array_moving_mean, multi_array_moving_std

function skipnan(f)
    return filter(!isnan,f)
end

function skipbad(f)
    f_nomiss = collect(skipmissing(f))
    f_nonan = skipnan(f_nomiss)
    return f_nonan
end

function moving_mean(f,N)
    # window = 2*N + 1
    f_m = copy(f)
    for i = 1:length(f)
        if i < N + 1
            f_m[i] = mean(f[begin:i+N])
            elseif i > length(f) - N
            f_m[i] = mean(f[i-N:end])
            else
            f_m[i] = mean(f[i-N:i+N])
        end
    end
    return f_m
end

function moving_std(f,N)
    # window = 2*N + 1
    f_std = copy(f)
    for i = 1:length(f)
        if i < N + 1
            f_std[i] = std(f[begin:i+N])
            elseif i > length(f) - N
            f_std[i] = std(f[i-N:end])
            else
            f_std[i] = std(f[i-N:i+N])
        end
    end
    return f_std
end


function nan_moving_mean(f,N)
    # window = 2*N + 1
    f_m = copy(f)
    for i = 1:length(f)
        if i < N + 1
            f_m[i] = mean(skipbad(f[begin:i+N]))
            elseif i > length(f) - N
            f_m[i] = mean(skipbad(f[i-N:end]))
            else
            f_m[i] = mean(skipbad(f[i-N:i+N]))
        end
    end
    return f_m
end

function nan_moving_std(f,N)
    # window = 2*N + 1
    f_std = copy(f)
    for i = 1:length(f)
        if i < N + 1
            f_std[i] = std(skipbad(f[begin:i+N]))
            elseif i > length(f) - N
            f_std[i] = std(skipbad(f[i-N:end]))
            else
            f_std[i] = std(skipbad(f[i-N:i+N]))
        end
    end
    return f_std
end

function linear_filter(x,xth,a)
    return sign(x) * (xth + a * ( abs(x) - xth) )
end

function one_sided_linear_filter(x,xth,a)
    if x > xth
        xn = xth + a * (x - xth)
    else
        xn = x
    end
    return xn 
end

function wind_field_filter(u,v,um,vm,a)
    un = copy(u)
    vn = copy(v)
    for i = 1:size(u)[1]
        for j = 1:size(u)[2]
            for k = 1:size(u)[3]
                
                θ = v[i,j,k]/u[i,j,k]
                uth = ( ( um[i,j,k]^2 + vm[i,j,k]^2 ) / (1 + θ^2) )^.5
                un[i,j,k] = linear_filter(u[i,j,k],uth,a)
                vn[i,j,k] = un[i,j,k] * θ

                # if um[i,j,k] + us[i,j,k] < u[i,j,k]
                    
                #     θ = v[i,j,k]/u[i,j,k]
                    
                #     un[i,j,k] = ( (um[i,j,k]^2 + vm[i,j,k]^2)^.5 + (rand()*2-1) * (uds[i,j,k]^2 + vds[i,j,k]^2)^.5 ) / (1 + θ^2 )^.5

                #     vn[i,j,k] = θ * un[i,j,k]

                # end
            end
        end
        if i % 10 == 0
            println(i)
        end
    end
    return un,vn
end

function tracer_field_filter(T,Tm,a)
    Tn = copy(T)
    for i = 1:size(T)[1]
        for j = 1:size(T)[2]
            for k = 1:size(T)[3]
                Tn[i,j,k] = linear_filter(T[i,j,k],Tm[i,j,k],a)
                # if Tm[i,j,k] + Ts[i,j,k] < T[i,j,k]
                #     Tn[i,j,k] = Tm[i,j,k] + Ts[i,j,k] + (rand()*2-1) * Tds[i,j,k]
                # elseif Tm[i,j,k] - Ts[i,j,k] > T[i,j,k]
                #     Tn[i,j,k] = Tm[i,j,k] - Ts[i,j,k] - (rand()*2-1) * Tds[i,j,k]
                # end
            end
        end
        if i % 10 == 0
            println(i)
        end
    end
    return Tn
end

function multi_array_moving_mean(f,N)
    fn = copy(f)
    for i = 1:size(f)[1]
        for j = 1:size(f)[2]
            fn[i,j,:] = moving_mean(f[i,j,:],N)
        end
        if i % 10 == 0
            println(i)
        end
    end
    return fn
end

function multi_array_moving_std(f,N)
    fn = copy(f)
    for i = 1:size(f)[1]
        for j = 1:size(f)[2]
            fn[i,j,:] = moving_std(f[i,j,:],N)
        end
        if i % 10 == 0
            println(i)
        end
    end
    return fn
end

function field_moving_mean_by_hour(x,N,s)
    xn = copy(x)
    for i in 1:size(x)[1]
        for j in 1:size(x)[2]
            xn[i,j,:] = moving_mean_by_hour(x[i,j,:],N,s)
        end
        if i % 10 == 0
            println(i)
        end
    end
    return xn
end

function moving_mean_by_hour(x,N,s)
    xn = copy(x)
    for i in 1:s
        xn[i:s:end] = moving_mean(x[i:s:end],N)
    end
    return xn
end

println("nc_mod loaded")
end

