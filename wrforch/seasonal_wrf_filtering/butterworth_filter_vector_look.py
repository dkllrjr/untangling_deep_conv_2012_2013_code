import xarray as xr
from scipy.signal import iirfilter, sosfiltfilt
from glob import glob
import matplotlib.pyplot as plt
from dask.diagnostics import ProgressBar
import numpy as np
import os

##############################################################################

# ran on laplace

# data_path = '/home/laplace/Data/phd_temp/'
data_path = '/home/mach/Data/phd_temp/'

T_paths = glob(data_path + 'raw/*TAS.nc')
q_paths = glob(data_path + 'raw/*HUSS.nc')
u_paths = glob(data_path + 'raw/*UAS.nc')
v_paths = glob(data_path + 'raw/*VAS.nc')

T_paths.sort()
q_paths.sort()
u_paths.sort()
v_paths.sort()

Tb_paths = glob(data_path + 'butter_vector/*TAS.nc')
qb_paths = glob(data_path + 'butter_vector/*HUSS.nc')
ub_paths = glob(data_path + 'butter_vector/*UAS.nc')
vb_paths = glob(data_path + 'butter_vector/*VAS.nc')

Tb_paths.sort()
qb_paths.sort()
ub_paths.sort()
vb_paths.sort()

##############################################################################
# T and q

# var_raw = [T_paths, q_paths]
# var_butt = [Tb_paths, qb_paths]

# var_names = ['tas', 'huss']

# for vari, var_raw_paths in enumerate(var_raw):

    # for pathi, _ in enumerate(var_raw_paths[0:-1]):

        # var_data0 = xr.open_dataset(var_raw_paths[pathi])
        # var_data1 = xr.open_dataset(var_raw_paths[pathi+1])

        # print('loaded data')

        # var_data = xr.concat([var_data0, var_data1], dim='Time')

        # print('concatted data')

        # nvar_data = xr.open_mfdataset(var_butt[vari], concat_dim='Time', combine='nested')

        # plt.plot(var_data[var_names[vari]][:, 180, 180])
        # plt.plot(nvar_data[var_names[vari]][:, 180, 180])
        # plt.show()

        # print('filtered data')

    # del var_data
    # del nvar_data
    # del var_data0
    # del var_data1

##############################################################################

# u_paths, v_paths
# ub_paths, vb_paths

for pathi, _ in enumerate(u_paths[0:-1]):

    uas_data0 = xr.open_dataset(u_paths[pathi])
    uas_data1 = xr.open_dataset(u_paths[pathi+1])

    vas_data0 = xr.open_dataset(v_paths[pathi])
    vas_data1 = xr.open_dataset(v_paths[pathi+1])

    print('loaded data')

    uas_data = xr.concat([uas_data0, uas_data1], dim='Time')
    vas_data = xr.concat([vas_data0, vas_data1], dim='Time')

    plt.plot(uas_data['uas'][:, 180, 180])

    print('concatted data')

    nuas_data = xr.open_mfdataset(ub_paths, combine='nested', concat_dim='Time')
    nvas_data = xr.open_mfdataset(vb_paths, combine='nested', concat_dim='Time')

    plt.plot(nuas_data['uas'][:, 180, 180])
    plt.show()

