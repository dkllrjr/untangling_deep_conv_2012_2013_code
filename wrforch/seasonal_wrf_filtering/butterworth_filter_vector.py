import xarray as xr
from scipy.signal import iirfilter, sosfiltfilt
from glob import glob
import matplotlib.pyplot as plt
from dask.diagnostics import ProgressBar
import numpy as np
import os

##############################################################################

# ran on laplace

# data_path = '/home/laplace/Data/phd_temp/'
data_path = '/home/mach/Data/phd_temp/'

T_paths = glob(data_path + 'raw/*TAS.nc')
q_paths = glob(data_path + 'raw/*HUSS.nc')
u_paths = glob(data_path + 'raw/*UAS.nc')
v_paths = glob(data_path + 'raw/*VAS.nc')

T_paths.sort()
q_paths.sort()
u_paths.sort()
v_paths.sort()

# T = xr.open_mfdataset(glob(data_path + 'raw/*TAS.nc').sort())
# q = xr.open_mfdataset(glob(data_path + 'raw/*HUSS.nc').sort())
# u = xr.open_mfdataset(glob(data_path + 'raw/*UAS.nc').sort())
# v = xr.open_mfdataset(glob(data_path + 'raw/*VAS.nc').sort())

# raw_files = data_path + 'raw/'

# os.system('rm ' + data_path + 'butter_vector/*')
# os.system('cp ' + raw_files + '* ' + data_path + 'butter_vector/')

print('copied raw files over')

Tb_paths = glob(data_path + 'butter_vector/*TAS.nc')
qb_paths = glob(data_path + 'butter_vector/*HUSS.nc')
ub_paths = glob(data_path + 'butter_vector/*UAS.nc')
vb_paths = glob(data_path + 'butter_vector/*VAS.nc')

Tb_paths.sort()
qb_paths.sort()
ub_paths.sort()
vb_paths.sort()

##############################################################################

fs = 8/86400  # Hz; sampling frequency of the variables

f_high = 1/86400/1.25  # Hz
f_low = 1/86400/25  # Hz

order = 20
sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)

##############################################################################
# T and q

var_raw = [T_paths, q_paths, u_paths, v_paths]
var_butt = [Tb_paths, qb_paths, ub_paths, vb_paths]

var_names = ['tas', 'huss', 'uas', 'vas']

for vari, var_raw_paths in enumerate(var_raw):

    for pathi, _ in enumerate(var_raw_paths[0:-1]):

        var_data0 = xr.open_dataset(var_raw_paths[pathi])
        var_data1 = xr.open_dataset(var_raw_paths[pathi+1])

        print('loaded data')

        var0_shape = var_data0[var_names[vari]].shape[0]

        var_data = xr.concat([var_data0, var_data1], dim='Time')

        del var_data0
        del var_data1

        print('concatted data')

        nvar_data = sosfiltfilt(sos, var_data[var_names[vari]], axis=0)

        print('filtered data')

        var_data0 = xr.open_dataset(var_butt[vari][pathi])
        var_data0[var_names[vari]][:, :, :] = nvar_data[0:var0_shape, :, :]

        var_data1 = xr.open_dataset(var_butt[vari][pathi+1])
        var_data1[var_names[vari]][:, :, :] = nvar_data[var0_shape::, :, :]

        print('inserted filtered data')

        delayed = var_data0.to_netcdf(var_butt[vari][pathi], mode='a', compute=False)
        with ProgressBar():
            results = delayed.compute()
        
        delayed = var_data1.to_netcdf(var_butt[vari][pathi+1], mode='a', compute=False)
        with ProgressBar():
            results = delayed.compute()

        print('saved data')
