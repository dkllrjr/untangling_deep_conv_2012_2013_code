import xarray as xr
from scipy.signal import iirfilter, sosfiltfilt
from glob import glob
import matplotlib.pyplot as plt
from dask.diagnostics import ProgressBar
import numpy as np
import os

##############################################################################

# ran on laplace

# data_path = '/home/laplace/Data/phd_temp/'
data_path = '/home/mach/Data/phd_temp/'

T_paths = glob(data_path + 'raw/*TAS.nc')
q_paths = glob(data_path + 'raw/*HUSS.nc')
u_paths = glob(data_path + 'raw/*UAS.nc')
v_paths = glob(data_path + 'raw/*VAS.nc')

T_paths.sort()
q_paths.sort()
u_paths.sort()
v_paths.sort()

# T = xr.open_mfdataset(glob(data_path + 'raw/*TAS.nc').sort())
# q = xr.open_mfdataset(glob(data_path + 'raw/*HUSS.nc').sort())
# u = xr.open_mfdataset(glob(data_path + 'raw/*UAS.nc').sort())
# v = xr.open_mfdataset(glob(data_path + 'raw/*VAS.nc').sort())

raw_files = data_path + 'raw/'

os.system('rm ' + data_path + 'butter/*')
os.system('cp ' + raw_files + '* ' + data_path + 'butter/')

Tb_paths = glob(data_path + 'butter/*TAS.nc')
qb_paths = glob(data_path + 'butter/*HUSS.nc')
ub_paths = glob(data_path + 'butter/*UAS.nc')
vb_paths = glob(data_path + 'butter/*VAS.nc')

Tb_paths.sort()
qb_paths.sort()
ub_paths.sort()
vb_paths.sort()

# Tb = xr.open_mfdataset(glob(data_path + 'butter/*TAS.nc'))
# qb = xr.open_mfdataset(glob(data_path + 'butter/*HUSS.nc'))
# ub = xr.open_mfdataset(glob(data_path + 'butter/*UAS.nc'))
# vb = xr.open_mfdataset(glob(data_path + 'butter/*VAS.nc'))

##############################################################################
# T and q

T = xr.open_mfdataset(vb_paths)

print(T.nav_lat_grid_M.values)

