import xarray as xr
from scipy.signal import iirfilter, sosfiltfilt
from glob import glob
import matplotlib.pyplot as plt
from dask.diagnostics import ProgressBar
import numpy as np
import os

##############################################################################

# ran on laplace

# data_path = '/home/laplace/Data/phd_temp/'
data_path = '/home/mach/Data/phd_temp/'

T_paths = glob(data_path + 'raw/*TAS.nc')
q_paths = glob(data_path + 'raw/*HUSS.nc')
u_paths = glob(data_path + 'raw/*UAS.nc')
v_paths = glob(data_path + 'raw/*VAS.nc')

T_paths.sort()
q_paths.sort()
u_paths.sort()
v_paths.sort()

# T = xr.open_mfdataset(glob(data_path + 'raw/*TAS.nc').sort())
# q = xr.open_mfdataset(glob(data_path + 'raw/*HUSS.nc').sort())
# u = xr.open_mfdataset(glob(data_path + 'raw/*UAS.nc').sort())
# v = xr.open_mfdataset(glob(data_path + 'raw/*VAS.nc').sort())

raw_files = data_path + 'raw/'

os.system('rm ' + data_path + 'butter/*')
os.system('cp ' + raw_files + '* ' + data_path + 'butter/')

print('copied raw files over')

Tb_paths = glob(data_path + 'butter/*TAS.nc')
qb_paths = glob(data_path + 'butter/*HUSS.nc')
ub_paths = glob(data_path + 'butter/*UAS.nc')
vb_paths = glob(data_path + 'butter/*VAS.nc')

Tb_paths.sort()
qb_paths.sort()
ub_paths.sort()
vb_paths.sort()

# Tb = xr.open_mfdataset(glob(data_path + 'butter/*TAS.nc'))
# qb = xr.open_mfdataset(glob(data_path + 'butter/*HUSS.nc'))
# ub = xr.open_mfdataset(glob(data_path + 'butter/*UAS.nc'))
# vb = xr.open_mfdataset(glob(data_path + 'butter/*VAS.nc'))

##############################################################################

fs = 8/86400  # Hz; sampling frequency of the variables

f_high = 1/86400/1.25  # Hz
f_low = 1/86400/25  # Hz

order = 20
sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)

##############################################################################
# T and q

var_raw = [T_paths, q_paths]
var_butt = [Tb_paths, qb_paths]

var_names = ['tas', 'huss']

# for vari, var_raw_paths in enumerate(var_raw):

    # for pathi, _ in enumerate(var_raw_paths[1:-1]):

        # var_data0 = xr.open_dataset(var_raw_paths[pathi-1])
        # var_data1 = xr.open_dataset(var_raw_paths[pathi])
        # var_data2 = xr.open_dataset(var_raw_paths[pathi+1])

        # print('loaded data')

        # var_data = xr.concat([var_data0, var_data1, var_data2], dim='Time')

        # print('concatted data')

        # nvar_data = sosfiltfilt(sos, var_data[var_names[vari]], axis=0)

        # print('filtered data')

        # var_data1 = xr.open_dataset(var_butt[vari][pathi])
        # var_data1[var_names[vari]][:, :, :] = nvar_data[var_data0[var_names[vari]].shape[0]:var_data1[var_names[vari]].shape[0], :, :]

        # print('saved middle part')

        # if pathi == 1:

            # var_data0 = xr.open_dataset(var_butt[vari][pathi-1])
            # var_data0[var_names[vari]][:, :, :] = nvar_data[0:var_data0[var_names[vari]].shape[0], :, :]

            # delayed = var_data0.to_netcdf(var_butt[vari][pathi-1], compute=False)
            # with ProgressBar():
                # results = delayed.compute()

        # if pathi == len(var_raw_paths) - 2:

            # var_data2 = xr.open_dataset(var_butt[vari][pathi+1])
            # var_data2[var_names[vari]][:, :, :] = nvar_data[var_data1[var_names[vari]].shape[0]::, :, :]
        
            # delayed = var_data2.to_netcdf(var_butt[vari][pathi+1], compute=False)
            # with ProgressBar():
                # results = delayed.compute()

for vari, var_raw_paths in enumerate(var_raw):

    for pathi, _ in enumerate(var_raw_paths[0:-1]):

        var_data0 = xr.open_dataset(var_raw_paths[pathi])
        var_data1 = xr.open_dataset(var_raw_paths[pathi+1])

        print('loaded data')

        var0_shape = var_data0[var_names[vari]].shape[0]

        var_data = xr.concat([var_data0, var_data1], dim='Time')

        del var_data0
        del var_data1

        print('concatted data')

        nvar_data = sosfiltfilt(sos, var_data[var_names[vari]], axis=0)

        print('filtered data')

        var_data0 = xr.open_dataset(var_butt[vari][pathi])
        var_data0[var_names[vari]][:, :, :] = nvar_data[0:var0_shape, :, :]

        var_data1 = xr.open_dataset(var_butt[vari][pathi+1])
        var_data1[var_names[vari]][:, :, :] = nvar_data[var0_shape::, :, :]

        print('inserted filtered data')

        delayed = var_data0.to_netcdf(var_butt[vari][pathi], mode='a', compute=False)
        with ProgressBar():
            results = delayed.compute()
        
        delayed = var_data1.to_netcdf(var_butt[vari][pathi+1], mode='a', compute=False)
        with ProgressBar():
            results = delayed.compute()

        print('saved data')

del var_data
del nvar_data
del var_data0
del var_data1

##############################################################################

# u_paths, v_paths
# ub_paths, vb_paths

for pathi, _ in enumerate(u_paths[0:-1]):

    uas_data0 = xr.open_dataset(u_paths[pathi])
    uas_data1 = xr.open_dataset(u_paths[pathi+1])

    vas_data0 = xr.open_dataset(v_paths[pathi])
    vas_data1 = xr.open_dataset(v_paths[pathi+1])

    print('loaded data')

    uas_data = xr.concat([uas_data0, uas_data1], dim='Time')
    vas_data = xr.concat([vas_data0, vas_data1], dim='Time')

    # plt.plot(uas_data['uas'][:, 190, 190])

    ws_data = (uas_data['uas']**2 + vas_data['vas']**2)**.5
    theta_data = vas_data['vas']/uas_data['uas']

    del vas_data

    print('concatted data')

    nws_data = sosfiltfilt(sos, ws_data, axis=0)
    nuas_data = nws_data/(1 + theta_data**2)**.5 * np.sign(uas_data['uas'])
    nvas_data = theta_data * nuas_data

    del uas_data
    del nws_data
    del theta_data

    print('filtered data')

    uas_data0 = xr.open_dataset(ub_paths[pathi])
    uas_data0['uas'][:, :, :] = nuas_data[0:uas_data0['uas'].shape[0], :, :]

    uas_data1 = xr.open_dataset(ub_paths[pathi+1])
    uas_data1['uas'][:, :, :] = nuas_data[uas_data0['uas'].shape[0]::, :, :]

    # plt.plot(uas_data0['uas'][:, 190, 190])
    # plt.show()

    print('inserted filtered data')

    delayed = uas_data0.to_netcdf(ub_paths[pathi], mode='a', compute=False)
    with ProgressBar():
        results = delayed.compute()
    
    delayed = uas_data1.to_netcdf(ub_paths[pathi+1], mode='a', compute=False)
    with ProgressBar():
        results = delayed.compute()

    print('uas done')

    vas_data0 = xr.open_dataset(vb_paths[pathi])
    vas_data0['vas'][:, :, :] = nvas_data[0:vas_data0['vas'].shape[0], :, :]

    vas_data1 = xr.open_dataset(vb_paths[pathi+1])
    vas_data1['vas'][:, :, :] = nvas_data[vas_data0['vas'].shape[0]::, :, :]

    print('inserted filtered data')

    delayed = vas_data0.to_netcdf(vb_paths[pathi], mode='a', compute=False)
    with ProgressBar():
        results = delayed.compute()
    
    delayed = vas_data1.to_netcdf(vb_paths[pathi+1], mode='a', compute=False)
    with ProgressBar():
        results = delayed.compute()

    print('saved data')
