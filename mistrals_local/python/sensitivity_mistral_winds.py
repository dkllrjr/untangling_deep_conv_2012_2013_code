##############################################################################
# By Doug Keller - InTro Group, Laboratoire de Météorologie Dynamique
#
# Script to separate and select days with certain wind characteristics for 
# sensitivity analysis. Produces a pickle and mat file of potential mistral
# dates.

import numpy as np
import pickle
import datetime
import scipy.io as sio

def stringify_dates(xarr):
    dates = []
    for i in xarr:
        dates.append(str(np.array(i['time_counter'])))
    return dates

def both_dates(dates1, dates2):
    return list(set(dates1).intersection(dates2))

def dates2indices(xarr, dates):
    ind = []
    xr_dates = stringify_dates(xarr)
    for i in range(len(xr_dates)):
        if xr_dates[i] in dates:
            ind.append(i)
    return ind

def wind_speed(u, v, w):
    return (u**2 + v**2 + w**2)**.5

def wind_direction(u, v):
    return np.arctan2(v, u)  # radians


##############################################################################
# Extracting data

print('Opening WRF-ORCH data')

# opening data from a pickle file that contains the point surface measurements from the MEDCORDEX-B WRF ORCH (LMD) coupled run
with open('../data/mistral_wrforch_rhone_gol.pickle', 'rb') as file:
    data = pickle.load(file)

# data = {'Rv10':Rv10,'Ru10':Ru10,'GLu10':GLu10,'GLv10':GLv10,'ind_loc':ind_loc_np}

GLu10 = data['GLu10']  # Golf of Lion surface wind data at 10m
GLv10 = data['GLv10']
Ru10 = data['Ru10']  # Rhône Valley surface wind data at 10m (Montélimar)
Rv10 = data['Rv10']

print('Ready to load')

ind_loc_np = data['ind_loc']

GLu10.load()
GLv10.load()
Ru10.load()
Rv10.load()

print('Loaded and opened WRF-ORCH data')

##############################################################################
# Calculating wind speed and direction

print('Calculating wind speed and direction')

GL_wd = wind_direction(GLu10, GLv10)
GL_wm = wind_speed(GLu10, GLv10, 0)
R_wd = wind_direction(Ru10, Rv10)
R_wm = wind_speed(Ru10, Rv10, 0)

print('Calculations complete')

def find_mistral_dates(GL_wd, GL_wm, R_wd, R_wm, theta_0, theta_1, wind_thres):

    # Separating by wind direction

    theta_0 = np.radians(theta_0)
    theta_1 = np.radians(theta_1)
    # 0 and -135 work too but it does present more error

    print('Separating winds by direction')

    south_winds_ind = []
    for i in range(len(GL_wd)):
        if theta_1 > GL_wd[i] > theta_0 and theta_1 > R_wd[i] > theta_0:
            south_winds_ind.append(i)
            print(i)

    print('Separated winds by direction')

    # Separating by wind magnitude threshold

    print('Separating winds by magnitude')

    # 2 m/s threshold
    pGL_wm = wind_thres
    pR_wm = wind_thres

    for i in south_winds_ind:
        if pGL_wm > GL_wm[i] and pR_wm > R_wm[i]:
            south_winds_ind.remove(i)

    print('Separated winds by magnitude')

    # Extracting dates and putting them into a string

    print('Organizing dates')

    south_wind_dates = stringify_dates(GL_wd['time_counter'][south_winds_ind])

    print('Separating cases with correct winds and the presence of a Genoa cyclone')

    mistral = south_wind_dates
    mistral.sort()

    mistral_ind = dates2indices(GL_wm, mistral)

    print('Extracting potential Mistral dates')

    mistral_dates = []
    for i in mistral:
        mistral_dates.append(i[0:10])
        print(i[0:10])

    print('Extracted dates for potential Mistral cases')

    return mistral_dates, mistral_ind

theta_0s = [0, -22.5, -45, -67.5]
theta_1s = [-180, -157.5, -135, -112.5]

wind_thress = [0, 2, 4, 6, 8, 10]

mistral_dicts = []
mistral_dicts_w_ind = []

for wind_thres in wind_thress:
    for i, theta_0 in enumerate(theta_0s):
        print(wind_thres, theta_0, theta_1s[i])

        temp_dates, temp_ind = find_mistral_dates(GL_wd, GL_wm, R_wd, R_wm, theta_0, theta_1s[i], wind_thres)
        temp_dict = {'theta_0': theta_0, 'theta_1': theta_1s[i], 'wind_thres': wind_thres, 'mistral_dates': temp_dates}
        mistral_dicts.append(temp_dict)

        temp_dict['mistral_ind'] = temp_ind
        mistral_dicts_w_ind.append(temp_dict)

###############################################################################
# Constructing dict for storing the dates/data

# just to make future use of the data easier to understand

descript_dict = {'general': 'These dates correspond to the days with the corresponding wind direction and wind speed threshold in the Gulf de Lion and Rhone Valley.', 'theta_0': 'Angle closest to the origin of the two angle limits for the flow streamwise direction.', 'theta_1': 'Angle furthest from the origin of the two angle limits for the flow streamwise direction.', 'wind_thres': 'Wind speed threshold for the wind speed magnitude at the two locations.', 'mistral_dates': 'Dates that had flow between the two angle limits and were above the wind threshold.'}

# saving mat file for the WIS group, as well as to make it more portable


mistral_mat = {'mistral_data': mistral_dicts, 'description': descript_dict}

# saving a pickle file for my use

mistral_data = {'mistral_dates': mistral_dicts_w_ind, 'description': descript_dict}

print('Finished structuring data for saving')

##############################################################################
# I think we have a winner - saving Genoa Cyclone Mistral dates

print('Saving data')

with open('../data/mistral_dates_sensitivity.pickle', 'wb') as file:
    pickle.dump(mistral_data, file)

with open('../data/mistral_dates_sensitivity.mat', 'wb') as file:
    sio.savemat(file, mistral_mat)

print('Data successfully saved')
