from scipy.io import loadmat
import numpy as np
from datetime import datetime
import pickle

with open('../data/mistral_winds_post_threshold.pickle', 'rb') as file:
    winds = pickle.load(file)

gen_tab = loadmat('../data/Genoa_Cyc_table.mat')  # current
gen_dat = loadmat('../data/Genoa_Cyc_dates.mat')  # Yonatan's

gen_tab = gen_tab['Genua_Cyc_dates']
gen_dat = gen_dat['Genua_Cyc_dates']

beg = 20120801
end = 20130731

gtab = []
gdat = []

for i, date in enumerate(gen_tab):
    if beg <= date[0] <= end:
        gtab.append(datetime(year=int(str(date[0])[0:4]), month=int(str(date[0])[4:6]), day=int(str(date[0])[6::]), hour=12))

gtab = np.array(gtab)

for i, date in enumerate(gen_dat):
    if beg <= date[0] <= end:
        gdat.append(datetime(year=int(str(date[0])[0:4]), month=int(str(date[0])[4:6]), day=int(str(date[0])[6::]), hour=12))

gdat = np.array(gdat)

beg = datetime(year=2012, month=8, day=1, hour=12)
end = datetime(year=2013, month=7, day=31, hour=12)

win = []
for i, date in enumerate(winds):
    if beg <= datetime.fromisoformat(date[0:13]) <= end:
        win.append(datetime.fromisoformat(date[0:13]))

win = np.array(win)

##############################################################################
# checking overlap

gtab = set(gtab)
gdat = set(gdat)
win = set(win)

win_gtab = win.intersection(gtab)
win_gdat = win.intersection(gdat)

diff = win_gtab.difference(win_gdat)
