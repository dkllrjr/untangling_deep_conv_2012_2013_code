##############################################################################
# By Doug Keller - Laboratoire de Météorologie Dynamique
#
# Script to take extract the Genoa Cyclone dataset provided by the
# Weizmann Institute of Science - Department of Earth and Planetary Sciences -
# Dr. Shira Raveh-Rubin and Yonatan Givon

import pickle
import scipy.io as sio

##############################################################################
# Cyclones are searched for within the 38-44 N, 4-14 E box in the ERA-Interim dataset

with open('data/Genua_Cyc_table.mat', 'rb') as file:
    data = sio.loadmat(file)

cyclones_raw = data['Genua_Cyc_dates']

cyclones = []
for i in range(len(cyclones_raw)):
    cyclones.append(i)

cyclone_dates = cyclones_raw[cyclones, 0]

# need this format '1979-01-01T12:00:00'; actually I don't, only need to convert to string

genoa_cyclones = []
for i in range(len(cyclone_dates)):
    c = str(cyclone_dates[i])
    genoa_cyclones.append(c[0:4]+'-'+c[4:6]+'-'+c[6:8]+'T12:00:00.000000000')

##############################################################################
# Saving new string list to a pickle

with open('data/genoa_cyclones.pickle', 'wb') as file:
    pickle.dump(genoa_cyclones, file)
