import pickle
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import welch, iirfilter, sosfiltfilt
from datetime import datetime, timedelta
from matplotlib.patches import Rectangle

##############################################################################
# data

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral = pickle.load(file)

with open('../data/mistral_dates_2021-10-26.pickle', 'rb') as file:
    winds = pickle.load(file)

##############################################################################
# build time

S = winds['all_wind']

ind = mistral['index']

t = np.arange(0, len(winds['all_time']))
T = []
beg = datetime(1979, 1, 1, 12, 0, 0)
for ti in t:
    T.append(beg + timedelta(days=int(ti)))

s = np.zeros(t.size)
s.fill(np.nan)

for i, strength in enumerate(mistral['strength']):
    s[mistral['event_index'][i]] = strength

# Is = np.where(s > 0, 0, S)

f_day = 1/86400  # Hz
f_2week = 1/86400/14  # Hz
f_month = 1/86400/30.4375  # Hz
f_quarter = 1/86400/91.3125  # Hz
f_year = 1/86400/365  # Hz

order = 20

fs = 1/86400  # Hz; sampling frequency of the variables

f_high = 1/86400/1.25  # Hz
f_low = 1/86400/25  # Hz

sos = iirfilter(order, f_low, fs=fs, output='sos', btype='highpass', ftype='butter', rs=40)
sh = sosfiltfilt(sos, S)

sos = iirfilter(order, f_low, fs=fs, output='sos', btype='lowpass', ftype='butter', rs=40)
sl = sosfiltfilt(sos, S)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandpass', ftype='butter', rs=40)
sbp = sosfiltfilt(sos, S)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)
sbs = sosfiltfilt(sos, S)

##############################################################################

# N = 2**12
# fm, Fm = welch(s, fs=fs, nfft=N, scaling='density')
# fh, Fh = welch(sh, fs=fs, nfft=N, scaling='density')
# fl, Fl = welch(sl, fs=fs, nfft=N, scaling='density')
# # fb, Fb = welch(sb, fs=fs, nfft=N, scaling='density')
# fS, FS = welch(S, fs=fs, nfft=N, scaling='density')
# # fI, FI = welch(Is, fs=fs, nfft=N, scaling='density')

# fsize = 16

# fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=400)

# ax.semilogy(fm, Fm, color='tab:blue', label='Mistral')
# ax.semilogy(fh, Fh, color='tab:green', label='Mistral')
# ax.semilogy(fl, Fl, color='tab:purple', label='Seasonal')
# # ax.semilogy(fb, Fb, color='tab:pink', label='Band')
# ax.semilogy(fS, FS, color='k', label='All Winds')

# # ax.plot(fm, Fm, color='tab:blue', label='Mistral')
# # ax.plot(fS, FS, color='k', label='All Winds')
# # ax.plot(fI, FI, color='tab:red', label='Non-Mistral')
# # ax.plot(fI, (FI + Fm)/2, color='tab:green')

# # ax.axvline(f_day, color='tab:red', linestyle='--', label='Daily')
# ax.axvline(f_2week, color='tab:green', linestyle='--', label='Biweekly')
# ax.axvline(f_month, color='tab:purple', linestyle='--', label='Monthly')
# # ax.axvline(f_quarter, color='tab:purple', label='Quarterly')
# ax.axvline(f_year, color='tab:pink', linestyle='--', label='Annually')

# # ax.set_xlim(fm[0], fm[-1])
# ax.set_xlim(fm[0], fm[-1]/3)

# ax.set_ylabel('Power Spectral Density $\\frac{|U|^2}{Hz}$', fontsize=fsize)
# ax.set_xlabel('Frequency $Hz$', fontsize=fsize)

# ax.legend()

# fig.tight_layout()

# fig.savefig('../plots/fourier_mistral.png')
# plt.close()

##############################################################################

fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=400)

# ax.plot(T, s, color='k', alpha=.7, label='Total')
# ax.plot(T, sh, color='tab:red', alpha=.7, label='Highpass')
ax.plot(T, sl, color='tab:green', alpha=.7, label='Lowpass')
ax.plot(T, sbp, color='tab:purple', alpha=.7, label='Bandpass')
ax.plot(T, sbs, color='tab:blue', alpha=.7, label='Bandstop')
ax.plot(T, S, color='k', alpha=.7, label='Control')

for axi, axis in enumerate([ax]):

    for event in mistral['events']:
        if event == mistral['events'][0] and axi == 0:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25, label='Mistrals')
        else:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25)

# ax.set_xlim(T[4000], T[4500])

ax.set_ylim(0, 30)

ax.legend()

fig.tight_layout()
fig.savefig('../plots/fourier_mistral_time_series.png')
