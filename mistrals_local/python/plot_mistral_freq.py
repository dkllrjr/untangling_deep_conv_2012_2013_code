import pickle
import matplotlib.pyplot as plt
import numpy as np

##############################################################################
# data

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral = pickle.load(file)

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    pmistral = pickle.load(file)

##############################################################################
# frequency

f_day = 1/86400  # Hz
f_2week = 1/86400/14  # Hz
f_month = 1/86400/31  # Hz
f_year = 1/86400/365  # Hz

f_mistral = 1/86400/np.array(mistral['period'])
f_pmistral = 1/86400/np.array(pmistral['period'])

fig, ax = plt.subplots(2, 1, figsize=(12, 8), dpi=400)

_, bins, _ = ax[0].hist(f_mistral, bins=30)

ax[1].hist(f_pmistral, bins=bins)

for _, axis in enumerate(ax):

    # axis.set_xlim(0, 1.01*f_mistral.max())
    axis.axvline(f_day, color='tab:green', linestyle='--', alpha=.75)  # cutoff frequency
    axis.axvline(f_2week, color='tab:green', linestyle='--', alpha=.75)  # cutoff frequency
    axis.axvline(f_month, color='tab:green', linestyle='--', alpha=.75)  # cutoff frequency
    axis.axvline(f_year, color='tab:green', linestyle='--', alpha=.75)  # cutoff frequency

fig.tight_layout()

fig.savefig('../plots/mistral_frequency_dist_2012_2013.png')
