import pickle
import numpy as np

##############################################################################

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################

precond_start = precond_data['events'][0][0]
precond_end = precond_data['events'][-1][-1]

combined = {}
var = ['events', 'duration', 'period']
for v in var:
    combined[v] = []

j = 0
for i, event in enumerate(mistral_data['events']):
    if event[-1] < precond_start or event[0] > precond_end:
        combined['events'].append([event[0], event[-1]])
        combined['duration'].append(mistral_data['duration'][i])
        combined['period'].append(mistral_data['period'][i])
    elif j < len(precond_data['events']):
        combined['events'].append([precond_data['events'][j][0], precond_data['events'][j][-1]])
        combined['duration'].append(precond_data['duration'][j])
        combined['period'].append(precond_data['period'][j])
        j += 1

combined['duration_ave'] = np.mean(combined['duration'])
combined['period_ave'] = np.mean(combined['period'])
combined['duration_std'] = np.std(combined['duration'])
combined['period_std'] = np.std(combined['period'])

##############################################################################

with open('../data/combined_mistrals.pickle', 'wb') as file:
    pickle.dump(combined, file)
