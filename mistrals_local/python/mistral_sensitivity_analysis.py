import numpy as np
import pickle
# import matplotlib.pyplot as plt
import pandas
from tabulate import tabulate

##############################################################################

with open('../data/mistral_dates_sensitivity.pickle', 'rb') as file:
    data = pickle.load(file)

##############################################################################

mistral_dicts = data['mistral_dates']

arc = []
speed = []
date = []

for i, item in enumerate(mistral_dicts):

    # print(item['theta_0'], '\t', round(float(item['theta_1']), 2), '\t', item['wind_thres'], '\t', len(item['mistral_dates']))
    arc.append(item['theta_0'] - item['theta_1'])
    speed.append(item['wind_thres'])
    date.append(len(item['mistral_dates']))

arc = np.array(arc)
speed = np.array(speed)
date = np.array(date)

arcs = arc[0:4]
speeds = speed[::4]
dates = date.reshape(6, 4)

# arcs, speeds = np.meshgrid(arcs, speeds)

df = pandas.DataFrame(dict(arc = arc, speed = speed, counts = date))
df = df.pivot(index='speed', columns='arc', values='counts')

print(tabulate(df, headers='keys'))
