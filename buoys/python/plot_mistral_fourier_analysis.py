import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas
from glob import glob
from scipy.signal import welch, iirfilter, sosfiltfilt, fftconvolve
from scipy.interpolate import interp1d
from datetime import datetime, timedelta

##############################################################################
# data

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral = pickle.load(file)

buoy_files = glob('../data/Lion_wind/*')
buoy_files.sort()

##############################################################################
# build time

date = np.array([], dtype=np.datetime64)
ws = np.array([])

# date = []
# ws = []

for file in buoy_files:
    tmp = pandas.read_csv(file, header=None, delimiter=';')

    date_tmp = np.array(pandas.to_datetime(tmp[0]))

    date = np.append(date, date_tmp)
    ws = np.append(ws, tmp[1])

    # date.append(date_tmp)
    # ws.append(tmp[1])

ind = np.where(ws<=0)
ws = np.delete(ws, ind)
date = np.delete(date, ind)

S = ws

order = 20

fs = 1/3600  # Hz; sampling frequency of the variables

f_high = 1/86400/1.25  # Hz
f_low = 1/86400/25  # Hz

sos = iirfilter(order, f_low, fs=fs, output='sos', btype='highpass', ftype='butter', rs=40)
sh = sosfiltfilt(sos, S)

sos = iirfilter(order, f_low, fs=fs, output='sos', btype='lowpass', ftype='butter', rs=40)
sl = sosfiltfilt(sos, S)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandpass', ftype='butter', rs=40)
sbp = sosfiltfilt(sos, S)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)
sbs = sosfiltfilt(sos, S)

##############################################################################

beg = datetime(2002, 1, 1, 1, 30)
end = datetime(2021, 1, 1, 1, 30)

dt = np.array([np.datetime64(beg, 'ns')])
i = 1
while dt[-1] < np.datetime64(end, 'ns'):
    dt = np.append(dt, np.datetime64(beg + timedelta(hours=3*i), 'ns'))
    i += 1

datex = date.astype('float64')
dtx = dt.astype('float64')

y = np.interp(dtx, datex, S)

# # N = 2 * int(15 * 24/1) + 1
N = 2 * int(15) + 1
ones = np.ones(N)/N

s = y[0:len(y)//8*8]
s = s.reshape((8, len(s)//8), order='F')

mvs = np.zeros_like(s)

for i, arr in enumerate(s):
    mvs[i, :] = fftconvolve(arr, ones, 'same')

mvs = mvs.flatten(order='F')

extra = y[len(y)//8*8::]
mvs = np.append(mvs, extra)

# s = S[0:len(S)//24*24]
# s = s.reshape((24, len(s)//24), order='F')

# mvs = np.zeros_like(s)

# for i, arr in enumerate(s):
    # mvs[i, :] = fftconvolve(arr, ones, 'same')

# mvs = mvs.flatten(order='F')

# extra = S[len(S)//24*24::]
# mvs = np.append(mvs, extra)

##############################################################################

fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=400)

ax.plot(date, sl, color='tab:green', alpha=.7, label='Lowpass')
ax.plot(date, sbp + sl, color='tab:purple', alpha=.7, label='Bandpass')
ax.plot(date, sbs, color='tab:blue', alpha=.7, label='Bandstop')
ax.plot(date, S, color='k', alpha=.7, label='Control')
ax.plot(dt, mvs, color='tab:red', alpha=.7, label='Moving Average')

for axi, axis in enumerate([ax]):

    for event in mistral['events']:
        if event == mistral['events'][0] and axi == 0:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25, label='Mistrals')
        else:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25)

ax.set_xlim(date[0], date[-1])
# ax.set_xlim(date[0], event[-1])
# ax.set_xlim(date[0], date[10000])

# ax.set_ylim(0, 30)

ax.legend()

fig.tight_layout()
fig.savefig('../plots/buoy_mistral_time_series.png')
plt.show()
