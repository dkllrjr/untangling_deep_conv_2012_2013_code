import xarray as xr
from glob import glob
import pandas
import numpy as np
import os
import matplotlib.pyplot as plt

##############################################################################

def find_lat_lon(loc,lat,lon):

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        #    (r0**2 + r1**2 - 2*r0*r1*(np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)))**.5
        a = r0**2 + r1**2
        b = 2*r0*r1
        c = np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)
        d = (a - b*c)
        return d**.5
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            # print(i,len(lat_lon))
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind


##############################################################################

azur_data_path = '../data/Azur/*SST*'
lion_data_path = '../data/Lion/*SST*'
nemo_data_path = os.environ['HOME'] + '/Data/PhD/DWF_GOL_SENSITIVITY/NEMO/NEMO-BULK-CONT/OCE/Output/HF/*2D.nc'

azur_data_paths = glob(azur_data_path)
lion_data_paths = glob(lion_data_path)
nemo_data_paths = glob(nemo_data_path)

azur_data_paths.sort()
lion_data_paths.sort()
nemo_data_paths.sort()

##############################################################################

azur_data_list = []
lion_data_list = []

for i, dat_file in enumerate(azur_data_paths):
    azur_data_list.append(pandas.read_table(dat_file, sep=';', usecols=['date', 'temperature']))
    lion_data_list.append(pandas.read_table(lion_data_paths[i], sep=';', usecols=['date', 'temperature']))

azur_data = pandas.concat(azur_data_list)
lion_data = pandas.concat(lion_data_list)

azur_dates = azur_data['date'].to_numpy(dtype=np.datetime64)
lion_dates = lion_data['date'].to_numpy(dtype=np.datetime64)

lion_data['temperature'] = lion_data['temperature'].where(((5<lion_data['temperature']) & (lion_data['temperature']<35)), other=np.nan)

##############################################################################

nemo_data = xr.open_mfdataset(nemo_data_paths)

loc = [[43.38, 7.83], [42.06, 4.64]]  # azur and lion buoy loc respectively
lat = nemo_data.nav_lat.values
lon = nemo_data.nav_lon.values

ind = find_lat_lon(loc, lat, lon)

nemo_azur_data = nemo_data.isel(x=ind[0][1], y=ind[0][0])
nemo_lion_data = nemo_data.isel(x=ind[1][1], y=ind[1][0])

nemo_azur_sst = nemo_azur_data.sosstsst
nemo_lion_sst = nemo_lion_data.sosstsst
nemo_date = nemo_azur_sst.time_counter.values

##############################################################################

tsize = 20
fsize = 16

fig, ax = plt.subplots(2, 1, figsize=(9, 6), dpi=300)

# azur

ax[0].plot(azur_dates, azur_data['temperature'], color='k', label='Buoy', linestyle='', marker='o', markersize=1, zorder=3)
ax[0].plot(nemo_date, nemo_azur_sst, color='tab:blue', label='NEMO')

ax[0].set_title('Azur $43.38^\\circ N$ $7.83^\\circ E$', fontsize=tsize)

# lion

ax[1].plot(lion_dates, lion_data['temperature'], color='k', label='Buoy', linestyle='', marker='o', markersize=1)
ax[1].plot(nemo_date, nemo_lion_sst, color='tab:blue', label='NEMO')

# ax[1].set_ylim([nemo_lion_sst.min(), nemo_lion_sst.max()])

ax[1].set_title('Lion $42.06^\\circ N$ $4.64^\\circ E$', fontsize=tsize)

# both

for axis in ax:
    axis.set_xlabel('Date', fontsize=fsize)
    axis.set_ylabel('$^\\circ C$', fontsize=fsize)
    axis.legend()
    axis.set_xlim([nemo_date[0], nemo_date[-1]])
    axis.tick_params(axis='x', labelsize=fsize-2, labelrotation=22.5)
    axis.tick_params(axis='y', labelsize=fsize-2, labelrotation=22.5)

# text label

# fig.text(.05, .9, 'a)', fontsize=fsize-2)
# fig.text(.05, .45, 'b)', fontsize=fsize-2)

# fig.suptitle('Gulf of Lion Buoy SST', fontsize=tsize+2)

fig.text(0.015, 0.95, '(a)', fontweight='bold', fontsize=fsize)
fig.text(0.015, 0.49, '(b)', fontweight='bold', fontsize=fsize)

fig.tight_layout()
fig.savefig('../plots/azur_lion_buoy_sst.png')

