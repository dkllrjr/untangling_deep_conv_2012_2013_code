import xarray as xr
from glob import glob
import pandas
import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import pickle
from scipy.signal import correlate
from scipy.stats import pearsonr
from sklearn.metrics import mean_squared_error

##############################################################################

def find_lat_lon(loc,lat,lon):

    def archav(rad):
        return np.arccos(1 - 2*rad)
        
    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            # print(i,len(lat_lon))
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind


def same_size(ax, ay, bx, by):
    ax, bx = np.array(ax), np.array(bx)
    ay, by = np.array(ay), np.array(by)

    if ax[0] < bx[0]:
        begx = ax[0]
    else:
        begx = bx[0]

    if ax[-1] > bx[-1]:
        endx = bx[-1]
    else:
        endx = ax[-1]

    dax, dbx = np.diff(ax), np.diff(bx)

    if dax.min() < dbx.min():
        dx = dax.min()
    else:
        dx = dbx.min()

    x = np.arange(begx, endx, dx)
    
    an, bn = np.interp(x, ax, ay), np.interp(x, bx, by)

    return x, an, bn
        

##############################################################################

with open('../data/ctd_data.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

##############################################################################

nemo_data_path = os.environ['HOME'] + '/Data/PhD/DWF_GOL_SENSITIVITY/NEMO/NEMO-CONT/OCE/Output/DA/*T.nc'

nemo_data_paths = glob(nemo_data_path)

nemo_data_paths.sort()

nemo_data = xr.open_mfdataset(nemo_data_paths)

##############################################################################

loc = []
for i, item in enumerate(ctd_data):
    loc.append([item['lat'], item['long']])
    
    if i == 4:
        ctd_data[i]['date'] = datetime(2013, 1, 30, 11, 49)

    if i == 5:
        ctd_data[i]['date'] = datetime(2013, 2, 20, 14, 29)

lat = nemo_data.nav_lat.values
lon = nemo_data.nav_lon.values

ind = find_lat_lon(loc, lat, lon)

nemo_temp = nemo_data.votemper

nemo_temp_prof_list = []
for i, item in enumerate(ctd_data):
    temp = nemo_temp.isel(y=ind[i][0], x=ind[i][1])
    nemo_temp_prof_list.append(temp.sel(time_counter=item['date'].isoformat(), method='nearest'))

##############################################################################

print('loaded data')

ctd = []
nemo = []
for i, item in enumerate(ctd_data):
    ctd.append(item)

    nemo_temp = np.where(nemo_temp_prof_list[i].values>0, nemo_temp_prof_list[i].values, np.nan)
    temp_isnotnan = ~np.isnan(nemo_temp)
    nemo.append((nemo_temp_prof_list[i].deptht.values[temp_isnotnan], nemo_temp[temp_isnotnan]))

rs = []
ctdns, nemons = [], []
depths = []
biass = []
rmses = []
for i, item in enumerate(ctd):
    depth, ctdn, nemon = same_size(item['depth'], item['temperature'], nemo[i][0], nemo[i][1])
    r, p = pearsonr(ctdn, nemon)
    rmse = mean_squared_error(ctdn, nemon, squared=False)
    bias = nemon - ctdn
    ctdns.append(ctdn)
    nemons.append(nemon)
    depths.append(depth)
    rs.append(r)
    biass.append(np.mean(bias))
    rmses.append(rmse)

data = {'ctd': ctd, 'nemo': nemo, 'loc': loc, 'depths': depths, 'overlap_nemo': nemons, 'overlap_ctd': ctdns, 'r': rs, 'rmse': rmses, 'bias': biass}
with open('../data/nemo_ctd_temp.pickle', 'wb') as file:
    pickle.dump(data, file)
