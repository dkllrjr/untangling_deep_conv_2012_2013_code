import pandas
import pickle
import numpy as np

##############################################################################

def hlines(latex_table, words_to_replace, replacing_words):
    
    for i, word in enumerate(words_to_replace):
        latex_table = latex_table.replace(word, replacing_words[i])

    return latex_table


##############################################################################

with open('../data/nemo_ctd_density.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/nemo_ctd_temp.pickle', 'rb') as file:
    temp_data = pickle.load(file)

ctd = data['ctd']
nemo = data['nemo']
rmse = data['rmse']
bias = data['bias']
loc = data['loc']
rmse_temp = temp_data['rmse']
bias_temp = temp_data['bias']

date = []
for i, item in enumerate(ctd):
    date.append(item['date'])

ave_rmse = np.mean(rmse)
rmse = np.round(rmse, 3)

ave_rmse_temp = np.mean(rmse_temp)
rmse_temp = np.round(rmse_temp, 3)

ave_bias = np.mean(bias)
bias = np.round(bias, 4)

ave_bias_temp = np.mean(bias_temp)
bias_temp = np.round(bias_temp, 4)

dates = []
lat = []
lon = []
for i, item in enumerate(date):
    dates.append(item.strftime('%Y-%m-%d-%H:%M'))
    lat.append(round(loc[i][0], 3))
    lon.append(round(loc[i][1], 3))

half = 4
ind_half = slice(0, half)
table_data = {'Date': dates, 'Lat.': lat, 'Lon.': lon, 'Rd': rmse, 'Rt': rmse_temp, 'biasd': bias, 'biast': bias_temp}
# table_data = {'Date': dates[ind_half], 'Lat.': lat[ind_half], 'Lon.': lon[ind_half], 'Rd': rmse[ind_half], 'Rt': rmse_temp[ind_half], 'biasd': bias[ind_half], 'biast': bias_temp[ind_half], 'Date1': dates[half::], 'Lat.1': lat[half::], 'Lon.1': lon[half::], 'Rd1': rmse[half::], 'Rt1': rmse_temp[half::], 'biasd1': bias[half::], 'biast1': bias_temp[half::]}
df = pandas.DataFrame(table_data)

##############################################################################

words_to_replace = ['table', 'begin{table*}', 'toprule', 'midrule', 'bottomrule']
replacing_words = ['table*', 'begin{table*}[t]', 'tophline', 'middlehline', 'bottomhline']

##############################################################################

table = df.to_latex(index=False, label='tab:ctd_nemo_corr', caption='')

table = hlines(table, words_to_replace, replacing_words)
table = table.replace('lrrrrrr', 'lcccccc')
# table = table.replace('lrrrrrrlrrrrrr', 'lcccccc|lcccccc')
# table = table.replace('Rd1', 'RMSE$_{\\rho}$')
# table = table.replace('Rt1', 'RMSE$_{\\theta}$')
table = table.replace('Rd', 'RMSE$_{\\rho}$')
table = table.replace('Rt', 'RMSE$_{\\theta}$')
table = table.replace('biasd', 'Bias$_{\\rho}$')
# table = table.replace('biasd1', 'Bias$_{\\rho}$')
table = table.replace('biast', 'Bias$_{\\theta}$')
# table = table.replace('biast1', 'Bias$_{\\theta}$')
# table = table.replace('Date1', 'Date')
# table = table.replace('Lat.1', 'Lat.')
# table = table.replace('Lon.1', 'Lon.')

print(table)

with open('../tables/ctd_nemo_corr.tex', 'w') as file:
    file.writelines(table)
