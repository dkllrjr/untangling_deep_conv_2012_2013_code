import xarray as xr
from glob import glob
import pandas
import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import pickle
from scipy.signal import correlate
from scipy.stats import pearsonr
from matplotlib.ticker import FormatStrFormatter

##############################################################################

with open('../data/nemo_ctd_density.pickle', 'rb') as file:
    data = pickle.load(file)

ctd = data['ctd']
nemo = data['nemo']
loc = data['loc']

date = []
for i, item in enumerate(ctd):
    date.append(item['date'])

sortdate = np.argsort(date)

date = list(np.array(date)[sortdate])
ctd = list(np.array(ctd)[sortdate])
loc = list(np.array(loc)[sortdate])
nemo = list(np.array(nemo)[sortdate])

##############################################################################

tsize = 24
fsize = 20

fig, ax = plt.subplots(2, 4, figsize=(18, 10), dpi=300)

ax = ax.flatten()

for i, axis in enumerate(ax):
    axis.plot(ctd[i]['density'], ctd[i]['depth'], color='k', label='$\\overline{\\rho_{CTD}}$')
    axis.fill_betweenx(ctd[i]['depth'], np.array(ctd[i]['density']) - np.array(ctd[i]['density_std']), np.array(ctd[i]['density']) + np.array(ctd[i]['density_std']), color='tab:red', alpha=.5, label='$\\pm \\sigma_{\\rho_{CTD}}$')
    axis.plot(nemo[i][1], nemo[i][0], color='tab:blue', label='$\\rho_{NEMO}$')
    axis.set_title(str(round(loc[i][0], 3)) + '$^\\circ$N ' + str(round(loc[i][1], 3)) + '$^\\circ$E\n' + date[i].strftime('%Y-%m-%d-%H:%M'), fontsize=tsize-2)

ax[0].legend(fontsize=fsize-4)

for axis in ax:
    axis.set_xlabel('Density $kg/m^3$', fontsize=fsize)
    axis.set_ylabel('Depth $m$', fontsize=fsize)
    axis.invert_yaxis()
    axis.tick_params(axis='x', labelsize=fsize-2)
    axis.tick_params(axis='y', labelsize=fsize-2)
    axis.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# fig.suptitle('Gulf of Lion CTD', fontsize=tsize+2)

fig.tight_layout()
fig.savefig('../plots/nemo_ctd_density.png')
