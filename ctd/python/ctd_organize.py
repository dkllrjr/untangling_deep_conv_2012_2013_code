import numpy as np
from glob import glob
from datetime import datetime
import pandas
import pickle

##############################################################################

def average_diff_len(arrs):
    
    # length = []
    # for arr in arrs:
        
    narr = []
    for i, arr in enumerate(arrs):
        for j, item in enumerate(arr):
            if len(narr) < len(arr):
                narr.append([])
            narr[j].append(item)

    mean_arr = []
    std_arr = []
    for sec in narr:
        mean_arr.append(np.mean(sec))
        std_arr.append(np.std(sec))

    return mean_arr, std_arr
            
##############################################################################

ctd_data_path = '../data/SOP2_CTD_10DB/*'

ctd_data_paths = glob(ctd_data_path)

ctd_data_paths.sort()

##############################################################################

ctd_data_list = []

for i, asc_file in enumerate(ctd_data_paths):
    ctd_data_list.append(pandas.read_csv(asc_file, sep='\t'))

density_index = ctd_data_list[0].columns[12]
depth_index = ctd_data_list[0].columns[9]
temperature_index = ctd_data_list[0].columns[10]

loc = []
date = []
posix = []
for i, item in enumerate(ctd_data_list):
    month = item.columns[5]
    day = item.columns[6]
    loc.append([item.Lat[0], item.Long[0]])
    date.append(datetime(item.Year[0], item[month][0], item[day][0], int(item.Time[0][0:2]), int(item.Time[0][3:5]), int(item.Time[0][6:8])))
    posix.append(date[-1].timestamp())

# sorted_prof_ind = [[0, 1],
        # [2, 3, 4, 5, 6],
        # [7, 8, 9, 10],
        # [11, 12],
        # [13, 14, 15, 16, 17, 18, 19, 20, 21],
        # [22, 23, 24],
        # [25, 26],
        # [27]]

sorted_prof_ind = [slice(0, 2), slice(2, 7), slice(7, 11), slice(11, 13), slice(13, 22), slice(22, 25), slice(25, 27), slice(27, 28)]

data = []
for ind in sorted_prof_ind:
    tmp_time = posix[ind]
    tmp_lat = []
    tmp_long = []
    tmp_depth = []
    tmp_density = []
    tmp_temperature = []
    for i, item in enumerate(ctd_data_list[ind]):
        tmp_lat.append(item.Lat[0])
        tmp_long.append(item.Long[0])
        tmp_depth.append(item.DepSM)
        tmp_density.append(item[density_index])
        tmp_temperature.append(item[temperature_index])

    tmp_time = np.mean(tmp_time)
    tmp_lat = np.mean(tmp_lat)
    tmp_long = np.mean(tmp_long)
    mean_depth, _ = average_diff_len(tmp_depth)
    mean_density, std_density = average_diff_len(tmp_density)
    mean_temperature, std_temperature = average_diff_len(tmp_temperature)

    data.append({'lat': tmp_lat, 'long': tmp_long, 'date': datetime.fromtimestamp(tmp_time), 'depth': mean_depth, 'density': mean_density, 'temperature': mean_temperature, 'density_std': std_density, 'temperature_std': std_temperature})

##############################################################################

with open('../data/ctd_data.pickle', 'wb') as file:
    pickle.dump(data, file)
