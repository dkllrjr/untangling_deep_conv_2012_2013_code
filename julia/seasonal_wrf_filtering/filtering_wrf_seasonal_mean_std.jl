using Distributed,Glob,NCDatasets,Missings,Serialization

##############################################################################
# loading data

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

Tq_data = data_path*"raw_Tq_2012_2013/"
uv_data = data_path*"raw_uv_2012_2013/"

T_files = glob("*TAS.nc",Tq_data)
q_files = glob("*HUSS.nc",Tq_data)
u_files = glob("*UAS.nc",uv_data)
v_files = glob("*VAS.nc",uv_data)

T_files = sort(T_files)
q_files = sort(q_files)
u_files = sort(u_files)
v_files = sort(v_files)

println("starting loading nc files")

T,q,u,v = let T = [],q = [],u = [],v = []
    for i = 1:length(T_files)
        Tnc = NCDataset(T_files[i])
        T = append!(T,[disallowmissing(Array(Tnc["tas"]))])
        close(Tnc)
        
        qnc = NCDataset(q_files[i])
        q = append!(q,[disallowmissing(Array(qnc["huss"]))])
        close(qnc)

        unc = NCDataset(u_files[i])
        u = append!(u,[disallowmissing(Array(unc["uas"]))])
        close(unc)

        vnc = NCDataset(v_files[i])
        v = append!(v,[disallowmissing(Array(vnc["vas"]))])
        close(vnc)

        println(i)
    end
    T,q,u,v
end

println("finished loading nc arrays; concatenating arrays")

println(size(T[1]),size(T[2]))

T = cat(T[1],T[2],dims=3)
q = cat(q[1],q[2],dims=3)
u = cat(u[1],u[2],dims=3)
v = cat(v[1],v[2],dims=3)

println("finished concatenating")

##############################################################################
# multiprocessing time baby!

#region multiprocessing

addprocs(4)

@everywhere include(expanduser("~") * "/PhD/julia/nc_mod.jl")

println("loaded modules into workers")

N = 120 #3 hourly data
dN = 4

Tm = nc_mod.moving_mean(T[190,190,:],N)
Ts = nc_mod.moving_std(T[190,190,:],N)

Tm = @spawnat :any nc_mod.multi_array_moving_mean(T,N)
qm = @spawnat :any nc_mod.multi_array_moving_mean(q,N)
um = @spawnat :any nc_mod.multi_array_moving_mean(u,N)
vm = @spawnat :any nc_mod.multi_array_moving_mean(v,N)

Tm,qm,um,vm = fetch(Tm),fetch(qm),fetch(um),fetch(vm)

serialize(data_path*"mean_data.jls",[Tm,qm,um,vm])

Tm,qm,um,vm = nothing,nothing,nothing,nothing

GC.gc()

Ts = @spawnat :any nc_mod.multi_array_moving_std(T,N)
qs = @spawnat :any nc_mod.multi_array_moving_std(q,N)
us = @spawnat :any nc_mod.multi_array_moving_std(u,N)
vs = @spawnat :any nc_mod.multi_array_moving_std(v,N)

Ts,qs,us,vs = fetch(Ts),fetch(qs),fetch(us),fetch(vs)

serialize(data_path*"std_data.jls",[Ts,qs,us,vs])

Ts,qs,us,vs = nothing,nothing,nothing,nothing

GC.gc()

println("done with calculating the moving mean")

Tds = @spawnat :any nc_mod.multi_array_moving_std(T,dN)
qds = @spawnat :any nc_mod.multi_array_moving_std(q,dN)
uds = @spawnat :any nc_mod.multi_array_moving_std(u,dN)
vds = @spawnat :any nc_mod.multi_array_moving_std(v,dN)

Tds,qds,uds,vds = fetch(Tds),fetch(qds),fetch(uds),fetch(vds)

serialize(data_path*"dstd_data.jls",[Tds,qds,uds,vds])

Tds,qds,uds,vds = nothing,nothing,nothing,nothing

rmprocs(workers())

println("done with multiprocessing")

#endregion

##############################################################################
