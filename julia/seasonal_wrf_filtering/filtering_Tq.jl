using Distributed,Glob,NCDatasets,Serialization,Missings,Plots

##############################################################################
# loading data

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

Tq_data = data_path*"raw_Tq_2012_2013/"

T_files = glob("*TAS.nc",Tq_data)
q_files = glob("*HUSS.nc",Tq_data)

T_files = sort(T_files)
q_files = sort(q_files)

println("starting loading nc files")

T,q,u,v = let T = [],q = [],u = [],v = []
    for i = 1:length(T_files)
        Tnc = NCDataset(T_files[i])
        T = append!(T,[disallowmissing(Array(Tnc["tas"]))])
        close(Tnc)
        
        qnc = NCDataset(q_files[i])
        q = append!(q,[disallowmissing(Array(qnc["huss"]))])
        close(qnc)

        println(i)
    end
    T,q,u,v
end

println("finished loading nc arrays; concatenating arrays")

println(size(T[1]),size(T[2]))

T = cat(T[1],T[2],dims=3)
q = cat(q[1],q[2],dims=3)

println("finished concatenating")

Tm,qm,_,_ = deserialize(data_path*"mean_data.jls")
# Ts,qs,_,_ = deserialize(data_path*"std_data.jls")
# Tds,qds,_,_ = deserialize(data_path*"dstd_data.jls")

##############################################################################
# multiprocessing time baby!

#region multiprocessing

addprocs(4)

@everywhere include(expanduser("~") * "/PhD/julia/nc_mod.jl")

println("loaded modules into workers")

println("filtering")

a = .25
Tn = @spawnat :any nc_mod.tracer_field_filter(T,Tm,a)
qn = @spawnat :any nc_mod.tracer_field_filter(q,qm,a)

Tn,qn = fetch(Tn),fetch(qn)

rmprocs(workers())

serialize(data_path*"Tq_filtered.jls",[Tn,qn])

println("done with multiprocessing")

#endregion

plot(qn[190,190,:],size=(1250,750))
plot!(q[190,190,:],size=(1250,750))
plot!(qTm[190,190,:],size=(1250,750))