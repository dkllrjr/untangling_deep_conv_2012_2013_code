using Glob,NCDatasets,Serialization

##############################################################################
# loading data

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

new_data_path = data_path * "seasonal_wrf"

T_files = glob("*TAS.nc",new_data_path)
q_files = glob("*HUSS.nc",new_data_path)
u_files = glob("*UAS.nc",new_data_path)
v_files = glob("*VAS.nc",new_data_path)

T_files = sort(T_files)
q_files = sort(q_files)
u_files = sort(u_files)
v_files = sort(v_files)

T,q = deserialize(data_path*"Tq_filtered.jls")
u,v = deserialize(data_path*"uv_filtered.jls")

ind = [1:2928,2929:5848]

println("starting loading nc files")

for i = 1:length(T_files)

    Tnc = NCDataset(T_files[i],"a")
    println(Tnc["tas"][1,1,1:10])
    Tnc["tas"][:,:,:] = T[:,:,ind[i]]
    println(Tnc["tas"][1,1,1:10])
    close(Tnc)
    
    qnc = NCDataset(q_files[i],"a")
    qnc["huss"][:,:,:] = q[:,:,ind[i]]
    close(qnc)

    unc = NCDataset(u_files[i],"a")
    unc["uas"][:,:,:] = u[:,:,ind[i]]
    close(unc)

    vnc = NCDataset(v_files[i],"a")
    vnc["vas"][:,:,:] = v[:,:,ind[i]]
    close(vnc)

    println(i)

end

println("finished editing nc files")
