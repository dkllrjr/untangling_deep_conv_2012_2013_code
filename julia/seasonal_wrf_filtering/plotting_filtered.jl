using Serialization,Plots,Glob,NCDatasets,Missings

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

Tq_data = data_path*"raw_Tq_2012_2013/"
uv_data = data_path*"raw_uv_2012_2013/"

T_files = glob("*TAS.nc",Tq_data)
q_files = glob("*HUSS.nc",Tq_data)
u_files = glob("*UAS.nc",uv_data)
v_files = glob("*VAS.nc",uv_data)

T_files = sort(T_files)
q_files = sort(q_files)
u_files = sort(u_files)
v_files = sort(v_files)

println("starting loading nc files")

T,q,u,v = let T = [],q = [],u = [],v = []
    for i = 1:length(T_files)
        Tnc = NCDataset(T_files[i])
        T = append!(T,[disallowmissing(Array(Tnc["tas"]))])
        close(Tnc)
        
        qnc = NCDataset(q_files[i])
        q = append!(q,[disallowmissing(Array(qnc["huss"]))])
        close(qnc)

        unc = NCDataset(u_files[i])
        u = append!(u,[disallowmissing(Array(unc["uas"]))])
        close(unc)

        vnc = NCDataset(v_files[i])
        v = append!(v,[disallowmissing(Array(vnc["vas"]))])
        close(vnc)

        println(i)
    end
    T,q,u,v
end

println("finished loading nc arrays; concatenating arrays")

println(size(T[1]),size(T[2]))

T = cat(T[1],T[2],dims=3)
q = cat(q[1],q[2],dims=3)
u = cat(u[1],u[2],dims=3)
v = cat(v[1],v[2],dims=3)

T = T[190,190,:]
q = q[190,190,:]
u = u[190,190,:]
v = v[190,190,:]

w = (u.^2 + v.^2).^.5

GC.gc()

println("finished concatenating")

##############################################################################

Tn,qn = deserialize(data_path*"Tq_filtered.jls")
un,vn = deserialize(data_path*"uv_filtered.jls")

Tn = Tn[190,190,:]
qn = qn[190,190,:]
un = un[190,190,:]
vn = vn[190,190,:]

wn = (un.^2 + vn.^2).^.5

GC.gc()

println("finished grabbing filtered data")

Tm,qm,um,vm = deserialize(data_path*"mean_data.jls")

Tm = Tm[190,190,:]
qm = qm[190,190,:]
um = um[190,190,:]
vm = vm[190,190,:]

wm = (um.^2 + vm.^2).^.5

GC.gc()

println("finished grabbing mean data")

# plot(T,size=(1250,750))
# plot!(Tn,size=(1250,750))
# plot!(Tm,size=(1250,750))

# plot(q,size=(1250,750))
# plot!(qn,size=(1250,750))
# plot!(qm,size=(1250,750))

plot(w,size=(1250,750))
plot!(wn,size=(1250,750))
plot!(wm,size=(1250,750))
