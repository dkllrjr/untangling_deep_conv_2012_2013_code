using Distributed,Glob,NCDatasets,Serialization,Missings,Plots

##############################################################################
# loading data

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

uv_data = data_path*"raw_uv_2012_2013/"

u_files = glob("*UAS.nc",uv_data)
v_files = glob("*VAS.nc",uv_data)

u_files = sort(u_files)
v_files = sort(v_files)

println("starting loading nc files")

u,v = let u = [],v = []
    for i = 1:length(u_files)
        
        unc = NCDataset(u_files[i])
        u = append!(u,[disallowmissing(Array(unc["uas"]))])
        close(unc)

        vnc = NCDataset(v_files[i])
        v = append!(v,[disallowmissing(Array(vnc["vas"]))])
        close(vnc)

        println(i)
    end
    u,v
end

println("finished loading nc arrays; concatenating arrays")

println(size(u[1]),size(u[2]))

u = cat(u[1],u[2],dims=3)
v = cat(v[1],v[2],dims=3)

println("finished concatenating")

_,_,um,vm = deserialize(data_path*"mean_data.jls")
# _,_,us,vs = deserialize(data_path*"std_data.jls")
# _,_,uds,vds = deserialize(data_path*"dstd_data.jls")

##############################################################################
# multiprocessing time baby!

#region multiprocessing

include(expanduser("~") * "/PhD/julia/nc_mod.jl")

println("filtering")

a = .25
@time un,vn = nc_mod.wind_field_filter(u,v,um,vm,.25)

serialize(data_path*"uv_filtered.jls",[un,vn])

println("done")

#endregion

w = (u[190,190,:].^2 + v[190,190,:].^2).^.5
wm = (um[190,190,:].^2 + vm[190,190,:].^2).^.5
wn = (un[190,190,:].^2 + vn[190,190,:].^2).^.5

plot(w,size=(1250,750))
plot!(wm,size=(1250,750))
plot!(wn,size=(1250,750))

plot(v[190,190,:],size=(1250,750))
plot!(vm[190,190,:],size=(1250,750))
plot!(vn[190,190,:],size=(1250,750))