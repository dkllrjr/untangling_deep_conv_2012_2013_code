#!/usr/bin/bash

cd $HOME/Data/France_PhD_data/seasonal_wrf_forcing/

mkdir -p seasonal_wrf2/

cp -r raw_Tq_2012_2013/* seasonal_wrf2/
cp -r raw_uv_2012_2013/* seasonal_wrf2/
