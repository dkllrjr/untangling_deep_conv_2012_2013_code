using Distributed,Glob,NCDatasets,Missings,JLD,Plots

##############################################################################
# loading data

data_path = expanduser("~") * "/Data/France_PhD_data/seasonal_wrf_forcing/"

Tq_data = data_path*"raw_Tq_2012_2013/"
uv_data = data_path*"raw_uv_2012_2013/"

T_files = glob("*TAS.nc",Tq_data)
q_files = glob("*HUSS.nc",Tq_data)
u_files = glob("*UAS.nc",uv_data)
v_files = glob("*VAS.nc",uv_data)

T_files = sort(T_files)
q_files = sort(q_files)
u_files = sort(u_files)
v_files = sort(v_files)

println("starting loading nc files")

T,q,u,v = let T = [],q = [],u = [],v = []
    for i = 1:length(T_files)
        Tnc = NCDataset(T_files[i])
        T = append!(T,[disallowmissing(Array(Tnc["tas"]))])
        close(Tnc)
        
        qnc = NCDataset(q_files[i])
        q = append!(q,[disallowmissing(Array(qnc["huss"]))])
        close(qnc)

        unc = NCDataset(u_files[i])
        u = append!(u,[disallowmissing(Array(unc["uas"]))])
        close(unc)

        vnc = NCDataset(v_files[i])
        v = append!(v,[disallowmissing(Array(vnc["vas"]))])
        close(vnc)

        println(i)
    end
    T,q,u,v
end

println("finished loading nc arrays; concatenating arrays")

println(size(T[1]),size(T[2]))

T = cat(T[1],T[2],dims=3)
q = cat(q[1],q[2],dims=3)
u = cat(u[1],u[2],dims=3)
v = cat(v[1],v[2],dims=3)

println("finished concatenating")

##############################################################################
# multiprocessing time baby!

#region multiprocessing

addprocs(4)

@everywhere include(expanduser("~") * "/PhD/julia/seasonal_wrf_filtering/nc_mod.jl")

println("loaded modules into workers")

N = 15 #3 hourly data
s = 8

Tn = @spawnat :any nc_mod.field_moving_mean_by_hour(T,N,s)
qn = @spawnat :any nc_mod.field_moving_mean_by_hour(q,N,s)
un = @spawnat :any nc_mod.field_moving_mean_by_hour(u,N,s)
vn = @spawnat :any nc_mod.field_moving_mean_by_hour(v,N,s)

Tn,qn,un,vn = fetch(Tn),fetch(qn),fetch(un),fetch(vn)

rmprocs(workers())

println("done with multiprocessing")

#endregion

save(data_path*"mean_data.jld","Tn",Tn,"qn",qn,"un",un,"vn",vn)

println("finished successfully")

##############################################################################

pyplot(size=(3000,1000))

T = T[190,190,:]
Tn = Tn[190,190,:]

q = q[190,190,:]
qn = qn[190,190,:]

u = u[190,190,:]
un = un[190,190,:]

v = v[190,190,:]
vn = vn[190,190,:]

θ = atan.(v,u)
θn = atan.(vn,un)

w = (u.^2 + v.^2).^.5
wn = (un.^2 + vn.^2).^.5

filt_plot = plot([T q u v θ w Tn qn un vn θn wn],layout=(6,1),label=["T" "q" "u" "v" "θ" "|U|" "Tₙ" "qₙ" "uₙ" "vₙ" "θₙ" "|Uₙ|"])
savefig(filt_plot,"seasonally_filtered_var.png")