# Code to Analyze and Plot Data for [Untangling the mistral and seasonal atmospheric forcing driving deep convection in the Gulf of Lion: 2012-2013](https://doi.org/10.5194/os-2021-72)

This repo contains the code used to generate plots used to analyze the data for [Untangling the mistral and seasonal atmospheric forcing driving deep convection in the Gulf of Lion: 2012-2013](https://doi.org/10.5194/os-2021-72).

## Citation

To cite this repo, please use:

Keller, D.: Code to Analyze and Plot Data for Untangling the mistral and seasonal atmospheric forcing driving deep convection in the Gulf of Lion: 2012-2013, GitLab, GitLab repository, https://gitlab.com/dkllrjr/untangling_deep_conv_2012_2013_code, 2022.

or with BibTeX:

```
@misc{KellerCode2022,
    author = {Keller, Douglas},
    title = {Code to Analyze and Plot Data for Untangling the mistral and seasonal atmospheric forcing driving deep convection in the Gulf of Lion: 2012-2013},
    year = {2022},
    publisher = {GitLab},
    journal = {GitLab repository},
    howpublished = {\url{https://gitlab.com/dkllrjr/untangling_deep_conv_2012_2013_code}}
}
```
