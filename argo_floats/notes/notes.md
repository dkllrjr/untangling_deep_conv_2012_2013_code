# argo file name

File name: GL_yyyyMM_TT_PF_XXXXXXX.nc
where:
- XXXXXXX: float code
- yyyy: year
- MM: month
- TT: data type (PR = vertical profiles, TS = Trajectory data)

# run scripts

organize_data.py
overlapping_dates_data.py
argo_nemo_loc.py
argo_data_temp.py
