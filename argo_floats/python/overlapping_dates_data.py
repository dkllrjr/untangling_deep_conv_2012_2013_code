import pickle
from datetime import datetime
import numpy as np

##############################################################################

with open('../data/argo_profiles.pickle', 'rb') as file:
    data = pickle.load(file)

##############################################################################

beg = datetime(2012, 8, 1, 0)
end = datetime(2013, 8, 1, 0)

ind = []
for ti, t in enumerate(data['time']):
    if  beg < t < end:
        ind.append(ti)

ndata = {}
for key in data.keys():
    ndata[key] = np.array(data[key], dtype=object)[ind]

##############################################################################

with open('../data/argo_profiles_2012_2013.pickle', 'wb') as file:
    pickle.dump(ndata, file)

