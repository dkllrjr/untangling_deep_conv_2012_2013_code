import xarray as xr
import matplotlib.pyplot as plt
from glob import glob
import numpy as np
import seawater as sw  # eos80
import pandas
import pickle

##############################################################################

def average_diff_len(arrs):
    
    narr = []
    for i, arr in enumerate(arrs):
        for j, item in enumerate(arr):
            if len(narr) < len(arr):
                narr.append([])
            narr[j].append(item)

    mean_arr = []
    std_arr = []
    for sec in narr:
        mean_arr.append(np.nanmean(sec))
        std_arr.append(np.nanstd(sec))

    return mean_arr, std_arr

##############################################################################

data_paths = glob('../data/*PR*.nc')
data_paths.sort()

# data_paths = data_paths[0:5]

times = []
lats = []
lons = []
depths = []
mean_rhos = []
std_rhos = []
mean_temps = []
std_temps = []

bad_files = []

for loc, data_path in enumerate(data_paths):

    x = xr.open_dataset(data_path)

    psu = x.PSAL.values
    pres = x.PRES.values
    lat = x.LATITUDE.values
    lon = x.LONGITUDE.values
    t = pandas.to_datetime(x.TIME.values)
    depth = sw.dpth(pres.T, lat)
    depth = depth.T
    temp = x.TEMP.values
    temp = sw.ptmp(psu, temp, pres)
    rho = sw.pden(psu, temp, pres) - 1000

    if 30 > np.min(lat) or 50 < np.max(lat):
        bad_files.append(data_path)

    tmp_ind = [[]]
    for ti, _ in enumerate(t):
        
        if ti < len(t)-1:
            if t[ti].day < t[ti+1].day:
                tmp_ind[-1].append(ti)
                tmp_ind.append([])

            else:
                tmp_ind[-1].append(ti)
        else:
            tmp_ind[-1].append(ti)

    tmp_slice = []
    for ind in tmp_ind:
        tmp_slice.append(slice(ind[0], ind[-1]+1))

    # print(loc, np.nanmax(depth))

    for s in tmp_slice:

        if 30 > np.min(lat[s]) or 50 < np.max(lat[s]):
            pass
        
        elif s.stop - s.start > 1:
            mean_depth, _ = average_diff_len(depth[s])
            mean_rho, std_rho = average_diff_len(rho[s])
            mean_temp, std_temp = average_diff_len(temp[s])

            isnan = np.isnan(mean_temp)

            mean_depth = np.delete(mean_depth, isnan)
            mean_rho = np.delete(mean_rho, isnan)
            mean_temp = np.delete(mean_temp, isnan)
            std_rho = np.delete(std_rho, isnan)
            std_temp = np.delete(std_temp, isnan)

            if len(mean_depth) > 0:

                if np.all((mean_rho > 15) & (mean_rho < 40)):
                    times.append(t[s].mean())
                    lats.append(np.mean(lat[s]))
                    lons.append(np.mean(lon[s]))

                    depths.append(mean_depth)
                    mean_rhos.append(mean_rho)
                    std_rhos.append(std_rho)
                    mean_temps.append(mean_temp)
                    std_temps.append(std_temp)

        else:
    
            tmp_t = t[s][0]
            tmp_lat = lat[s][0]
            tmp_lon = lon[s][0]

            mean_depth = depth[s][0]
            mean_rho = rho[s][0]
            mean_temp = temp[s][0]

            std_rho = np.zeros(len(mean_rho))
            std_rho.fill(np.nan)

            std_temp = np.zeros(len(mean_temp))
            std_temp.fill(np.nan)

            isnan = np.isnan(mean_temp)

            mean_depth = np.delete(mean_depth, isnan)
            mean_rho = np.delete(mean_rho, isnan)
            mean_temp = np.delete(mean_temp, isnan)
            std_rho = np.delete(std_rho, isnan)
            std_temp = np.delete(std_temp, isnan)

            if len(mean_depth) > 0:

                if np.all((mean_rho > 15) & (mean_rho < 40)):
                    times.append(tmp_t)
                    lats.append(tmp_lat)
                    lons.append(tmp_lon)

                    depths.append(mean_depth)
                    mean_rhos.append(mean_rho)
                    std_rhos.append(std_rho)
                    mean_temps.append(mean_temp)
                    std_temps.append(std_temp)

data = {'time': times, 'lat': lats, 'lon': lons, 'depth': depths, 'rho_mean': mean_rhos, 'rho_std': std_rhos, 'temp_mean': mean_temps, 'temp_std': std_temps}

with open('../data/argo_profiles.pickle', 'wb') as file:
    pickle.dump(data, file)
