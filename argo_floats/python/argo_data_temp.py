import xarray as xr
from glob import glob
import pandas
import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import pickle
from scipy.signal import correlate
from scipy.stats import pearsonr
from sklearn.metrics import mean_squared_error

##############################################################################

def same_size(ax, ay, bx, by):
    ax, bx = np.array(ax), np.array(bx)
    ay, by = np.array(ay), np.array(by)

    if ax[0] < bx[0]:
        begx = ax[0]
    else:
        begx = bx[0]

    if ax[-1] > bx[-1]:
        endx = bx[-1]
    else:
        endx = ax[-1]

    dax, dbx = np.diff(ax), np.diff(bx)

    if dax.min() < dbx.min():
        dx = dax.min()
    else:
        dx = dbx.min()

    if dx < 0.01:
        dx = 0.01

    print(begx, endx, dx)

    x = np.arange(begx, endx, dx)
    
    an, bn = np.interp(x, ax, ay), np.interp(x, bx, by)

    return x, an, bn
        

##############################################################################

with open('../data/nemo_argo_loc.pickle', 'rb') as file:
    loc_data = pickle.load(file)

ind = loc_data['ind']
loc = loc_data['loc']

with open('../data/argo_profiles_2012_2013.pickle', 'rb') as file:
    argo_data = pickle.load(file)

##############################################################################

nemo_data_path = os.environ['HOME'] + '/Data/PhD/DWF_GOL_SENSITIVITY/NEMO/NEMO-BULK-CONT/OCE/Output/DA/*T.nc'

nemo_data_path = os.environ['HOME'] + '/Data/phd_temp/nemoT/*T.nc'

nemo_data_paths = glob(nemo_data_path)

nemo_data_paths.sort()

nemo_data = xr.open_mfdataset(nemo_data_paths, concat_dim="time_counter")

##############################################################################

nemo_temps = nemo_data.votemper

nemo_temp_prof_list = []
for i, t in enumerate(argo_data['time']):
    print(i)
    tmp = nemo_temps.isel(y=ind[i][0], x=ind[i][1])
    nemo_temp_prof_list.append(tmp.sel(time_counter=t.isoformat(), method='nearest'))

##############################################################################

print('loaded data')

argo = []
nemo = []
fix_ind = []
for i, temp in enumerate(argo_data['temp_mean']):
    print(i)

    nemo_temp = np.where(nemo_temp_prof_list[i].values>0, nemo_temp_prof_list[i].values, np.nan)

    if len(nemo_temp) > 0:

        if np.any(argo_data['depth'][i]<0):
            pass
        elif len(temp) > 3:
            temp_isnotnan = ~np.isnan(nemo_temp)

            nemo.append((nemo_temp_prof_list[i].deptht.values[temp_isnotnan], nemo_temp[temp_isnotnan]))
            argo.append(temp)

            fix_ind.append(i)

print('getting profiles')

fix_fix_ind = []
argo_depths = np.array(argo_data['depth'])[fix_ind]
rs = []
argons, nemons = [], []
depths = []
biass = []
rmses = []
for i, item in enumerate(argo):
    print(i)

    try:
        depth, argon, nemon = same_size(argo_depths[i], item, nemo[i][0], nemo[i][1])
        r, p = pearsonr(argon, nemon)
        rmse = mean_squared_error(argon, nemon, squared=False)
        bias = nemon - argon
        argons.append(argon)
        nemons.append(nemon)
        depths.append(depth)
        rs.append(r)
        biass.append(np.mean(bias))
        rmses.append(rmse)
        fix_fix_ind.append(i)
    except:
        pass

loc = np.array(loc)[fix_ind][fix_fix_ind]
argo = np.array(argo, dtype=object)[fix_fix_ind]
nemo = np.array(nemo, dtype=object)[fix_fix_ind]

print('calculated rms')

data = {'argo': argo, 'nemo': nemo, 'loc': loc, 'depths': depths, 'overlap_nemo': nemons, 'overlap_argo': argons, 'r': rs, 'rmse': rmses, 'bias': biass}
with open('../data/nemo_argo_temp.pickle', 'wb') as file:
    pickle.dump(data, file)
