import xarray as xr
from glob import glob
import pandas
import numpy as np
import os
import matplotlib.pyplot as plt
from datetime import datetime
import pickle
from scipy.signal import correlate
from scipy.stats import pearsonr
from sklearn.metrics import mean_squared_error
import multiprocessing as mp
import time

##############################################################################

def find_lat_lon(loc, lat, lon):

    def archav(rad):
        return np.arccos(1 - 2*rad)
        
    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))

    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)

    def lat_lon_near(lat, lon, loc):
        E_r = 6371000  # Earth's radius
        d = dist_sphere_deg(E_r, lon, lat, E_r, loc[1], loc[0])
        ind = np.unravel_index(d.argmin(), d.shape)
        return (lat[ind], lon[ind]), ind

    ind = []
    
    for i, loci in enumerate(loc):
        _, tmp = lat_lon_near(lat, lon, loci)
        ind.append(tmp)
    
    return ind 
    
##############################################################################

with open('../data/argo_profiles_2012_2013.pickle', 'rb') as file:
    argo_data = pickle.load(file)

##############################################################################

nemo_data_path = os.environ['HOME'] + '/Data/PhD/DWF_GOL_SENSITIVITY/NEMO/NEMO-BULK-CONT/OCE/Output/DA/*T.nc'

nemo_data_path = os.environ['HOME'] + '/Data/phd_temp/nemoT/*T.nc'

nemo_data_paths = glob(nemo_data_path)

nemo_data_paths.sort()

nemo_data = xr.open_mfdataset(nemo_data_paths, concat_dim="time_counter")

##############################################################################

loc = []
for i, lat in enumerate(argo_data['lat']):
    loc.append([lat, argo_data['lon'][i]])

# nloc = []
# for loci in loc:
    # if 40 < loci[0] < 44 and 2 < loci[1] < 8:
        # nloc.append(loci)

# print(len(loc), len(nloc))

# loc = nloc

print('found locs')

lat = nemo_data.nav_lat.values
lon = nemo_data.nav_lon.values

##############################################################################

ind = find_lat_lon(loc, lat, lon)

data = {'ind': ind, 'loc': loc}
with open('../data/nemo_argo_loc.pickle', 'wb') as file:
    pickle.dump(data, file)
