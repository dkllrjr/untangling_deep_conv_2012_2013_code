import pickle
import numpy as np

##############################################################################

with open('../data/nemo_argo_temp.pickle', 'rb') as file:
    argo_data = pickle.load(file)

##############################################################################

argo_loc = argo_data['loc']

ind = []
for i, loc in enumerate(argo_loc):
    if 40 < loc[0] < 44:
        if 2 < loc[1] < 8:
            ind.append(i)

##############################################################################

bias = np.array(argo_data['bias'])[ind]
R = np.array(argo_data['r'])[ind]
rmse = np.array(argo_data['rmse'])[ind]

nbias = np.nanmean(np.abs(bias))
nR = np.nanmean(R)
nrmse = np.nanmean(rmse)
