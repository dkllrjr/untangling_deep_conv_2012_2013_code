import pickle
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle

##############################################################################

with open('../data/nemo_argo_temp.pickle', 'rb') as file:
    argo_data = pickle.load(file)

with open('../data/nemo_ctd_temp.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

##############################################################################

argo_loc = argo_data['loc']

ind = []
for i, loc in enumerate(argo_loc):
    if 40 < loc[0] < 44:
        if 2 < loc[1] < 8:
            ind.append(i)

argo_loc = argo_loc[ind]

fig, ax = plt.subplots(1, 1, figsize=(8, 5.6), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

fsize = 18
tsize = 22  

europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white')
fsize = 18

gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
gl.xlocator = mticker.FixedLocator([-90,3,5,7,90])
gl.ylocator = mticker.FixedLocator([0,41,42,43,60])
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'size': fsize}
gl.ylabel_style = {'size': fsize}

# ax.set_extent([0,10,38,45], crs=ccrs.PlateCarree());
ax.set_extent([2,8,40,44], crs=ccrs.PlateCarree());

ax.add_feature(europe_land_10m)

left = .07

# DC box
x, y = 3.9, 41.05
rect = Rectangle((x, y), 5.9-x, 42.4-y, fill=False, edgecolor='k', linestyle='--', transform=ccrs.PlateCarree(), label='Deep Conv.')
ax.add_patch(rect)

markersize = 3

ax.plot(argo_loc[:, 1], argo_loc[:, 0], linestyle='', marker='^', color='tab:blue', markersize=markersize, transform=ccrs.PlateCarree(), label='Argo')

ax.plot(np.array(ctd_data['loc'])[:, 1], np.array(ctd_data['loc'])[:, 0], marker='o', linestyle='', markersize=markersize+1, color='tab:red', transform=ccrs.PlateCarree(), label='CTD')

ax.plot(5, 42, transform=ccrs.PlateCarree(), marker='x', color='black', markersize=10, markeredgewidth=2, linestyle='', label='42$^\circ$N 5$^\circ$E')

ax.legend()

fig.suptitle('CTD and Argo profile locations', fontsize=tsize)
fig.tight_layout()
# fig.subplots_adjust(top=1)

fig.savefig('../plots/argo_locs.png')
