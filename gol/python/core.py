# NCAR Bulk formulas from Large and Yeger 2004, adapted from NEMO

import numpy as np

def sign(a, b):
    return a * np.sign(b)

def cd_neutral_10m(zw10):

    rgt33 = 0.5 + sign( 0.5, (zw10 - 33.) )  

    return 1.e-3 * ((1.- rgt33)*( 2.7/zw10 + 0.142 + zw10/13.09 - 3.14807E-10*zw10**6) + rgt33  * 2.34   )

def psi_h(pta):

    X2 = np.sqrt( abs( 1. - 16.*pta ) )
    X2 = max( [X2 , 1.] )
    X = np.sqrt( X2 )
    stabit = 0.5 + sign( 0.5 , pta )

    return -5.*pta*stabit + (1. - stabit)*(2.*np.log( (1. + X2)*0.5 ))

def psi_m(pta):

    X2 = np.sqrt( abs( 1. - 16.*pta ) )  ;  X2 = max( [X2 , 1.] )
    X = np.sqrt( X2 )
    stabit = 0.5 + sign( 0.5 , pta )

    return -5.*pta*stabit + (1. - stabit)*(2.*np.log((1. + X)*0.5) + np.log((1. + X2)*0.5) - 2.*np.arctan(X) + 3.14159*0.5)

def turb_core_2z( zt, zu, sst, T_zt, q_sat, q_zt, dU):

    vkarmn = 0.4
    grav = 9.81

    U_zu = max([0.5, dU])

    ztmp0 = T_zt*(1. + 0.608*q_zt) - sst*(1. + 0.608*q_sat)
    
    stab  = 0.5 + sign(0.5, ztmp0)

    ztmp0 = cd_neutral_10m( U_zu )

    sqrt_Cd_n10 = np.sqrt( ztmp0 )
    Ce_n10  = 1.e-3*( 34.6 * sqrt_Cd_n10 )
    Ch_n10  = 1.e-3*sqrt_Cd_n10*(18.*stab + 32.7*(1. - stab))

    Cd = ztmp0
    Ce = Ce_n10
    Ch = Ch_n10
    sqrt_Cd = sqrt_Cd_n10

    T_zu = T_zt
    q_zu = q_zt

    nb_itt = np.arange(5)
    for j_itt in nb_itt:

        ztmp1 = T_zu - sst
        ztmp2 = q_zu - q_sat 

        ztmp1  = Ch/sqrt_Cd*ztmp1
        ztmp2  = Ce/sqrt_Cd*ztmp2

        ztmp0 = T_zu*(1. + 0.608*q_zu)

        ztmp0 =  (vkarmn*grav/ztmp0*(ztmp1*(1.+0.608*q_zu) + 0.608*T_zu*ztmp2)) / (Cd*U_zu*U_zu) 

        zeta_u   = zu*ztmp0
        zeta_u = sign( min([abs(zeta_u),10.0]), zeta_u )
        zpsi_h_u = psi_h( zeta_u )
        zpsi_m_u = psi_m( zeta_u )

        zeta_t = zt*ztmp0 ;  zeta_t = sign( min([abs(zeta_t),10.0]), zeta_t )
        stab = np.log(zu/zt) - zpsi_h_u + psi_h(zeta_t)
        T_zu = T_zt + ztmp1/vkarmn*stab
        q_zu = q_zt + ztmp2/vkarmn*stab
        q_zu = max([0., q_zu])

        ztmp0 = max( [0.25 , U_zu/(1. + sqrt_Cd_n10/vkarmn*(np.log(zu/10.) - zpsi_m_u))] ) 
        ztmp0 = cd_neutral_10m(ztmp0)
        sqrt_Cd_n10 = np.sqrt(ztmp0)

        Ce_n10  = 1.e-3 * (34.6 * sqrt_Cd_n10)
        stab    = 0.5 + sign(0.5,zeta_u)
        Ch_n10  = 1.e-3*sqrt_Cd_n10*(18.*stab + 32.7*(1. - stab))

        ztmp1 = 1. + sqrt_Cd_n10/vkarmn*(np.log(zu/10.) - zpsi_m_u)
        Cd      = ztmp0 / ( ztmp1*ztmp1 )   
        sqrt_Cd = np.sqrt( Cd )

        ztmp0 = (np.log(zu/10.) - zpsi_h_u) / vkarmn / sqrt_Cd_n10
        ztmp2 = sqrt_Cd / sqrt_Cd_n10
        ztmp1 = 1. + Ch_n10*ztmp0               
        Ch  = Ch_n10*ztmp2 / ztmp1

        ztmp1 = 1. + Ce_n10*ztmp0               
        Ce  = Ce_n10*ztmp2 / ztmp1

    return Cd, Ch, Ce, T_zu, q_zu

def E(Ce, zqsatw, zq_zu, U):
    rhoa = 1.22
    Lv = 2.5e6
    return Lv*max( [0., rhoa*Ce*( zqsatw - zq_zu )* U] )

def Qh(Ch, zst, zt_zu, U):
    rhoa = 1.22
    cpa = 1000.5
    return cpa*rhoa*Ch*( zst - zt_zu )*U
