##############################################################################
# File to write raw and filtered wrf data to pickle
##############################################################################

from glob import glob
import matplotlib.pyplot as plt
import xarray as xr
import pickle

##############################################################################

data = {'control': {}, 'seasonal': {}, 'description': 'wind at 10m, T and q at 2m'}
top_keys = ['control', 'seasonal']
keys = ['huss', 'tas', 'uas', 'vas']
data_path = [glob('../data/wrforch_raw*as.nc') + ['../data/wrforch_raw_huss.nc'], glob('../data/wrforch_seasonal_*')]
# data_path = [glob('../data/wrforch_raw*as.nc') + ['../data/wrforch_raw_huss.nc'], glob('../data/wrforch_seasonal_butter*')]

for i, _ in enumerate(data_path):
    data_path[i].sort()

for i, top_key in enumerate(top_keys):
    for j, key in enumerate(keys):
        data[top_key][key] = xr.open_dataarray(data_path[i][j]).values

data['t'] = xr.open_dataarray(data_path[-1][-1]).Time.values

##############################################################################

with open('../data/wrf_gol.pickle', 'wb') as file:
    pickle.dump(data, file)
