import pickle
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import core

##############################################################################

def q_lw(rlds, SST):
    o = 5.67 * 10**-8
    return rlds - o * SST**4
    
def qsat(sst):
    rhoa = 1.22
    q1 = .98 * 640380
    q2 = -5107.4
    return q1/rhoa * np.exp(q2/sst)


##############################################################################

with open('../data/surf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si = pickle.load(file)

rlds = xr.open_dataset('../data/wrforch_raw_rlds.nc')
tas = xr.open_dataset('../data/wrforch_seasonal_tas.nc')
huss = xr.open_dataset('../data/wrforch_seasonal_huss.nc')
# uas = xr.open_dataset('../data/wrforch_seasonal_uas.nc')
# vas = xr.open_dataset('../data/wrforch_seasonal_vas.nc')

##############################################################################

rlds = rlds.rlds
Ta = tas.tas
qa = huss.huss
# ws = (uas.uas**2 + vas.vas**2)**.5

sst = data['seasonal']['SST'] + 273.15
qnet = data['seasonal']['NHF']
qsw = data['seasonal']['SWD']
ws = data['seasonal']['WND']

# separating

beg = np.datetime64(data['t'][0])
end = np.datetime64(data['t'][-1])

ind = (rlds.Time >= beg) & (rlds.Time <= end)

# indexing

rlds = rlds[ind].values
Ta = Ta[ind].values
qa = qa[ind].values
# ws = ws[ind].values

qlw = q_lw(rlds, sst)
qlhsh = qnet - qsw - qlw

qsh = np.zeros_like(qlw)
qlh = np.zeros_like(qlw)

for i, u in enumerate(ws):
    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sst[i], Ta[i], qsat(sst[i]), qa[i], ws[i])
    qsh[i], qlh[i] = core.Qh(Ch, sst[i], T_zu, ws[i]), core.E(Ce, qsat(sst[i]), q_zu, ws[i])

qsh = -qsh
qlh = -qlh
