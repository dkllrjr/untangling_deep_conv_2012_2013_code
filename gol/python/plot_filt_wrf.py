##############################################################################
# File to plot the filtered and unfiltered wrf data used to drive my NEMO
# deep convection simulations
##############################################################################

import pickle
import matplotlib.pyplot as plt
import numpy as np

##############################################################################

with open('../data/wrf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

##############################################################################

top_keys = ['control', 'seasonal']
keys = data[top_keys[0]].keys()

t = data['t']

fsize = 18
tsize = 22

w = {}
theta = {}
for top_key in top_keys:
    w[top_key] = ( data[top_key]['uas']**2 + data[top_key]['vas']**2 )**.5
    theta[top_key] = np.arctan2(data[top_key]['vas'], data[top_key]['uas']) * 180/np.pi

fig, ax = plt.subplots(6, 1, figsize=(14, 14), dpi=300)

fig.suptitle('WRF filtered variables', fontsize=tsize, y = .99)

colors = ['black', 'tab:blue']
labels = ['Control', 'Seasonal']
ylabels = ['$q$ $kg/kg$', '$T$ $^\circ C$', '$u$ $m/s$', '$v$ $m/s$']
alphas = [1, 1]
linestyles = ['-', '--']

for i, key in enumerate(keys):
    for j, top_key in enumerate(top_keys):
        ax[i].plot(t, data[top_key][key], color=colors[j], label=labels[j], linestyle=linestyles[j], alpha=alphas[j])

        if i == 0:
            ax[i].legend(fontsize=fsize-2)

    ax[i].set_ylabel(ylabels[i], fontsize = fsize)
    ax[i].tick_params(axis="x", labelsize=fsize)
    ax[i].tick_params(axis="y", labelsize=fsize)

for i, top_key in enumerate(top_keys):
    ax[4].plot(t, w[top_key], color=colors[i], linestyle=linestyles[i], alpha=alphas[i])
    ax[5].plot(t, theta[top_key], color=colors[i], linestyle=linestyles[i], alpha=alphas[i])

    ax[4].set_ylabel('Wind speed $m/s$', fontsize=fsize)
    ax[5].set_ylabel('Wind direction $Deg$', fontsize=fsize)

    ax[4].tick_params(axis="x", labelsize=fsize)
    ax[4].tick_params(axis="y", labelsize=fsize)
    ax[5].tick_params(axis="x", labelsize=fsize)
    ax[5].tick_params(axis="y", labelsize=fsize)

for axis in ax:
    axis.set_xlim(t[0], t[-1])

ax[-1].set_xlabel('Time', fontsize = fsize)

fig.tight_layout()
fig.savefig('../plots/wrf_filtered.png')
