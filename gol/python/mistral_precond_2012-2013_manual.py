##############################################################################

##############################################################################

import pickle
import numpy as np
import datetime

##############################################################################
# determining preconditioning mistral attributes

beg = datetime.date(2012, 8, 30)
end = datetime.date(2012, 12, 16)

events = [[datetime.date(2012, 8, 30), datetime.date(2012, 9, 6)],
        [datetime.date(2012, 9, 12), datetime.date(2012, 9, 15)],
        [datetime.date(2012, 9, 19), datetime.date(2012, 9, 20)],
        [datetime.date(2012, 9, 28), datetime.date(2012, 10, 2)],
        [datetime.date(2012, 10, 12), datetime.date(2012, 10, 15)],
        [datetime.date(2012, 10, 27), datetime.date(2012, 10, 31)],
        [datetime.date(2012, 11, 11), datetime.date(2012, 11, 13)],
        [datetime.date(2012, 11, 19), datetime.date(2012, 11, 20)],
        [datetime.date(2012, 11, 27), datetime.date(2012, 12, 2)],
        [datetime.date(2012, 12, 8), datetime.date(2012, 12, 11)],
        [datetime.date(2012, 12, 17), datetime.date(2012, 12, 19)],
        [datetime.date(2012, 12, 21), datetime.date(2012, 12, 22)],
        [datetime.date(2012, 12, 26), datetime.date(2012, 12, 30)],
        [datetime.date(2013, 1, 2), datetime.date(2013, 1, 18)],
        [datetime.date(2013, 1, 23), datetime.date(2013, 1, 28)],
        [datetime.date(2013, 2, 2), datetime.date(2013, 2, 16)]]

during_flag = []
destrat_flag = [7, 11, 12, 13, 14]

for i, _ in enumerate(events):
    if i in destrat_flag:
        during_flag.append(False)
    else:
        during_flag.append(True)

after_flag = []
# restrat_flag = [0, 1, 2, 3, 4, 6, 7, 9, 10, 11, 12, 14]
restrat_flag = [0, 1, 2, 3, 4, 6]

for i, _ in enumerate(events):
    if i in restrat_flag:
        after_flag.append(True)
    else:
        after_flag.append(False)

precond = {}
precond['events'] = []
precond['duration'] = []
precond['period'] = []

precond['destrat_flag'] = during_flag
precond['restrat_flag'] = after_flag

for i, event in enumerate(events):
    precond['events'].append(event)
    precond['duration'].append((event[-1] - event[0]).days + 1)
    
    if i < len(events)-1:
        precond['period'].append((events[i+1][0] - event[0]).days)
    else:
        precond['period'].append((datetime.date(2013, 2, 20) - event[0]).days) # taken from the mistral data

duration_ave = np.nanmean(precond['duration'])
period_ave = np.nanmean(precond['period'])
total_events = len(precond['events'])

precond['duration_ave'] = duration_ave
precond['period_ave'] = period_ave
precond['duration_std'] = np.nanstd(precond['duration'])
precond['period_std'] = np.nanstd(precond['period'])

##############################################################################
# saving data

with open('../data/precond_gol_manual.pickle', 'wb') as file:
    pickle.dump(precond, file)
