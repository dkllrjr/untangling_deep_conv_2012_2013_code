##############################################################################



##############################################################################

import xarray as xr
import numpy as np
from scipy.integrate import simps
from datetime import datetime
import pickle

##############################################################################
# functions

def datetime64todatetime(d64):
    d = np.empty_like(d64).astype(datetime)
    for i, item in enumerate(d64):
        d[i] = datetime.fromisoformat(str(item)[0:19])

    return d


##############################################################################
# loading data

N_cont = xr.open_dataset('../data/cont_gol_conv_N.nc')
N_seas = xr.open_dataset('../data/seas_gol_conv_N.nc')

##############################################################################
# calculating

d = N_cont.depthw.values

D = np.max(d[np.where(N_cont.vovaisala.values[0]>0)])
t = datetime64todatetime(N_cont.time_counter.values)

SI_cont = simps(N_cont.vovaisala*d, d)
SI_seas = simps(N_seas.vovaisala*d, d)

dSI = SI_cont - SI_seas

N_cont_ave = 2*SI_cont/D**2
N_seas_ave = 2*SI_seas/D**2

##############################################################################
# saving

cont = {'SI': SI_cont, 'N_ave': N_cont_ave}
seas = {'SI': SI_seas, 'N_ave': N_seas_ave}
data = {'D': D, 'd': d, 't': t, 'dSI': dSI, 'control': cont, 'seasonal': seas}

with open('../data/si_gol.pickle', 'wb') as file:
    pickle.dump(data, file)
