##############################################################################

##############################################################################

import pickle
import numpy as np
import datetime

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

with open('../../mistrals/data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

##############################################################################
# determining preconditioning mistral attributes

beg = datetime.date(2012, 8, 30)
# end = datetime.date(2012, 12, 11)
seas_min_t = si_data['t'][np.argmin(si_data['seasonal']['SI'])]
end = seas_min_t.date()

precond = {}
precond['events'] = []
precond['duration'] = []
precond['period'] = []

for i, event in enumerate(mistral_data['events']):
    if beg <= event[0] and event[-1] <= end:
        precond['events'].append(event)
        precond['duration'].append(mistral_data['duration'][i])
        precond['period'].append(mistral_data['period'][i])

duration_ave = np.mean(precond['duration'])
period_ave = np.mean(precond['period'])
total_events = len(precond['events'])

precond['duration_ave'] = duration_ave
precond['period_ave'] = period_ave
precond['duration_std'] = np.std(precond['duration'])
precond['period_std'] = np.std(precond['period'])

##############################################################################
# saving data

with open('../data/precond_gol.pickle', 'wb') as file:
    pickle.dump(precond, file)
