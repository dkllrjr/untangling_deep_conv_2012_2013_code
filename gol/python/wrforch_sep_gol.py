##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
# This file separates the 42 N 5 E grid point from the WRFORCH MEDCORDEX run
# to compare it to my filtered versions
#
##############################################################################

import os
import xarray as xr
from glob import glob
from dask.diagnostics import ProgressBar
import numpy as np

##############################################################################

def find_lat_lon(loc,lat,lon):

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        #    (r0**2 + r1**2 - 2*r0*r1*(np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)))**.5
        a = r0**2 + r1**2
        b = 2*r0*r1
        c = np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)
        d = (a - b*c)
        return d**.5
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            # print(i,len(lat_lon))
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])

    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))

    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind

##############################################################################
# CONTROL data

## Setting the data paths

data_path = ['/home/mach/Data/phd_temp/butter/']
save_path = ['../data/wrforch_seasonal_butter_']

# data_path = ['../../WRFORCH/RAW/','../../WRFORCH/SEASONAL/']
# save_path = ['../../GOL/wrforch_raw_','../../GOL/wrforch_seasonal_']
# there are no wrforch seasonal pr, prsn, rlds, nor rsds

data = {}
# at first was ['T', 'q', 'u', 'v']
# variables = ['pr', 'prsn', 'rlds', 'rsds']
# files = ['*PR.nc', '*PRSN.nc', '*RLDS.nc', '*RSDS.nc']
# arr = ['pr', 'prsn', 'rlds', 'rsds']

variables = ['tas', 'huss', 'uas', 'vas']
files = ['*TAS.nc', '*HUSS.nc', '*UAS.nc', '*VAS.nc']
arr = ['tas', 'huss', 'uas', 'vas']

for variable in variables:
    data[variable] = {}

for i, path in enumerate(data_path):

    print(i)
   
    for j, variable in enumerate(variables):
        data[variable]['path'] = glob(path + files[j])
        data[variable]['path'].sort()
        
    print('Collected paths')

    for j, variable in enumerate(variables):
        data[variable]['xarray'] = xr.open_mfdataset(data[variable]['path'])
        
        print('Loaded data')

        if j == 0:

            loc = [[42,5]]
            lat = data[variable]['xarray'].nav_lat_grid_M.values
            lon = data[variable]['xarray'].nav_lon_grid_M.values

            ind = find_lat_lon(loc,lat,lon)
            ind = ind[0]
            
            print('Found indices')

        data[variable]['xarray'] = data[variable]['xarray'][arr[j]][:, ind[0], ind[1]]
        save = save_path[i] + arr[j] + '.nc'
        delayed = data[variable]['xarray'].to_netcdf(save, compute=False)
        
        print(variable, save)
        with ProgressBar():
            results = delayed.compute()
    
    print('Saved data')
