import pickle
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import welch, iirfilter, sosfiltfilt, fftconvolve
from datetime import datetime, timedelta, date
import xarray as xr

##############################################################################

def intra_moving_average(x, N, res):

    ones = np.ones(N)/N

    s = x[0:len(x)//res*res]
    s = s.reshape((res, len(s)//res), order='F')

    mvs = np.zeros_like(s)

    for i, arr in enumerate(s):
        mvs[i, :] = fftconvolve(arr, ones, 'same')

    mvs = mvs.flatten(order='F')

    extra = x[len(x)//res*res::]
    mvs = np.append(mvs, extra)

    return mvs

##############################################################################
# data

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    other_mistral = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    mistral = pickle.load(file)

##############################################################################
# build time

data_path = '../data/'

T = xr.open_dataset(data_path + 'wrforch_raw_tas.nc')
t = T.Time.values
T = T.tas.values
u = xr.open_dataset(data_path + 'wrforch_raw_uas.nc').uas.values
v = xr.open_dataset(data_path + 'wrforch_raw_vas.nc').vas.values
q = xr.open_dataset(data_path + 'wrforch_raw_huss.nc').huss.values

us = xr.open_dataset(data_path + 'wrforch_seasonal_uas.nc').uas.values
vs = xr.open_dataset(data_path + 'wrforch_seasonal_vas.nc').vas.values

##############################################################################
# 

ws = (us**2 + vs**2)**.5

fs = 8/86400  # Hz
N = 2**18

f_day = 1/86400  # Hz
f_2week = 1/86400/14  # Hz
f_month = 1/86400/30.4375  # Hz
f_quarter = 1/86400/91.3125  # Hz
f_2month = f_month*.5
f_year = 1/86400/365  # Hz

order = 20

f_high = 1/86400/1.25  # Hz
f_low = 1/86400/25  # Hz

sos = iirfilter(order, f_low, fs=fs, output='sos', btype='lowpass', ftype='butter', rs=40)
ul = sosfiltfilt(sos, u)
vl = sosfiltfilt(sos, v)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandpass', ftype='butter', rs=40)
ubp = sosfiltfilt(sos, u)
vbp = sosfiltfilt(sos, v)

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)
ubs = sosfiltfilt(sos, u)
vbs = sosfiltfilt(sos, v)

S = (u**2 + v**2)**.5
sl = (ul**2 + vl**2)**.5
sbp = (ubp**2 + vbp**2)**.5
sbs = (ubs**2 + vbs**2)**.5

fS, FS = welch(S, fs=fs, nfft=N, scaling='density')
fws, Fws = welch(ws, fs=fs, nfft=N, scaling='density')
fl, Fl = welch(sl, fs=fs, nfft=N, scaling='density')
fbp, Fbp = welch(sbp, fs=fs, nfft=N, scaling='density')
fbs, Fbs = welch(sbs, fs=fs, nfft=N, scaling='density')

fsize = 16

fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=400)

ax.semilogy(fS, FS, color='k', label='Control')
ax.semilogy(fws, Fws, color='tab:red', label='Moving Average')
# ax.semilogy(fl, Fl, color='tab:green', label='Lowpass')
ax.semilogy(fbs, Fbs, color='tab:blue', label='Bandstop')
# ax.semilogy(fbp, Fbp, color='tab:red', label='Bandpass')

ax.axvline(f_day, color='tab:pink', linestyle='--', label='Daily')
ax.axvline(f_2week, color='tab:blue', linestyle='--', label='Biweekly')
ax.axvline(f_month, color='tab:green', linestyle='--', label='Monthly')

ax.set_xlim(fS[0], fS[-1]/3)

ax.set_ylabel('Power Spectral Density $\\frac{|U|^2}{Hz}$', fontsize=fsize)
ax.set_xlabel('Frequency $Hz$', fontsize=fsize)

ax.legend(fontsize=fsize-2)

ax.tick_params(axis='both', labelsize=fsize-2)

fig.suptitle('Filtered wind at 42$^\circ$E 5$^\circ$', fontsize=fsize+4)

fig.tight_layout()

fig.savefig('../plots/fourier_wind.png')
plt.close()

##############################################################################

fig, ax = plt.subplots(1, 1, figsize=(16, 5), dpi=400)

ax.plot(t, S, color='k', alpha=.4, label='Total')
ax.plot(t, sl, color='tab:green', alpha=.7, label='Lowpass')
# ax.plot(t, sbp + sl, color='tab:red', alpha=1, linestyle='-', label='Bandpass')
ax.plot(t, sbs, color='tab:blue', alpha=.7, label='Bandstop')
ax.plot(t, ws, color='tab:red', alpha=.7, label='Moving Average')


ax.set_xlabel('Date', fontsize=fsize)
ax.set_ylabel('Wind speed $m/s$', fontsize=fsize)

for axi, axis in enumerate([ax]):

    axis.set_xlim(datetime(2012, 8, 1), datetime(2013, 8, 1))
    axis.set_ylim(0, 23)

    for event in mistral['events']:
        if event == mistral['events'][0] and axi == 0:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25, label='Mistrals')
        else:
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25)

    for event in other_mistral['events']:
        if event[-1] < date(2012, 8, 30) or event[0] > date(2013, 2, 16):
            axis.axvspan(event[0], event[-1] + timedelta(days=1), color='tab:green', alpha=.25)

    axis.tick_params(axis='both', labelsize=fsize-2)

for axi, axis in enumerate([ax]):
    axis.legend(fontsize=fsize-2)

fig.suptitle('Filtered wind at 42$^\circ$E 5$^\circ$', fontsize=fsize+4)

fig.tight_layout()
fig.savefig('../plots/fourier_wind_time_series.png')
