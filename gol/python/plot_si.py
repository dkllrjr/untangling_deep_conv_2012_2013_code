##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import plot_mld as pm
import numpy as np

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################
# important time stamps

time_stamps = [datetime(2012, 8, 30, 12), datetime(2012, 12, 11, 12), datetime(2013, 2, 13, 12), datetime(2013, 6, 1, 12)]
time_stamp_labels = ['$t_A$', '$t_B$', '$t_C$', '$t_D$']

##############################################################################
# ideal events

ideal_des = np.arange(len(precond_data['destrat_flag']))[precond_data['destrat_flag']]
ideal_res = np.arange(len(precond_data['restrat_flag']))[precond_data['restrat_flag']]

# ideal_des = [0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 15]
# ideal_res = [0, 1, 2, 3, 4, 6]

##############################################################################
# plot

fig, ax = plt.subplots(3, 1, dpi=300, figsize=(16, 13))

fsize = 18
tsize = 22

print(np.max(si_data['control']['SI']))

ax[0].plot(si_data['t'], si_data['control']['SI'], color='black', label='$\delta$SI + SI$_S$')
ax[0].plot(si_data['t'], si_data['seasonal']['SI'], color='tab:blue', label='SI$_S$')

ax[1].plot(si_data['t'], si_data['dSI'], color='black')

## adding the mistral events and the key time points
for ax_i, axis in enumerate(ax):
    axis.set_xlim(si_data['t'][0], si_data['t'][-1])
    
    axis.tick_params(axis="x", labelsize=fsize)
    axis.tick_params(axis="y", labelsize=fsize)
    
    h0, h1 = -1, 3
    y_text = [0.08, 0.03, 2000]

    if ax_i == 0:
        for event in precond_data['events']:
            if event == precond_data['events'][0]:
                rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Preconditioning Mistrals')
     
            else:
                rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
            axis.add_patch(rect)

        for i, time_stamp in enumerate(time_stamps):
            axis.plot([time_stamp, time_stamp], [h0, h1], color='black', linestyle='--')
            axis.text(time_stamp + timedelta(days=2), y_text[ax_i], time_stamp_labels[i], fontsize=fsize-2)

        for event in mistral_data['events']:
            if event[0] < precond_data['events'][0][0] or event[0] > precond_data['events'][-1][-1]:
                if event == mistral_data['events'][0]:
                    rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25, label='Other Mistrals')
         
                else:
                    rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25)
                axis.add_patch(rect)
        
    else:
        for i, time_stamp in enumerate(time_stamps):
            if ax_i < 2:
                axis.plot([time_stamp, time_stamp], [h0, h1], color='black', linestyle='--')
                axis.text(time_stamp + timedelta(days=2), y_text[ax_i], time_stamp_labels[i], fontsize=fsize-2)

        for event in mistral_data['events']:
            if event[0] < precond_data['events'][0][0] or event[0] > precond_data['events'][-1][-1]:
                if event == mistral_data['events'][0]:
                    rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25)
         
                else:
                    rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25)
                axis.add_patch(rect)

for event_i, event in enumerate(precond_data['events']):
    if event_i in ideal_des:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, hatch='..', label='Ideal destratification')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', hatch='..', alpha=.25)
    else:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Ideal destratification')
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
    ax[1].add_patch(rect)
    
    if event_i in ideal_res:
        if event == precond_data['events'][0]:
            rect = Rectangle((event[-1] + timedelta(days=1), h0), timedelta(days=(precond_data['period'][event_i] - precond_data['duration'][event_i])), h1, color='tab:blue', alpha=.25, label='Ideal restratification')
        else:
            rect = Rectangle((event[-1] + timedelta(days=1), h0), timedelta(days=(precond_data['period'][event_i] - precond_data['duration'][event_i])), h1, color='tab:blue', alpha=.25)
        ax[1].add_patch(rect)

ax[0].set_ylabel('SI $m^2/s^2$', fontsize=fsize)
ax[0].set_ylim(0, 2)
ax[0].legend(ncol=4, fontsize=fsize-2)

# ax[1].set_xlabel('Time', fontsize=fsize)
ax[1].set_ylabel('$\delta$SI $m^2/s^2$', fontsize=fsize)
ax[1].set_ylim(-.7, .1)

# for butter
# ax[1].set_ylim(-.3, .1)

ax[1].legend(ncol=4, fontsize=fsize-2, loc='lower right')

ax[2].axhline(max(pm.mld_data['control']['MLD']), color='red', label='Sea bottom')

ax[2].plot(pm.mld_data['t'], pm.mld_data['control']['MLD'], color='black', linestyle=':', label='Control$_{\\Delta \\rho}$')
ax[2].plot(pm.mld_data['t'], pm.mld_data['seasonal']['MLD'], color='tab:blue', linestyle=':', label='Seasonal$_{\\Delta \\rho}$')

ax[2].plot(pm.mld_data['t'], pm.mld_data['control']['MLHGT'], color='black', label='Control$_{K_z}$')
ax[2].plot(pm.mld_data['t'], pm.mld_data['seasonal']['MLHGT'], color='tab:blue', label='Seasonal$_{K_z}$')

# adding the mistral events and the key time points
ax[2].set_xlim(pm.mld_data['t'][0], pm.mld_data['t'][-1])

ax[2].tick_params(axis="x", labelsize=fsize)
ax[2].tick_params(axis="y", labelsize=fsize)

h0, h1 = -25, max(pm.mld_data['control']['MLD'])*1.1
y_text = h1 - 50

for event in pm.precond_data['events']:
    if event == precond_data['events'][0]:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:green', alpha=.25)
        # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:green', alpha=.25, label='Preconditioning Mistrals')

    else:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:green', alpha=.25)
    ax[2].add_patch(rect)

for i, time_stamp in enumerate(pm.time_stamps):
    ax[2].plot([time_stamp, time_stamp], [h0, h1], color='black', linestyle='--')
    ax[2].text(time_stamp + timedelta(days=2), y_text, pm.time_stamp_labels[i], fontsize=fsize-2)

for event in pm.mistral_data['events']:
    if event[0] < pm.precond_data['events'][0][0] or event[0] > pm.precond_data['events'][-1][-1]:
        if event == pm.mistral_data['events'][0]:
            # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:red', alpha=.25, label='Other Mistrals')
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:red', alpha=.25)
 
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:red', alpha=.25)
        ax[2].add_patch(rect)


ax[2].set_ylabel('MLD $m$', fontsize=fsize)
ax[2].set_xlabel('Time', fontsize=fsize)
ax[2].set_ylim([h0, h1])
ax[2].invert_yaxis()
ax[2].legend(ncol=2, fontsize=fsize-2, loc='upper right')

fig.suptitle('NW Mediterranean 42$^\circ$N 5$^\circ$E', fontsize=tsize)

fig.text(0.015, 0.96, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(0.015, 0.64, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(0.015, 0.33, '(c)', fontweight='bold', fontsize=fsize+2)

fig.tight_layout()
fig.savefig('../plots/si_gol.png')
# plt.show()
