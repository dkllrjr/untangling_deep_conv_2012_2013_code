##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import xarray as xr

##############################################################################
# load data

with open('../data/surf_gol.pickle', 'rb') as file:
    mld_data = pickle.load(file)

with open('../data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol_manual.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################
# important time stamps

time_stamps = [datetime(2012, 8, 30, 12), datetime(2012, 12, 11, 12), datetime(2013, 2, 13, 12), datetime(2013, 6, 1, 12)]
time_stamp_labels = ['$t_A$', '$t_B$', '$t_C$', '$t_D$']

##############################################################################
# ideal events

ideal_des = [0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 15]
ideal_res = [0, 1, 2, 3, 4, 6]

##############################################################################
# plot

fig, ax = plt.subplots(1, 1, dpi=200, figsize=(16, 6))

fsize = 18
tsize = 22

ax.axhline(max(mld_data['control']['MLD']), color='red', label='Sea Bottom')

ax.plot(mld_data['t'], mld_data['control']['MLD'], color='black', linestyle=':', label='Control$_{\\Delta \\rho}$')
ax.plot(mld_data['t'], mld_data['seasonal']['MLD'], color='tab:blue', linestyle=':', label='Seasonal$_{\\Delta \\rho}$')

ax.plot(mld_data['t'], mld_data['control']['MLHGT'], color='black', label='Control$_{K_z}$')
ax.plot(mld_data['t'], mld_data['seasonal']['MLHGT'], color='tab:blue', label='Seasonal$_{K_z}$')

# adding the mistral events and the key time points
ax.set_xlim(mld_data['t'][0], mld_data['t'][-1])

ax.tick_params(axis="x", labelsize=fsize)
ax.tick_params(axis="y", labelsize=fsize)

h0, h1 = -25, max(mld_data['control']['MLD'])*1.1
y_text = h1 - 50

for event in precond_data['events']:
    if event == precond_data['events'][0]:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:green', alpha=.25, label='Preconditioning Mistrals')

    else:
        rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:green', alpha=.25)
    ax.add_patch(rect)

for i, time_stamp in enumerate(time_stamps):
    ax.plot([time_stamp, time_stamp], [h0, h1], color='black', linestyle='--')
    ax.text(time_stamp + timedelta(days=2), y_text, time_stamp_labels[i], fontsize=fsize-4)

for event in mistral_data['events']:
    if event[0] < precond_data['events'][0][0] or event[0] > precond_data['events'][-1][-1]:
        if event == mistral_data['events'][0]:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:red', alpha=.25, label='Other Mistrals')
 
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1 + 50, color='tab:red', alpha=.25)
        ax.add_patch(rect)


ax.set_ylabel('Mixed Layer Depth $m$', fontsize=fsize)
ax.set_xlabel('Time', fontsize=fsize)
ax.set_ylim([h0, h1])
ax.invert_yaxis()
ax.legend(ncol=1, fontsize=fsize-4, loc='upper right')


fig.suptitle('NW Mediterranean 42$^\circ$N 5$^\circ$E', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/mld_gol.png')
