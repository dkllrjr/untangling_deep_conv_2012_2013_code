import matplotlib.pyplot as plt
import pickle
import numpy as np

##############################################################################

def moving_mean(f,N):
    #Window = 2*N + 1
    mm = np.zeros(f.size)
    
    for i in range(f.size):
        if i < N:
            m = []
            for j in range(i+N+1):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        elif i+N > f.size-1:
            m = []
            for j in range(i-N,f.size):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        else:
            mm[i] = np.nanmean(f[i-N:i+N+1][np.where(np.isfinite(f[i-N:i+N+1]))[0]])
        
    return mm


##############################################################################

with open('../data/surf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si = pickle.load(file)

##############################################################################

data_keys = list(data.keys())[1::]
subdata_keys = list(data['control'].keys())

titles = ['Temperature', 'Salinity', 'Short Wave Rad. Downwelling', 'Net Downward Heat Flux']

t = data['t']

fig, ax = plt.subplots(4, 1, figsize=(10, 9), dpi=250)

for i, axis in enumerate(ax):
    for key in data_keys:
        axis.plot(t, data[key][subdata_keys[i]], label=key, alpha=.5)
        axis.set_title(titles[i])
        axis.set_ylabel('$W/m^2$')
        axis.set_xlim(t[0], t[-1])
        axis.legend()

ax[-1].set_xlabel('Time')

fig.suptitle('Sea Surface Values')

fig.tight_layout()
fig.savefig('../plots/surf_flux_gol.png')
plt.close()

##############################################################################
# averaged plot

N = int(24/3 * 30)

fig, ax = plt.subplots(4, 1, figsize=(10, 9), dpi=250)

twinax = []
for axis in ax:
    twinax.append(axis.twinx())

for i, axis in enumerate(ax):
    for key in data_keys:
        axis.plot(t, moving_mean(data[key][subdata_keys[i]], N), label=key, alpha=.75)
        axis.set_title(titles[i])
        axis.set_ylabel('$W/m^2$')
        axis.set_xlim(t[0], t[-1])
        axis.legend()

    twinax[i].plot(si['t'], si['seasonal']['SI'], color='black', alpha=.5, linestyle='--')
    twinax[i].set_ylabel('SI$_S$ $m^2/s^2$')

ax[3].plot([t[0], t[-1]], [0, 0], alpha=.5, color='black')
ax[-1].set_xlabel('Time')

fig.suptitle('Sea Surface Values')

fig.tight_layout()
fig.savefig('../plots/surf_flux_gol_ave.png')
plt.close()
