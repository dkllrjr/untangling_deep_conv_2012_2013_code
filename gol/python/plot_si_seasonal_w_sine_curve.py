##############################################################################
# By Doug Keller
# 
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from scipy.optimize import curve_fit
import csv

##############################################################################
# sin curve fit

# def SI_S_func(t, SImax, SImin, phi):
    
    # w = 2*np.pi/365.25
    # SI_S = (SImax + SImin)/2 + (SImax - SImin)/2*np.cos(w*(t + phi))

    # return SI_S

def SI_S_func(t, phi):
    
    SImax = 1.88
    SImin = .43
    w = 2*np.pi/365.25
    SI_S = (SImax + SImin)/2 + (SImax - SImin)/2*np.cos(w*(t + phi))

    return SI_S


##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################
# curve fitting

# ref_date = datetime(2013, 1, 1, 12)
# t = [(date - ref_date).days for date in si_data['t']]
# param, _ = curve_fit(SI_S_func, t, si_data['seasonal']['SI'], [1.5, .5, 80])

# SI_S = SI_S_func(t, param[0], param[1], param[2])

ref_date = datetime(2013, 1, 1, 12)
t = [(date - ref_date).days for date in si_data['t']]
param, _ = curve_fit(SI_S_func, t, si_data['seasonal']['SI'], [80])

SI_S = SI_S_func(t, param[0])

##############################################################################
# plot

fig, ax = plt.subplots(1, 1, dpi=150, figsize=(10, 3))

ax.plot(si_data['t'], si_data['seasonal']['SI'], color='black', label='SI$_S$')
ax.plot(si_data['t'], SI_S, color='tab:blue', label='SI$_{S,fit}$', linestyle='--')

ax.set_ylabel('SI $\\frac{m^2}{s^2}$')
ax.set_ylim(0, 2)
ax.legend()
ax.set_xlim(si_data['t'][0], si_data['t'][-1])

fig.tight_layout()
fig.savefig('../plots/si_seasonal_w_sine_curve.png')

##############################################################################
# saving curve fit params

with open('../data/si_seasonal_sine_curve_fit.csv', 'w') as file:
    csvwriter = csv.writer(file, delimiter=',')
    
    csvwriter.writerow(['SImax (m^2/s^2)', 'SImin (m^2/s^2)', 'phi (days)'])
    csvwriter.writerow(param)

