import pickle
import numpy as np
import xarray as xr
import latent_heat
from scipy.io import savemat

##############################################################################

def q_lw(rlds, SST):
    SST = SST + 273.15  
    o = 5.67 * 10**-8
    return rlds - o * SST**4

##############################################################################

with open('../data/surf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si = pickle.load(file)

SST = data['seasonal']['SST']

rlds = xr.open_dataset('../data/wrforch_raw_rlds.nc')
rlds = rlds.rlds

beg = np.datetime64(data['t'][0])
end = np.datetime64(data['t'][-1])

ind = (rlds.Time >= beg) & (rlds.Time <= end)
rlds = rlds[ind].values

qlw = q_lw(rlds, SST)
qsw = data['seasonal']['SWD']
qsh = latent_heat.qsh
qlh = latent_heat.qlh
# qt = np.arange(0, 365, 1/8)
qnet = data['seasonal']['NHF']

qt = data['t']
t = []

for i, qti in enumerate(qt):
    t.append(qti.isoformat())


##############################################################################

q_dict = {'qlw': qlw, 'qsw': qsw, 'qsh':qsh, 'qlh':qlh, 't': t, 'notes': '2012 to 2013 year; data is calculated for the grid point nearest to 42 N 5 E averaged with the immediately adjacent grid points, so 9 total grid points were averaged with 42 N 5 E by the center point'}

savemat('../data/seasonal_surface_flux_components_42_5_2012-2013.mat', q_dict)
