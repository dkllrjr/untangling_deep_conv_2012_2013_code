##############################################################################
# File to plot the filtered and unfiltered wrf data used to drive my NEMO
# deep convection simulations
##############################################################################

import pickle
import matplotlib.pyplot as plt
import numpy as np

##############################################################################

with open('../data/wrf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

##############################################################################

top_keys = ['control', 'seasonal']
keys = ['huss', 'tas']

t = data['t']

w = {}
theta = {}
for top_key in top_keys:
    w[top_key] = ( data[top_key]['uas']**2 + data[top_key]['vas']**2 )**.5
    theta[top_key] = np.arctan2(data[top_key]['vas'], data[top_key]['uas']) * 180/np.pi

fig, ax = plt.subplots(3, 1, figsize=(14, 6), dpi=200)

tsize = 22
fig.suptitle('WRF Filtered Variables', fontsize = tsize, y = .975)

colors = ['black', 'tab:blue']
labels = ['Control', 'Filtered']
ylabels = ['$q$ $kg/kg$', '$T$ $^\circ C$', 'Wind Speed $m/s$']
alphas = [1, 1]
linestyles = ['-', '--']
fsize = 18

for i, key in enumerate(keys):
    for j, top_key in enumerate(top_keys):
        ax[i].plot(t, data[top_key][key], color=colors[j], label=labels[j], linestyle=linestyles[j], alpha=alphas[j])
        ax[i].tick_params(axis="x", labelsize=fsize)
        ax[i].tick_params(axis="y", labelsize=fsize)
        
        if i == 0:
            ax[i].legend(fontsize=fsize-4)

    ax[i].set_ylabel(ylabels[i], fontsize=fsize)

for i, top_key in enumerate(top_keys):
    ax[2].plot(t, w[top_key], color=colors[i], linestyle=linestyles[i], alpha=alphas[i])
    ax[2].set_ylabel('Wind Speed $m/s$', fontsize=fsize)
    ax[2].tick_params(axis="x", labelsize=fsize)
    ax[2].tick_params(axis="y", labelsize=fsize)

for axis in ax:
    axis.set_xlim(t[0], t[-1])

ax[-1].set_xlabel('Time', fontsize=fsize)

fig.tight_layout()
fig.savefig('../plots/wrf_filtered_poster.png')
