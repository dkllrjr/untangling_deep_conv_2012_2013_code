##############################################################################



##############################################################################

import xarray as xr
from datetime import datetime
import pickle
import numpy as np

##############################################################################

def datetime64todatetime(d64):
    d = np.empty_like(d64).astype(datetime)
    for i, item in enumerate(d64):
        d[i] = datetime.fromisoformat(str(item)[0:19])

    return d


##############################################################################
# loading data date#

SF_cont = xr.open_dataset('../data/cont_gol_conv_grid.nc')
SF_seas = xr.open_dataset('../data/seas_gol_conv_grid.nc')

##############################################################################
# loading variables

t = datetime64todatetime(SF_cont.time_counter.values)

cont = {}

cont['SST'] = SF_cont.sosstsst.values  # sea surface temperature
cont['SSS'] = SF_cont.sosaline.values  # sea surface salinity
cont['SWD'] = SF_cont.soshfldo.values  # sea surface net downward shortwave
cont['NHF'] = SF_cont.sohefldo.values  # sea surface net downward heatflux 
cont['MLD'] = SF_cont.somxl010.values  # mixed layer depth with sigma theta
cont['MLHGT'] = SF_cont.somixhgt.values  # mixed layer depth with turbocline depth
cont['WND'] = SF_cont.sowindsp.values  # wind speed

seas = {}

seas['SST'] = SF_seas.sosstsst.values
seas['SSS'] = SF_seas.sosaline.values
seas['SWD'] = SF_seas.soshfldo.values
seas['NHF'] = SF_seas.sohefldo.values
seas['MLD'] = SF_seas.somxl010.values
seas['MLHGT'] = SF_seas.somixhgt.values
seas['WND'] = SF_seas.sowindsp.values

##############################################################################

data = {'t': t, 'control': cont, 'seasonal': seas}

with open('../data/surf_gol.pickle', 'wb') as file:
    pickle.dump(data, file)
