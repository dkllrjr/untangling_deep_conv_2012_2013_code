import xarray as xr
from scipy.signal import welch, sosfilt, butter, freqs, iirfilter, freqz, sosfiltfilt, sosfreqz
import matplotlib.pyplot as plt
import numpy as np
import time
import pickle

##############################################################################
# functions

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

##############################################################################
# data

data_path = '../data/'

T = xr.open_dataset(data_path + 'wrforch_raw_tas.nc')
t = T.Time.values
T = T.tas.values
u = xr.open_dataset(data_path + 'wrforch_raw_uas.nc').uas.values
v = xr.open_dataset(data_path + 'wrforch_raw_vas.nc').vas.values
q = xr.open_dataset(data_path + 'wrforch_raw_huss.nc').huss.values

with open(data_path + 'mistral_attributes.pickle', 'rb') as file:
    mistral = pickle.load(file)

# Ts = xr.open_dataset(data_path + 'wrforch_seasonal_tas.nc').tas.values
# us = xr.open_dataset(data_path + 'wrforch_seasonal_uas.nc').uas.values
# vs = xr.open_dataset(data_path + 'wrforch_seasonal_vas.nc').vas.values
# qs = xr.open_dataset(data_path + 'wrforch_seasonal_huss.nc').huss.values

##############################################################################
# butterworth bandpass

f_mistral = 1/86400/np.array(mistral['period'])

f_day = 1/86400  # Hz
f_2week = 1/86400/14  # Hz
f_month = 1/86400/31  # Hz
f_year = 1/86400/365  # Hz

order = 20

fs = 8/86400  # Hz; sampling frequency of the variables

f_high = 1/86400/1.25  # Hz
# f_high = 
# f_low = 1/86400/21  # Hz
f_low = 1/86400/25  # Hz

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)
w, h = sosfreqz(sos, fs=fs)

fsize = 16
tsize = 22

fig, ax = plt.subplots(1, 1, figsize=(8, 5), dpi=400)

ax.semilogx(w, 20*np.log10(np.abs(h)), color='k')

ax.set_title('Butterworth filter frequency response', fontsize=tsize)
ax.set_xlabel('Frequency $Hz$', fontsize=fsize)
ax.set_ylabel('Amplitude $dB$', fontsize=fsize)

ax.axvline(f_low, color='tab:blue', alpha=.5)  # cutoff frequency
ax.axvline(f_high, color='tab:blue', alpha=.5)  # cutoff frequency

ax.axvline(f_day, color='green', linestyle='--', alpha=.75)  # cutoff frequency
ax.axvline(f_month, color='green', linestyle='--', alpha=.75)  # cutoff frequency

ax.text(f_month + 4e-8, -165, 'Monthly')
ax.text(f_day + 1e-6, -165, 'Daily')

ax.grid(which='both', axis='both', alpha=.5)

# fig.tight_layout()

fig.savefig('../plots/butterworth_freq_resp.png')

##############################################################################
# time series

# varss = [T, q, u, v]
varss = [T, q]
units = ['T', 'q', 'u', 'v']

sos = iirfilter(order, [f_low, f_high], fs=fs, output='sos', btype='bandstop', ftype='butter', rs=40)

fig, ax = plt.subplots(len(varss)+ 1, 1, figsize=(16, 12), dpi=400)

for i, var in enumerate(varss):

    start = time.time()
    varb = sosfiltfilt(sos, var)
    end = time.time()
    print(end-start)

    ax[i].plot(t, var, color='k', alpha=.9, label='Raw')
    # ax.plot(vars, color='tab:red', alpha=.5, label='Moving Average')
    ax[i].plot(t, varb, color='tab:blue', alpha=.9, label='Butterworth')

    ax[i].set_xlim(t[0], t[-1])

w = (u**2 + v**2)**.5
wb = sosfiltfilt(sos, w)

ax[-1].plot(t, w, color='k', alpha=.9, label='Raw')
ax[-1].plot(t, wb, color='tab:blue', alpha=.9, label='Butterworth')
ax[-1].set_xlim(t[0], t[-1])

ax[-1].legend()
ax[-1].set_xlabel('Date', fontsize=fsize)

fig.tight_layout()

fig.savefig('../plots/time_series_butterworth.png')

##############################################################################
# fourier

fig, ax = plt.subplots(len(varss), 1, figsize=(16, 8), dpi=400)

ax = ax.flatten()

for i, var in enumerate(varss):

    N = 2**18
    varb = sosfiltfilt(sos, var)
    fvar, Fvar = welch(var, fs=fs, nfft=N, scaling='density')
    # fTs, FTs = welch(Ts, fs=fs, nfft=N, scaling='density')
    fvarb, Fvarb = welch(varb, fs=fs, nfft=N, scaling='density')

    ax[i].semilogy(fvar, Fvar, color='k')
    # ax.semilogy(fTs, FTs, color='tab:blue')
    ax[i].semilogy(fvarb, Fvarb, color='tab:red')

    ax[i].axvline(f_day, color='tab:red', label='day')
    ax[i].axvline(f_2week, color='tab:green', label='2 weeks')
    ax[i].axvline(f_month, color='tab:purple', label='month')
    ax[i].axvline(f_year, color='tab:pink', label='year')
    ax[i].axvline(f_low, color='tab:blue', label='low')
    ax[i].axvline(f_high, color='tab:blue', label='high')
    ax[i].axvline(f_mistral.mean(), linestyle='--', color='tab:blue', label='mean')
    ax[i].axvline(f_mistral.min(), linestyle='--', color='tab:blue', label='min')
    ax[i].axvline(f_mistral.max(), linestyle='--', color='tab:blue', label='max')

    ax[i].set_xlim(0, 1.5*f_day)

    ax[i].set_ylabel('Power Spectrum $' + units[i] + '^2$', fontsize=fsize)
    ax[i].set_xlabel('Frequency $Hz$', fontsize=fsize)

    ax[i].legend()

fig.tight_layout()

fig.savefig('../plots/fourier_gol.png')

##############################################################################
# plot of transfer function

# L=31*8  # L-point filter
# b = (np.ones(L))/L  # numerator co-effs of filter transfer function
# a = np.ones(1)  # denominator co-effs of filter transfer function

# w, h = freqz(b,a, fs=fs)
# s = 20 * np.log10(np.abs(h))
# plt.plot(w, s)
# plt.ylabel('Magnitude [dB]')
# plt.xlabel('Frequency [Hz/sample]')
# plt.show()

# plt.subplot(2, 1, 2)
# angles = np.unwrap(np.angle(h))
# plt.plot(w, angles)
# plt.ylabel('Angle (radians)')
# plt.xlabel('Frequency [rad/sample]')
# plt.show()
