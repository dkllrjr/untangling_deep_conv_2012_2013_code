import matplotlib.pyplot as plt
import pickle
import numpy as np
import xarray as xr
import latent_heat

##############################################################################

def moving_mean(f,N):
    #Window = 2*N + 1
    mm = np.zeros(f.size)
    
    for i in range(f.size):
        if i < N:
            m = []
            for j in range(i+N+1):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        elif i+N > f.size-1:
            m = []
            for j in range(i-N,f.size):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        else:
            mm[i] = np.nanmean(f[i-N:i+N+1][np.where(np.isfinite(f[i-N:i+N+1]))[0]])
        
    return mm

def q_lw(rlds, SST):
    SST = SST + 273.15  
    o = 5.67 * 10**-8
    return rlds - o * SST**4

##############################################################################

with open('../data/surf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si = pickle.load(file)

SST = data['seasonal']['SST']

rlds = xr.open_dataset('../data/wrforch_raw_rlds.nc')
rlds = rlds.rlds

beg = np.datetime64(data['t'][0])
end = np.datetime64(data['t'][-1])

ind = (rlds.Time >= beg) & (rlds.Time <= end)
rlds = rlds[ind].values

qlw = q_lw(rlds, SST)
qshlh = data['seasonal']['NHF'] - data['seasonal']['SWD'] - qlw

##############################################################################

data_keys = list(data.keys())[1::]
subdata_keys = list(data['control'].keys())

titles = ['Temperature', 'Salinity', 'Short Wave Rad. Downwelling', 'Net Downward Heat Flux']

t = data['t']

fig, ax = plt.subplots(3, 1, figsize=(10, 9), dpi=250)

# for i, axis in enumerate(ax):
    # for key in data_keys:
        # axis.set_title(titles[i])
        # axis.set_ylabel('$W/m^2$')
        # axis.set_xlim(t[0], t[-1])
        # axis.legend()

# ax[-1].set_xlabel('Time')

N = int(24/3 * 15)
# N = int(24/3 * 7)
# N = 11
fsize = 18
tsize = 24

fig, ax = plt.subplots(2, 1, figsize=(16, 9), dpi=300)

twinax0 = []
twinax1 = []
for axis in ax:
    twinax0.append(axis.twinx())
    twinax1.append(axis.twinx())

for twinax in twinax0:
    twinax.spines.right.set_position(("axes", 1.09))

# # temperature, salinity

# scalar_keys = ['SST', 'SSS']
# scalar_labels = ['SST', 'Salinity']
scalar_colors = ['tab:blue', 'tab:red']
# scalar_units = ['$^\circ$C', 'PSU']

# twinax0[0].plot(t, data['seasonal'][scalar_keys[0]], color=scalar_colors[0], alpha=.25)
# p1, = twinax0[0].plot(t, moving_mean(data['seasonal'][scalar_keys[0]], N), label=scalar_labels[0], color=scalar_colors[0])

# twinax1[0].plot(t, data['seasonal'][scalar_keys[1]], color=scalar_colors[1], alpha=.25)
# p2, = twinax1[0].plot(t, moving_mean(data['seasonal'][scalar_keys[1]], N), label=scalar_labels[1], color=scalar_colors[1])

# twinax0[0].set_ylabel(scalar_units[0], fontsize=fsize)
# twinax1[0].set_ylabel(scalar_units[1], fontsize=fsize)

# twinax0[0].yaxis.label.set_color(p1.get_color())
# twinax1[0].yaxis.label.set_color(p2.get_color())

# twinax0[0].tick_params(axis='y', colors=p1.get_color(), labelsize=fsize-4)
# twinax1[0].tick_params(axis='y', colors=p2.get_color(), labelsize=fsize-4)

flux_keys = ['NHF']
flux_labels = ['$\dot{Q}_{net}$']

p3, = twinax0[0].plot(t, moving_mean(data['seasonal'][flux_keys[0]], N), label=flux_labels[0], color=scalar_colors[0])


twinax0[0].set_ylabel('$W/m^2$', fontsize=fsize)

twinax0[0].yaxis.label.set_color(p3.get_color())

twinax0[0].tick_params(axis='y', colors=p3.get_color(), labelsize=fsize-4)

twinax0[0].set_ylim(-300, 200)

twinax0[0].plot([t[0], t[-1]], [0, 0], alpha=1, color=scalar_colors[0], linestyle=':')

for axis in [ax[0]]:
    p4, = axis.plot(si['t'], si['seasonal']['SI'], color='black', alpha=.5, linestyle='--', label='SI$_S$')
    axis.set_ylabel('$m^2/s^2$', fontsize=fsize)
    axis.set_xlim(t[0], t[-1])

dsidt = np.gradient(si['seasonal']['SI'])/86400
p5, = twinax1[0].plot(si['t'], dsidt*10**9, color='tab:green', linestyle='--', label='$\\frac{\\partial SI_S}{\\partial t}$')
twinax1[0].tick_params(axis='y', labelsize=fsize-2)
twinax1[0].set_ylabel('$m^2/s^3$ $\\times 10^{9}$ $m^4/Js^2$ ($W/m^2$)', fontsize=fsize)
twinax1[0].yaxis.offsetText.set_visible(False)
twinax1[0].tick_params(axis='y', colors=p5.get_color(), labelsize=fsize-2)
twinax1[0].yaxis.label.set_color(p5.get_color())
twinax1[0].set_ylim(-300, 200)

# ax[0].legend(handles=[p1, p2, p4], loc='lower right')
ax[0].legend(ncol=2, handles=[p3, p4, p5], loc='lower right', fontsize=fsize-2)

mswd = moving_mean(data['seasonal']['SWD'], N)
mqnet = moving_mean(data['seasonal']['NHF'], N)
mqshlh = moving_mean(qshlh, N)
mqlw = moving_mean(qlw, N)
mqsh = moving_mean(latent_heat.qsh, N)
mqlh = moving_mean(latent_heat.qlh, N)

# mswd = data['seasonal']['SWD']
# mqnet = data['seasonal']['NHF']
# mqshlh = qshlh
# mqlw = qlw
# mqsh = latent_heat.qsh
# mqlh = latent_heat.qlh

p6, = ax[1].plot(t, mswd, color='black', label='$\dot{Q}_{SW}$')
p7, = twinax0[1].plot(t, mqnet, color='tab:blue', label='$\dot{Q}_{net}$')
p8, = twinax1[1].plot(t, mqsh, color='tab:red', linestyle=':', label='$\dot{Q}_H$')
p9, = twinax1[1].plot(t, mqlh, color='tab:red', label='$\dot{Q}_E$')
p10, = twinax1[1].plot(t, mqlw, linestyle='--', color='tab:red', label='$\dot{Q}_{LW}$')

ax[1].set_ylabel('$W/m^2$', fontsize=fsize)
twinax0[1].set_ylabel('$W/m^2$', fontsize=fsize)
twinax1[1].set_ylabel('$W/m^2$', fontsize=fsize)

twinax0[1].yaxis.label.set_color(p7.get_color())
twinax1[1].yaxis.label.set_color(p8.get_color())

twinax0[1].tick_params(axis='y', colors=p7.get_color(), labelsize=fsize-2)
twinax1[1].tick_params(axis='y', colors=p8.get_color(), labelsize=fsize-2)

ax[1].set_xlim(t[0], t[-1])
ax[1].set_ylim(0, 500)
twinax0[1].set_ylim(-300, 200)
twinax1[1].set_ylim(-500, 0)

twinax0[1].plot([t[0], t[-1]], [0, 0], alpha=1, color=scalar_colors[0], linestyle=':')

ax[1].legend(ncol=2, handles=[p6, p7, p8, p9, p10], loc='lower right', fontsize=fsize-2)

ax[-1].set_xlabel('Time', fontsize=fsize)

for axis in ax:
    axis.tick_params(axis='y', labelsize=fsize-2)
    axis.tick_params(axis='x', labelsize=fsize-2)

text_x = 0.008
fig.text(text_x, .93, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(text_x, .5, '(b)', fontweight='bold', fontsize=fsize+2)
# fig.text(text_x, .34, 'c)', fontsize=fsize)

fig.suptitle('Seasonal sea surface fluxes', fontsize=tsize)

fig.tight_layout()
fig.savefig('../plots/surf_flux_gol_combined.png')
plt.close()

##############################################################################

qt = np.arange(0, 365, 1/8)
qnet = data['seasonal']['NHF']
sqnet = np.trapz(qnet, qt)
ssqnet = sqnet * 365

sit = np.arange(0, 365, 1)
sis = si['seasonal']['SI']
ssi = np.trapz(sis, sit)
