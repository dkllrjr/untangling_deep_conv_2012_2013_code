import xarray as xr
from scipy.signal import welch, sosfilt, butter
import matplotlib.pyplot as plt
import numpy as np

##############################################################################
# functions

def moving_mean(f,N):
    #Window = 2*N + 1
    mm = np.zeros(f.size)
    
    for i in range(f.size):
        if i < N:
            m = []
            for j in range(i+N+1):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        elif i+N > f.size-1:
            m = []
            for j in range(i-N,f.size):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        else:
            mm[i] = np.nanmean(f[i-N:i+N+1][np.where(np.isfinite(f[i-N:i+N+1]))[0]])
        
    return mm

##############################################################################
# data

data_path = '../data/'

T = xr.open_dataset(data_path + 'wrforch_raw_tas.nc').tas.values
u = xr.open_dataset(data_path + 'wrforch_raw_uas.nc').uas.values
v = xr.open_dataset(data_path + 'wrforch_raw_vas.nc').vas.values
q = xr.open_dataset(data_path + 'wrforch_raw_huss.nc').huss.values

Ts = xr.open_dataset(data_path + 'wrforch_seasonal_tas.nc').tas.values
us = xr.open_dataset(data_path + 'wrforch_seasonal_uas.nc').uas.values
vs = xr.open_dataset(data_path + 'wrforch_seasonal_vas.nc').vas.values
qs = xr.open_dataset(data_path + 'wrforch_seasonal_huss.nc').huss.values

##############################################################################
# filtering

# sos = butter(
Tmm = moving_mean(T, 15*8)

# plt.plot(T)
# plt.plot(Tmm)
# plt.plot(Ts)

##############################################################################
# fourier analysis

f_day = 1/86400  # Hz
f_2week = 1/86400/14  # Hz
f_month = 1/86400/31  # Hz
f_year = 1/86400/365  # Hz

fs = 1/(3*3600)  # Hz
N = 2**18
fT, FT = welch(T, fs=fs, nfft=N, scaling='density')
fTs, FTs = welch(Ts, fs=fs, nfft=N, scaling='density')

# fT /= fs
# fTs /= fs

FT /= np.max(FT)
FTs /= np.max(FTs)

plt.semilogy(fT, FT, color='k')
plt.semilogy(fTs, FTs, color='tab:blue')

# plt.plot(fT, FT, color='k')
# plt.plot(fTs, FTs, color='tab:blue')

plt.axvline(f_day, color='tab:red', label='day')
plt.axvline(f_2week, color='tab:green', label='2 weeks')
plt.axvline(f_month, color='tab:purple', label='month')
plt.axvline(f_year, color='tab:pink', label='year')

plt.legend()

plt.show()
