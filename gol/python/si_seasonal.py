##############################################################################

##############################################################################

import pickle
import numpy as np
import datetime

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

with open('../../mistrals/data/mistral_attributes_2012-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/precond_gol.pickle', 'rb') as file:
    precond_data = pickle.load(file)

##############################################################################
# calculations

deep_conv_t = si_data['t'][np.where(si_data['control']['SI']<=0)][0]
seas_min_t = si_data['t'][np.argmin(si_data['seasonal']['SI'])]

e = deep_conv_t - seas_min_t

for i, date in enumerate(si_data['t']):
    if date.date() == precond_data['events'][-1][-1]:
        ind_seas_min = i

dSI_seas_min = si_data['dSI'][ind_seas_min]

for i, date in enumerate(si_data['t']):
    if date.date() == precond_data['events'][0][-1]:
        ind_first_mistral = i

dSI_first_mistral = si_data['dSI'][ind_first_mistral]

si_dates = []
for date in si_data['t']:
    si_dates.append(date.date())

dF = []
for i, event in enumerate(precond_data['events']):
    dF.append(si_data['dSI'][np.where(si_data['t']==event[0])])
