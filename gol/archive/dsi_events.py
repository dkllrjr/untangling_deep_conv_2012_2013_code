import pickle
import numpy as np
import datetime
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

##############################################################################
# functions

def super_sample(f, x):
    
    x_ss = []
    dx = np.diff(x)
    for i in range(x.size-1):
        x_ss.append(x[i])
        x_ss.append(x[i]+dx[i]/2)
    x_ss.append(x[-1])
    x_ss = np.array(x_ss)
    f_ss = np.interp(x_ss, x, f)
    return f_ss, x_ss

def moving_mean(f,N):
    #Window = 2*N + 1
    mm = np.zeros(f.size)
    
    for i in range(f.size):
        if i < N:
            m = []
            for j in range(i+N+1):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        elif i+N > f.size-1:
            m = []
            for j in range(i-N,f.size):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        else:
            mm[i] = np.nanmean(f[i-N:i+N+1][np.where(np.isfinite(f[i-N:i+N+1]))[0]])
        
    return mm

##############################################################################
# load data

with open('../data/si_gol.pickle', 'rb') as file:
    si_data = pickle.load(file)

with open('../data/surf_gol.pickle', 'rb') as file:
    surf_data = pickle.load(file)

##############################################################################
# calculations

dSI = si_data['dSI']

x = np.arange(len(dSI))
dSIss, xss = super_sample(dSI, x)
dSImm = moving_mean(dSIss, 5)

peaks, _ = find_peaks(dSI, prominence=.0125)
peaksmm, _ = find_peaks(dSImm)
troughs, _ = find_peaks(-dSI, prominence=.015)

plt.plot(dSI)
plt.plot(peaks, dSI[peaks], marker='x', linestyle='')
plt.plot(troughs, dSI[troughs], marker='o', linestyle='')
plt.show()
