import pickle
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import core

##############################################################################

def q_lw(rlds, SST):
    o = 5.67 * 10**-8
    return rlds - o * SST**4

def cd(un):
    return (2.7/un + .142 + un/13.09)/1000

def ce(cd):
    return (34.6 * cd**.5)/1000

def ch(cd, zeta):
    if zeta > 0:
        return 18 * cd**.5 / 1000
    else:
        return 32.7 * cd**.5 / 1000
    
def qsat(sst):
    rhoa = 1.22
    q1 = .98 * 640380
    q2 = -5107.4
    return q1/rhoa * np.exp(q2/sst)

def qe(ce, qa, qsat, u):
    rhoa = 1.22
    Lambda = 2.5e6

    E = rhoa * ce * (qa - qsat) * u

    return Lambda * E

def Tv(Ta, qa):
    return Ta * (1 + .608*qa)

def ustar(cd, u):
    return cd**.5 * u

def tstar(ch, cd, Ta, sst):
    return ch / cd**.5 * (Ta - sst)

def qstar(ce, cd, qa, qsat):
    return ce / cd**.5 * (qa - qsat)

def zeta(z, ustar, tstar, qstar, qa, Tv):
    k = 0.4
    g = 9.81

    return k * g * z / ustar**2 * (tstar / Tv + qstar / (qa + 1/0.608))

def X(zeta):
    zeta = float(zeta)
    return np.real((1 - 16*zeta)**.25)

def phim(zeta, X):

    if zeta >= 0:
        return -5 * zeta

    else:
        return 2 * np.log((1 + X)/2) + np.log((1 + X**2)/2) - 2 * np.arctan(X) + np.pi/2

def phih(zeta, X):

    if zeta >= 0:
        return -5 * zeta

    else:
        return 2 * np.log((1 + X**2)/2)

def un(u, cd, zetau):
    k = 0.4
    return u * (1 + cd**.5 / k * (-phim(zetau, X(zetau))))**-1

def T10(Ta, tstar, zetau, zetat):
    k = 0.4
    return Ta - tstar / k * (np.log(2/10) + phih(zetau, X(zetau)) - phih(zetat, X(zetat)))

def q10(qa, qstar, zetau, zetaq):
    k = 0.4
    return qa - qstar / k * (np.log(2/10) + phih(zetau, X(zetau)) - phih(zetaq, X(zetaq)))

def cd10(cd, zetau):
    k = 0.4
    return cd * (1 + cd**.5 / k * (-phim(zetau, X(zetau))))**-2

def ch10(ch, cd, cd10, zetau):
    k = 0.4
    return ch * (cd10/cd)**.5 * (1 + ch/k/cd**.5 * -phih(zetau, X(zetau)))**-1

def ce10(ce, cd, cd10, zetau):
    k = 0.4
    return ce * (cd10/cd)**.5 * (1 + ce/k/cd**.5 * -phih(zetau, X(zetau)))**-1

def iterator(u, Ta, qa, sst, error):
    
    u_n = u
    zeta_n = 0

    c_d = cd(u_n)
    c_e = ce(c_d)
    c_h = ch(c_d, zeta_n)

    T_v = Tv(Ta, qa)
    q_sat = qsat(sst)

    u_star = ustar(c_d, u)
    t_star = tstar(c_h, c_d, Ta, sst)
    q_star = qstar(c_e, c_d, qa, q_sat)

    zetau = zeta(10, u_star, t_star, q_star, qa, T_v)
    zetaq = zeta(2, u_star, t_star, q_star, qa, T_v)
    zetat = zetaq

    u_n = un(u, c_d, zetau)
    T_10 = T10(Ta, t_star, zetau, zetat)
    q_10 = q10(qa, q_star, zetau, zetaq)

    c_d_10 = cd10(c_d, zetau)
    c_e = ce10(c_e, c_d, c_d_10, zetau)
    c_h = ch10(c_h, c_d, c_d_10, zetau)

    c_d = c_d_10

    err = error + 1

    while err > error:

        T_v = Tv(T_10, q_10)

        u_star = ustar(c_d, u)
        t_star = tstar(c_h, c_d, T_10, sst)
        q_star = qstar(c_e, c_d, q_10, q_sat)

        zetau = zeta(10, u_star, t_star, q_star, q_10, T_v)
        zetaq = zeta(2, u_star, t_star, q_star, q_10, T_v)
        zetat = zetaq
        
        u_n_bef = u_n
        u_n = un(u, c_d, zetau)
        
        err = np.abs(u_n - u_n_bef)

        T_10 = T10(T_10, t_star, zetau, zetat)
        q_10 = q10(q_10, q_star, zetau, zetaq)

        c_d_10 = cd10(c_d, zetau)
        c_e = ce10(c_e, c_d, c_d_10, zetau)
        c_h = ch10(c_h, c_d, c_d_10, zetau)

        c_d = c_d_10

        print('\n', u_star, t_star, q_star, zetau, zetaq, T_10, q_10, c_d, c_e, c_h)
        
    # print(err)
    
    rhoa = 1.22
    cp = 1000.5
    Qh = t_star * rhoa * cp * u_star
    E = q_star * rhoa * u_star

    # E = qe(c_e, qa, q_sat, u)

    return Qh, E
        

##############################################################################

with open('../data/surf_gol.pickle', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_gol.pickle', 'rb') as file:
    si = pickle.load(file)

rlds = xr.open_dataset('../data/wrforch_raw_rlds.nc')
tas = xr.open_dataset('../data/wrforch_seasonal_tas.nc')
huss = xr.open_dataset('../data/wrforch_seasonal_huss.nc')
# uas = xr.open_dataset('../data/wrforch_seasonal_uas.nc')
# vas = xr.open_dataset('../data/wrforch_seasonal_vas.nc')

##############################################################################

rlds = rlds.rlds
Ta = tas.tas
qa = huss.huss
# ws = (uas.uas**2 + vas.vas**2)**.5

sst = data['seasonal']['SST'] + 273.15
qnet = data['seasonal']['NHF']
qsw = data['seasonal']['SWD']
ws = data['seasonal']['WND']

# separating

beg = np.datetime64(data['t'][0])
end = np.datetime64(data['t'][-1])

ind = (rlds.Time >= beg) & (rlds.Time <= end)

# indexing

rlds = rlds[ind].values
Ta = Ta[ind].values
qa = qa[ind].values
# ws = ws[ind].values

qlw = q_lw(rlds, sst)
qlhsh = qnet - qsw - qlw

qsh = np.zeros_like(qlw)
qlh = np.zeros_like(qlw)
qem = np.zeros_like(qlw)

for i, u in enumerate(ws):
    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sst[i], Ta[i], qsat(sst[i]), qa[i], ws[i])
    qsh[i], qlh[i] = core.Qh(Ch, sst[i], T_zu, ws[i]), core.E(Ce, qsat(sst[i]), q_zu, ws[i])
    qem[i] = core.Em(Ce, qsat(sst[i]), q_zu, ws[i], sst[i])

qsh = -qsh
qlh = -qlh
